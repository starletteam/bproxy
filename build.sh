#
#set -x
#set -d
#set -v


COPTS="-fPIC -g -D_DEBUG=1 -D__TRACE__=1"
LIBS=" -static -pthread -lrt -latomic -lresolv"

COPTS+=" -I ../"
COPTS+=" -I ../../utility_routines/"

SRCS=" bproxy.c req.c auth.c stm.c cdr.c ban.c io.c quota.c ns.c peer.c agent.c"
SRCS+=" utility_routines.c "
SRCS+=" avl.c "

EXE="bproxy"

build	()
{
	echo	"Compile with $1 gcc for $2 ..."

	$1gcc $COPTS -o $EXE-$2 -w -D__ARCH__NAME__=\"$2\" $SRCS $LIBS
	##$1strip $EXE-$2
}

	build	"arm-linux-gnueabihf-"		"ARMhf"
	build	"mips-linux-gnu-"		"MIPS"
	build	"mipsel-linux-gnu-"		"MIPSel"
	build	""				"x86_64"
	build	"/openwrt/staging_dir/toolchain-mipsel_24kc_gcc-8.3.0_musl/bin/mipsel-openwrt-linux-musl-" "MIPSel-24kc"
