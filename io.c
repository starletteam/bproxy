#define	__MODULE__	"IO"
#define	__IDENT__	"X.00-06"

#ifdef	__GNUC__
	#pragma GCC diagnostic ignored  "-Wparentheses"
	#pragma	GCC diagnostic ignored	"-Wdate-time"
	#pragma	GCC diagnostic ignored	"-Wunused-variable"
#endif

#ifdef _WIN32
	#pragma once
	#define WIN32_LEAN_AND_MEAN             // Exclude rarely-used stuff from Windows headers
#endif

/*
**++
**
**  FACILITY:  BroProxy - A yet another SOCKS5(4), HTTP(s) high performance proxy server
**
**  DESCRIPTION: This module contains routines to implement I/O processor functionality
**
**  DESIGN ISSUE: We use a dinamic pool o threads to perform two-way I/O: from client to server,
**	and back. Using epoll() stuff to process I/O events from socket descriptors.
**
**  AUTHORS: Ruslan R. (The BadAss SysMan) Laishev
**
**  CREATION DATE:  21-FEB-2019
**
**  MODIFICATION HISTORY:
**
**	30-APR-2019	RRL	Fixed I/O counters.
**
**	 2-FEB-2020	RRL	X.00-03 : Set thread's stack size = g_thstack
**
**	28-MAY-202	RRL	X.00-03 : Refactoring logic of routines for network I/O;
**				fixed a problem with losing I/O events has been introduced by usage of EPOLLONESHOT.
**
**	 2-APR-2021	RRL	X.00-05 :  Added obfuscation of the data traffic on the  master-agent leg
**
**	29-JUN-2021	RRL	Tiny corrections of diagnostic messages.
**
**	27-NOV-2021	RRL	X.00-06 :  Added zeroing contexts before putting into the QUEUE.
**
**
**--
*/

#include	<stdlib.h>
#include	<unistd.h>
#include	<pthread.h>
#include	<errno.h>
#include	<poll.h>
#include	<sys/epoll.h>
#include	<fcntl.h>
#include	<memory.h>
#include	<arpa/inet.h>
#include	<time.h>
#include	<stdatomic.h>
#include	<signal.h>


#define		__FAC__	"BPROXY"
#define		__TFAC__ __FAC__ ": "		/* Special prefix for $TRACE			*/
#include	"utility_routines.h"

#include	"bproxy.h"

extern int	g_exit_flag, g_trace, g_iobufsz, g_thstack;

extern	BP_QUOTAS	g_quota,		/* Quotas: BW, Traffic Volume, Connections	*/
		quota_usage;

extern const char g_private_key[];
extern const int g_private_key_sz;






static BP_DPOOL io_params = {0};
static struct timespec	ts_last_zero,		/* A time stamp of last I/O threads crew adjustment	*/
		ts_delta = {0};			/* Delta time in the timespec format		*/
static atomic_int	cnt_th_current = 0,	/* Current number of the I/O threads in crew	*/
		cnt_th_target = 0,		/* Target number of I/O threads in crew		*/
		cnt_io = 0;			/* A number of I/O since last zeroing		*/


static	pthread_attr_t th_attr;

						/* Mutex is supposed to be used when thread want
						 * to performs cancelation check as a part of
						 * dynamic adjustment logic
						 */
static	pthread_mutex_t	th_exit_lock = PTHREAD_MUTEX_INITIALIZER;

static	pthread_cond_t stm_ready_cond = PTHREAD_COND_INITIALIZER;
static	pthread_mutex_t	stm_ready_lock = PTHREAD_MUTEX_INITIALIZER;
static	__QUEUE	stm_ready = QUEUE_INITIALIZER;	/* A queue for ready to I/O contexts	*/

static	__QUEUE	ctx_pool = QUEUE_INITIALIZER;	/* Pool for free thread's contexts	*/


/*
 * EPOLL stuff
 */
int	epollfd = 0, maxevents = BP$K_IOMAXEVTS;
struct epoll_event *events;

//#define	BP$M_EVENTS	(EPOLLONESHOT | EPOLLET | EPOLLRDHUP |  EPOLLIN )
/* A set of EPOLL's option is used by this module,
 * be advised that EPOLLONESHOT - is a potential reason of losing I/O event for socket
 */

#define	BP$M_EVENTS	(EPOLLET | EPOLLRDHUP |  EPOLLIN )

typedef struct __io_ctx__ {
	pthread_t	tid;

	unsigned long long	iocount;/* I/O operation count	*/

} IO_CTX;


/*
 *   DESCRIPTION:  Performs a relaying data from one socket to other.
 *
 *   INPUT:
 *	iobuf:	A buffer for I/O
 *	skin:	A descriptor of source data
 *	skout:	Descriptor for receipient of data
 *	endec:	A flag to encrypt/decrypt data buffer
 *
 *   OUTPUT:
 *	NONE
 *
 *   RETURNS:
 *	condition code
 */

inline static int	io_xmit(
			char	*iobuf,
		BP_SOCK		*skin,
		BP_SOCK		*skout,
		int		encdec
		)
{
int	buflen, status;
struct pollfd pfd = {skin->sd, POLLIN, 0};


	while	( !g_exit_flag )
		{
		/*
		** Extra checking of status of network socket to prevent unprocesses data in the
		** socket buffer.
		*/
		if ( 0 >  (status = poll(&pfd, 1, 0)) )
			return	$LOG(STS$K_ERROR, "poll(%d, POLLIN)->%d, errno=%d", pfd.fd, status, errno);
		else if ( !(pfd.revents & POLLIN) )
			return	STS$K_SUCCESS; //$LOG(STS$K_SUCCESS, "poll(%d, POLLIN)->%d, nothing to do", pfd.fd, status, errno);

		if ( 0 > (status = recv(skin->sd, iobuf, g_iobufsz, 0)) )
			{
			if ( errno != EAGAIN )
				return	$LOG(STS$K_ERROR, "recv(#%d)->%d, errno=%d", skin->sd, status, errno);

			$IFTRACE(g_trace, "recv(#%d)->%d, errno=%d", skin->sd, status, errno);
			continue;
			}
		else if ( !(buflen = status) )	/* EOF - Remote peer has closed connection */
			{
//			$IFTRACE(g_trace, "recv(#%d)->%d, errno=%d", skin->sd, status, errno);
			return	STS$K_WARN;
			}


		//$DUMPHEX(iobuf, buflen);

		if ( encdec == BP$K_DECRYPT )
			__swap_nibbles__ (iobuf, buflen);
		else if ( encdec == BP$K_ENCRYPT )
			__swap_nibbles__ (iobuf, buflen);


		//$DUMPHEX(iobuf, buflen);


		//$IFTRACE(g_trace, "recv(#%d)->%d", skin->sd, buflen);

		skin->iocnt++;
		skin->inbcnt += buflen;

		if ( !( 1 & (status = xmit_n(skout->sd, iobuf, buflen))) )
			return	status;

		//$IFTRACE(g_trace, "xmit_n(#%d)->%d", skout->sd, buflen);

		skout->iocnt++;
		skout->outbcnt += buflen;
		}

	return	STS$K_SUCCESS;
}




/*
 *   DESCRIPTION:  Process I/O for single stream context, rearm watching by EPOLL for I/O event,
 *	in case of error during I/O - dequeue stream context (destroy).
 *
 *   INPUT:
 *	ctx:	I/O thread context
 *	stmL	STREAM context
 *
 *   OUTPUT:
 *	NONE
 *
 *   RETURNS:
 *	condition code
 */

int	io_do		(
		IO_CTX	*ctx,
		BP_STREAM	*stm
			)
{
int	status, encdec = (stm->proto == BP$K_PROTO_PRIVATE);
struct epoll_event ev;

//	$IFTRACE(g_trace, "stm=%#x, [#%d-#%d], events=%#x - process", stm, stm->skin.sd, stm->skout.sd, stm->events);

	ctx->iocount++;

	/*
	 * Performs I/O operation on sockets, update Last I/O timestamp
	 */
	if ( 1 & (status  = io_xmit(stm->iobuf, &stm->skin, &stm->skout, (encdec) ? BP$K_ENCRYPT : 0)) )	/* From client to remote web host */
		status = io_xmit(stm->iobuf, &stm->skout, &stm->skin, (encdec) ? BP$K_DECRYPT : 0);		/* From remote web-host to client */

	clock_gettime(CLOCK_REALTIME, &stm->lastio);

	if ( (!(stm->events & (EPOLLERR | EPOLLHUP))) && (1 & status) )
		{
		stm->events = 0;
		ev.data.ptr = stm;
		ev.events = BP$M_EVENTS;

		if ( 0 > epoll_ctl(epollfd, EPOLL_CTL_MOD, stm->skin.sd, &ev) )
			status = $LOG(STS$K_ERROR, "[#%d-#%d] epoll_ctl(%d, EPOLL_CTL_MOD, %d, ... ), errno=%d",
				      stm->skin.sd, stm->skout.sd, epollfd, stm->skin.sd, errno);

		if ( 0 > epoll_ctl(epollfd, EPOLL_CTL_MOD, stm->skout.sd, &ev) )
			status = $LOG(STS$K_ERROR, "[#%d-#%d] epoll_ctl(%d, EPOLL_CTL_MOD, %d, ... ), errno=%d",
				      stm->skin.sd, stm->skout.sd, epollfd, stm->skout.sd, errno);

		//$IFTRACE(g_trace, "stm=%#x, [#%d-#%d] - rearmed", stm, stm->skin.sd, stm->skout.sd);
		}
	else	{
		$IFTRACE(g_trace, "stm=%#x, [#%d-#%d], events=%#x - process", stm, stm->skin.sd, stm->skout.sd, stm->events);
		io_deq (stm);
		}

	return	STS$K_SUCCESS;
}


int	io_thread	(IO_CTX *ctx)
{
int	status, count, idlecount;
struct	timespec now = {0, 0}, etime = {0, 0}, tmo = {10, 0};

BP_STREAM	*stm;
char	in_ip[32], out_ip[32];

const IO_CTX zero_ctx = {0};


	atomic_fetch_add(&cnt_th_current, 1);

	for ( idlecount = 0; !g_exit_flag; idlecount++ )
		{
		/* Waiting for a new I/O events ... */
		if ( status = clock_gettime(CLOCK_REALTIME, &now) )
			$LOG(STS$K_ERROR, "clock_gettime->%d", status);

		__util$add_time (&now, &tmo, &etime);

		if ( status = pthread_mutex_timedlock(&stm_ready_lock, &etime) )
			{
			$LOG(STS$K_ERROR, "pthread_mutex_timedlock()->%d, errno=%d", status, errno);
			continue;
			}

		status = pthread_cond_timedwait(&stm_ready_cond, &stm_ready_lock, &etime);

		if ( pthread_mutex_unlock(&stm_ready_lock) )
			$LOG(STS$K_ERROR, "pthread_mutex_unlock(), errno=%d", errno);

		if ( status && (status != ETIMEDOUT) )
			{
			status = $LOG( STS$K_ERROR, "pthread_cond_timedwait()->%d", status);

			idlecount++;

			continue;
			}

		/* Has got events, try to get an Session Context from a queue */
		while ( !g_exit_flag && (1 & $REMQHEAD(&stm_ready, &stm, &count)) )
			{
			if ( !count )
				break;

			atomic_fetch_add(&cnt_io, 1);

			if ( !(1 & (status = io_do (ctx, stm))) )
				{
				inet_ntop(AF_INET, &stm->skin.sk.sin_addr, in_ip, (socklen_t) sizeof(in_ip));
				inet_ntop(AF_INET, &stm->skout.sk.sin_addr, out_ip, (socklen_t) sizeof(out_ip));

				$LOG(STS$K_ERROR, "%#x: %#x %s:%d->%s:%d", ctx, stm,
					in_ip, ntohs(stm->skin.sk.sin_port), out_ip, ntohs(stm->skout.sk.sin_port));
				}

			/*
			 * We expect that epoll's stuff has been rearmed, so we
			 * can release context
			 */
			if ( status = pthread_mutex_unlock (&stm->mtx) )
				$LOG(STS$K_ERROR, "[%#x] pthread_mutex_unlock()->%d, errno=%d", status, errno);
			}


		/* Quick check that we need to performs auto-adjustment */
		if ( atomic_load(&cnt_th_current) == atomic_load(&cnt_th_target) )
			continue;


		/* So at this point we are ready to performs adjustment,
		 * so set "disable threads cancelation" flag
		 */
		if ( status = clock_gettime(CLOCK_REALTIME, &now) )
			{
			$LOG(STS$K_ERROR, "clock_gettime->%d", status);
			continue;
			}

		__util$add_time (&now, &tmo, &etime);

		if ( status = pthread_mutex_timedlock(&th_exit_lock, &etime) )
			{
			$LOG(STS$K_ERROR, "pthread_mutex_timedlock()->%d, errno=%d", status, errno);
			continue;
			}

		if ( atomic_load(&cnt_th_current) > atomic_load(&cnt_th_target) )
			break;

		if ( status = pthread_mutex_unlock(&th_exit_lock) )
			$LOG(STS$K_ERROR, "pthread_mutex_timedlock()->%d, errno=%d", status, errno);
		}

	$IFTRACE(g_trace, "Stoping thread ...");

	atomic_fetch_sub(&cnt_th_current, 1);

	if ( status = pthread_mutex_unlock(&th_exit_lock) )
		$LOG(STS$K_WARN, "pthread_mutex_timedlock()->%d, errno=%d", status, errno);

	status = STS$K_SUCCESS;

	*ctx = zero_ctx;
	$INSQTAIL(&ctx_pool, ctx, &count);

	$IFTRACE(g_trace, "Stop thread");

	pthread_exit (NULL);
}

int	io_process_events	(
	struct epoll_event	*events,
		int		 cntevents
				)
{
int	status, count;
struct epoll_event	*eventp, ev;
BP_STREAM	*stm;

	for ( eventp = events; cntevents; eventp++, cntevents--)
		{
		if ( !(stm = eventp->data.ptr) )
			continue;

		stm->events |= eventp->events;

		if ( status = pthread_mutex_trylock (&stm->mtx) )
			{
			/*
			 * We assume that this stream context already has been processed,
			 * so just skip it.
			 */
			if (status != EBUSY)
				$LOG(STS$K_ERROR, "[%#x] pthread_mutex_trylock()->%d, errno=%d", stm, status, errno);

			continue;
			}

		ev.events = 0;
		ev.data.ptr = stm;

		/* Disable epoll() for the both socket descriptors */
		if ( 0 > epoll_ctl(epollfd, EPOLL_CTL_MOD, stm->skin.sd, &ev) )
			if ( errno != ENOENT )
				status = $LOG(STS$K_ERROR, "epoll_ctl(%d, EPOLL_CTL_MOD, %d, ... ), errno=%d", epollfd, stm->skin.sd, errno);

		if ( 0 > epoll_ctl(epollfd, EPOLL_CTL_MOD, stm->skout.sd, &ev) )
			if ( errno != ENOENT )
				status = $LOG(STS$K_ERROR, "epoll_ctl(%d, EPOLL_CTL_MOD, %d, ... ), errno=%d", epollfd, stm->skout.sd, errno);

		if ( !(1 & (status = $INSQTAIL(&stm_ready, stm, &count))) )
			{
			$LOG(STS$K_ERROR, "[#%d-#%d] Cannot insert stream (%#x) into I/O processor queue, status=%#x", stm->skin.sd, stm->skout.sd, stm, status);
			status = io_deq(stm);
			}
		}

	return	STS$K_SUCCESS;
}

/*
 *   DESCRIPTION: Thread routine - performs wait for I/O events by using epoll_wait,
 *	add ready to I/O context to the queue to process by I/O worker crew.
 *
 *   INPUT:
 *
 *   OUTPUT:
 *
 *   RETURN:
 */
void	*io_sched_thread	(void)
{
int	status, cntevents;
pthread_mutex_t __mtx = PTHREAD_MUTEX_INITIALIZER;


	while ( !g_exit_flag )
	       {
	       if ( 0 > (status = cntevents = epoll_wait(epollfd, events, maxevents, 15 * 1000)) )
			{
			if ( errno != EINTR )
			       $LOG(STS$K_ERROR, "epoll_wait(%d, ...), errno=%d", epollfd, errno);
			}

		if ( cntevents > 0 )
			{
			if ( cntevents >= (maxevents - 1) )
			       $LOG(STS$K_WARN, "A count of I/O events (%d) is near to maximum (%d)", cntevents, maxevents);

			io_process_events (events, cntevents);

			/*
			* Signaling to workers that new I/O events has took place !!!!
			*/
			if ( status = pthread_mutex_lock (&__mtx) )
			       $LOG(STS$K_ERROR, "pthread_mutex_lock()->%d, errno=%d", status, errno);
			else	{
			       if ( status = pthread_cond_broadcast (&stm_ready_cond) )
				       $LOG(STS$K_ERROR, "pthread_cond_broadcast()->%d, errno=%d", status, errno);

			       if ( status = pthread_mutex_unlock(&__mtx) )
				       $LOG(STS$K_ERROR, "pthread_mutex_unlock()->%d, errno=%d", status, errno);
			       }
			}
//		else	$IFTRACE(g_trace, "epoll_wait(%d)->%d, errno=%d", epollfd, status, errno);
		}

	return	STS$K_SUCCESS;
}

int	io_crew_init	(
		ASC	*params
		)
{
int	status, i, memsz, count;
IO_CTX *ctx;
BP_DPOOL *p = &io_params;
pthread_t	tid;

	sscanf ($ASCPTR(params), "%d,%d,%d,%d,%d,%d,%d,%d",
		&p->def, &p->min, &p->max, &p->inc, &p->dec, &p->delta, &p->hrate, &p->lrate);

	$LOG(STS$K_INFO, "Create I/O worker crew (def/min/max/inc/dec/delta/hrate/lrate=%d/%d/%d/%d/%d/%d/%d/%d)",
		p->def, p->min, p->max, p->inc, p->dec, p->delta, p->hrate, p->lrate);

	if ( (!p->delta) || ((!p->hrate) && (!p->lrate)) )
		$LOG(STS$K_WARN, "Dynamic I/O threads adjustment is DISABLED!");

	/* Create context pool with <p->max> contexts */
	if ( !(ctx = calloc (p->max, sizeof(IO_CTX))) )
		return	$LOG(STS$K_ERROR, "Cannot allocate %d octets for thread context, errno=%d", p->max * sizeof(IO_CTX), errno);

	for ( i = p->max; i--; ctx++)
		$INSQTAIL(&ctx_pool, ctx, &count);

	/*
	** Initialize EPOLL stuff ...
	 */
	if ( !(events = calloc(sizeof(struct epoll_event), maxevents)) )
		return	$LOG(STS$K_ERROR, "Cannot allocate %u octets for area for %d I/O events, errno=%d",
			sizeof(struct epoll_event) * maxevents, maxevents, errno);

	$IFTRACE(g_trace, "Allocated %u octets for %d I/O events", sizeof(struct epoll_event) * maxevents, maxevents);

	if ( 0 > (epollfd = epoll_create(maxevents)) )
		return $LOG(STS$K_ERROR, "epoll_create(%d), errno=%d", maxevents, errno);

	$IFTRACE(g_trace, "Create epool's context #%d for %d I/O events", epollfd, maxevents);

	pthread_attr_init(&th_attr);
	pthread_attr_setstacksize(&th_attr, g_thstack /* PTHREAD_STACK_MIN * 2*/  );
	pthread_attr_setdetachstate(&th_attr, PTHREAD_CREATE_DETACHED);

	/* Create 'default' quantity of threads */
	atomic_init(&cnt_th_target, p->def);

	for ( i = 0; i <  p->def; i++ )
		{
		$REMQHEAD(&ctx_pool, &ctx, &count);

		if ( status = pthread_create(&tid, &th_attr, io_thread, ctx) )
			{
			$LOG(STS$K_ERROR, "Cannot start I/O thread, pthread_create()->%d, errno=%d", status, errno);
			break;
			}
		else	pthread_setname_np(tid, "bproxy-iop");
		}

	if ( !i )
		return	$LOG(STS$K_ERROR, "No I/O thread has been created!");

	if ( status = pthread_create(&tid, &th_attr, io_sched_thread, NULL) )
		return	$LOG(STS$K_ERROR, "Cannot start I/O scheduler thread, pthread_create()->%d, errno=%d", status, errno);
	else	pthread_setname_np(tid, "bproxy-iosched");


	/* Set counters to initial states */
	clock_gettime(CLOCK_REALTIME, &ts_last_zero);

	ts_delta.tv_sec = p->delta;

	return	$LOG(STS$K_SUCCESS, "Requested %d I/O threads - %d has been created", p->def, i);
}



/*
 *   DESCRIPTION: Add new stream into the queue of the I/O processor engine.
 *
 *   INPUT:
 *	sd_in:		client's socket descriptor
 *	sk_in:		client's socket structure
 *	sd_out:		server's socket descriptor
 *	sk_out:		server's socket structure
 *	account:	user account
 *	host:		 ...
 *	proto:		protocol, see BP$K_PROTO *
 */
int	io_enq	(
			int	insd,
	struct sockaddr_in	*insk,
			int	outsd,
	struct sockaddr_in	*outsk,
		ASC		*account,
		ASC		*host,
			int	proto
		)
{
int	status;
BP_STREAM	*stm;
struct epoll_event ev = {BP$M_EVENTS | EPOLLOUT, {0}};
char	sia[32] = {0}, dia[32] = {0};

	if ( g_trace )
		{
		inet_ntop(insk->sin_family, &insk->sin_addr, sia, sizeof (sia));
		inet_ntop(outsk->sin_family, &outsk->sin_addr, dia, sizeof (dia));

		$IFTRACE(g_trace, "[#%d-#%d] %s:%d-%s:%d", insd, outsd,
			sia, htons(insk->sin_port), dia, htons(outsk->sin_port) );
		}

	/* We suppose use only NONBLOCK operation by this I/O engine !!! */
	fcntl(insd, F_SETFL, fcntl(insd, F_GETFL, 0) | O_NONBLOCK);
	fcntl(outsd, F_SETFL, fcntl(outsd, F_GETFL, 0) | O_NONBLOCK);

	/* Get new stream context from the Stream context pool */
	if ( !(1 & (status = stm_get(&stm))) )
		{
		close(insd);
		close(outsd);

		return	$LOG(STS$K_ERROR, "[#%d-#%d] Error allocation stream context", insd, outsd);
		}

	/*
	 * We expect that stream context has been zeroed,
	 * so just filling by new data
	 */
	clock_gettime(CLOCK_REALTIME, &stm->cretim);
	stm->lastio = stm->cretim;

	stm->skin.inbcnt = stm->skin.outbcnt = stm->skin.iocnt = 0;
	stm->skout.inbcnt = stm->skout.outbcnt = stm->skout.iocnt = 0;
	stm->events = 0;

	stm->skin.sd = insd;
	stm->skin.sk = *insk;
	stm->skout.sd = outsd;
	stm->skout.sk = *outsk;
	stm->proto = proto;


	if ( host )
		stm->dhost = *host;
	else	$ASCLEN(&stm->dhost) = 0;

	if ( account )
		stm->user = *account;
	else	$ASCLEN(&stm->user) = 0;


	/* Prepare event stuff for epoll() */
	ev.events = BP$M_EVENTS | EPOLLOUT;
	ev.data.ptr = stm;

	/* We monitoring I/O events on the both socket descriptors !!! */
	if ( 0 > epoll_ctl(epollfd, EPOLL_CTL_ADD, insd, &ev) )
		status = $LOG(STS$K_ERROR, "epoll_ctl(%d, EPOLL_CTL_ADD, %d, ... ), errno=%d", epollfd, insd, errno);
	else if ( 0 > epoll_ctl(epollfd, EPOLL_CTL_ADD, outsd, &ev) )
		status = $LOG(STS$K_ERROR, "epoll_ctl(%d, EPOLL_CTL_ADD, %d, ... ), errno=%d", epollfd, outsd, errno);
	else	{
		$IFTRACE(g_trace, "stm=%#x, [#%d-#%d] - added", stm, insd, outsd);
		return	STS$K_SUCCESS;
		}

	/* Error !!! Release resources ... */
	if ( 0 > epoll_ctl(epollfd, EPOLL_CTL_DEL, insd, NULL) )
		$LOG(STS$K_ERROR, "epoll_ctl(%d, EPOLL_CTL_ADD, %d, ... ), errno=%d", epollfd, insd, errno);

	if ( 0 > epoll_ctl(epollfd, EPOLL_CTL_DEL, outsd, NULL) )
		$LOG(STS$K_ERROR, "epoll_ctl(%d, EPOLL_CTL_DEL, %d, ... ), errno=%d", epollfd, outsd, errno);

	/* Get new stream context from the Stream context pool */
	if ( !(1 & (status = stm_free(stm))) )
		$LOG(STS$K_ERROR, "[#%d-#%d] Error releasing stream context", insd, outsd);


	close(insd);
	close(outsd);

	return	STS$K_ERROR;
}


/*
 *   DESCRIPTION: Remove stream context from the I/O processor queue, remove socket descriptors from epoll(),
 *	close socket descriptors, return stream context into the free pool.
 *
 *   INPUT:
 *	stm:	A BP stream's descriptor
 */
int	io_deq	(
		BP_STREAM	*stm
		)
{
int	status;

	/* Remove from epoll() */
	if ( 0 > epoll_ctl(epollfd, EPOLL_CTL_DEL, stm->skin.sd, NULL) )
		$LOG(STS$K_ERROR, "epoll_ctl(%d, EPOLL_CTL_ADD, %d, ... ), errno=%d", epollfd, stm->skin.sd, errno);

	if ( 0 > epoll_ctl(epollfd, EPOLL_CTL_DEL, stm->skout.sd, NULL) )
		$LOG(STS$K_ERROR, "epoll_ctl(%d, EPOLL_CTL_DEL, %d, ... ), errno=%d", epollfd, stm->skout.sd, errno);

	/* Close TCP-connections */
	close(stm->skin.sd);
	close(stm->skout.sd);

	$IFTRACE(g_trace, "stm=%#x, [#%d-#%d] - finished, client[in=%llu, out=%llu], target[in=%llu, out=%llu, duration=%d.%3d secs]", stm, stm->skin.sd, stm->skout.sd,
		 stm->skin.inbcnt, stm->skin.outbcnt, stm->skout.inbcnt, stm->skout.outbcnt,
		 stm->lastio.tv_sec - stm->cretim.tv_sec, stm->lastio.tv_nsec - stm->cretim.tv_nsec/1024);

	status = cdr_save(stm);

	/* Return Stream context into the pool */
	if ( !(1 & (status = stm_free(stm))) )
		return	$LOG(STS$K_ERROR, "Error releasing stream context");

	return	STS$K_SUCCESS;
}


/*
 *   DESCRIPTION: I/O processor threads manager - analyze counters, start new threads, adjust target thread's quantity
 *	according DPOOL's configuration vector. This routine is supposed to be called at regular basis with interval has been
 *	defined in the configuration vector.
 *
 *
 *   IMPLICIT INPUTS:
 *
 *   IMPLICIT OUTPUTS:
 *
 *   RETURN:
 *	condition code
 */
int	io_crew_manager	(void)
{
struct timespec now = {0}, etime = {0}, tmo = {3, 0};
int	status, rate, cur, tg, count, rc;
IO_CTX	*ctx = NULL;
pthread_t	tid;

	if ( (!io_params.delta) || ((!io_params.hrate) && (!io_params.lrate)) )
		return	STS$K_SUCCESS;

	/* Is we reach an end of interval of checking ?*/
	clock_gettime(CLOCK_REALTIME, &now);

	__util$add_time(&ts_last_zero, &ts_delta, &etime);

	if ( 0 > __util$cmp_time (&now, &etime) )
		return	STS$K_SUCCESS;

	/* Performs rate computaion and checks */
	rate = atomic_load(&cnt_io);
	rate /= ts_delta.tv_sec;

	/* Check that current rate is between <lowrate> and <highrate> */
	if ( __util$isinrange  (rate, io_params.lrate, io_params.hrate) )
		{
		atomic_store(&cnt_io, 0);
		clock_gettime(CLOCK_REALTIME, &ts_last_zero);

		return	STS$K_SUCCESS;	/* Nothing to do !*/
		}

	/* So at this point we are ready to performs adjustment
	 * so set "disable threads cancelation" flag
	 */
	__util$add_time (&now, &tmo, &etime);

	if ( status = pthread_mutex_timedlock(&th_exit_lock, &etime) )
		return	$LOG(STS$K_ERROR, "pthread_mutex_timedlock()->%d, errno=%d", status, errno);

	cur = atomic_load(&cnt_th_current);
	tg = atomic_load(&cnt_th_target);

	$IFTRACE(g_trace, "IO threads : current=%d, target=%d, rate=%d", cur, tg, rate);

	if ( rate > io_params.hrate )
		tg = cur + io_params.inc;

	if ( rate < io_params.lrate )
		tg = cur - io_params.dec;

	tg = __util$range (tg, io_params.min, io_params.max);

	atomic_store(&cnt_th_target, tg);

	if ( status = pthread_mutex_unlock(&th_exit_lock) )
		return	$LOG(STS$K_ERROR, "pthread_mutex_timedlock()->%d, errno=%d", status, errno);

	atomic_store(&cnt_io, 0);
	clock_gettime(CLOCK_REALTIME, &ts_last_zero);


	/* Do we need start additional threads ? */
	if ( cur < tg )
		{
		for ( count = tg - cur; count; count--)
			{
			/* Get or create new stream context */
			if ( !(1 & (status = $REMQHEAD(&ctx_pool, &ctx, &rc))) )
				$LOG(status, "Error allocation thread context");
			else if ( !(rc > 0) )
				$IFTRACE(g_trace, "Context pool is empty, check /IOTHREADS option");
			else if ( status = pthread_create(&tid, &th_attr, io_thread, ctx) )
				$LOG(STS$K_ERROR, "Cannot start I/O thread, pthread_create()->%d, errno=%d", status, errno);
			else	pthread_setname_np(tid, "bproxy-iop");
			}
		}


	return	STS$K_SUCCESS;
}
