#ifndef	__BAGENT$DEF__
#define	__BAGENT$DEF__	1



/*++
**
**  FACILITY:  BroProxy-Agent - a light weight proxy agent
**
**  DESCRIPTION: A Bagent API external routines and data declarations
**
**
**  AUTHORS: Ruslan R. (The BadAss SysMan) Laishev
**
**  CREATION DATE:  31-JUL-2021
**
**  MODIFICATION HISTORY:
**
**	 6-SEP-2021	RRL	BP$K_NSMAX 3 -> 4
**
**	29-SEP-2021	RRL	Added defs for BP$K_STATUS.
**
**	16-OCT-2021	RRL	Add internal NS's related counters into the BA_COUNTERS
**
**	27-NOV-2021	RRL	Added field into the BA_COUNTERS.
**
**--
*/

#ifdef __cplusplus
extern "C" {
#endif





#ifdef	WIN32

	#if	(_M_X64)
		#define	__ARCH__NAME__	"Win64"
	#else
		#define	__ARCH__NAME__	"Win32"
	#endif
#elif __APPLE__
	#define	__ARCH__NAME__	"APPLE"

#elif  ANDROID
	#if defined(__arm__)
		#if defined(__ARM_ARCH_7A__)
		#if defined(__ARM_NEON__)
			#if defined(__ARM_PCS_VFP)
				#define ABI "armeabi-v7a/NEON (hard-float)"
			#else
				#define ABI "armeabi-v7a/NEON"
			#endif
			#else
			#if defined(__ARM_PCS_VFP)
				#define ABI "armeabi-v7a (hard-float)"
			#else
				#define ABI "armeabi-v7a"
			#endif
		#endif
		#else
			#define ABI "armeabi"
		#endif
	#elif defined(__i386__)
		#define ABI "x86"
	#elif defined(__x86_64__)
		#define ABI "x86_64"
	#elif defined(__mips64)  /* mips64el-* toolchain defines __mips__ too */
		#define ABI "mips64"
	#elif defined(__mips__)
		#define ABI "mips"
	#elif defined(__aarch64__)
		#define ABI "arm64-v8a"
	#else
		#define ABI "unknown"
    #endif

    #define __ARCH__NAME__ ABI
#endif


#ifndef __ARCH__NAME__
	#define	__ARCH__NAME__	"APPLE"
#endif // !__ARCH_NAME__


#define	BP$K_IOBUFSZ	(64*1024)
#define	BP$K_NSMAX	4							/* A number of NS-es in the context	*/
#define	BP$K_NSCACHE	1024							/* Cache size for IP4 addresses	 	*/


#define	BP$K_PIPESZ	512
#define	BP$K_PIPENAME	"\\\\.\\pipe\\bagent-pipe"

enum	{
	BP$K_BAGUP = 1,								/* BAGENT is started, bagent_start() has been called */
	BP$K_BAGDOWN,								/* BAGENT is down, bagent_stop() --//--		*/
	BP$K_BAGCTLUP,								/* BAGENT Control channel is established	*/
	BP$K_BAGCTLDOWN,							/* BAGENT Control channel has been closed	*/

	BP$K_PROTSD,								/* Need performs "protect-from-VPN: for socket	*/
	BP$K_STATUS,								/* Status of the BAgent */
};

#pragma pack(push, 8)

typedef struct __ba_counters__ {
#ifdef ANDROID
	_Atomic unsigned long long inm,						/* Octets has been received from master */
#else
	unsigned long long inm,							/* Octets has been received from master */
#endif
		outm,								/* Octets has been sent to master	*/
		inh,								/* Octets has been received from target host */
		outh;								/* Octets has been sent to target host	*/

	unsigned long long	nshits,						/* A number of hits to the cache's entries */
				nstotal,					/* A total number of the NS's request */
				nserr;						/* A number of NS errors */

} BA_COUNTERS;


typedef struct __ba_status__ {
	int	tid,								/* PID/TID */
		state,								/* BAgent state, see BP$K_BAGUP - BP$K_BAGCTLDOWN ... */
		reqs;								/* Amount of server requests */

	BA_COUNTERS counters;
} BA_STATUS;

#pragma pack(pop)


enum {
	BP$M_TRACE = (1 << 0),							/* A set of bit-coded options for bagent_init()	*/
	BP$M_OBFUSCATE = (1 << 1)
};



#ifdef WIN32
#define	__ba_errno__	WSAGetLastError()
#else
#define	__ba_errno__	errno
#endif // WIN32

/*
 *  DESCRIPTION: BAGENT's API routine: initialize context area for a new control thread, it is supposed to be called
 *		one for every master. Created context is should be used on calling other  BAGENT's API routines:
 *		bagent_start();
 *		bagent_stop();
 *
 *  INPUTS:
 *	master:		A master IP:Port pais string
 *	auth:		A username:password pair is supposed to be used for authorization on master
 *	extbind:	An interface name or IP address
 *	nserver:	Name Server(-s) list
 *	bitopts:	A bit-coded options set
 *	cbrtn:		Callback routine
 *	cbarg:		Callback's routine arguments
 *
 *  OUTPUTS:
 *	ctx:		Newly created context
 *
 *  RETURNS:
 *	Condition code, see STS$K_* constants.
 */
int	bagent_init	(
		ASC	*master,
		ASC	*auth,
		ASC	*extbind,
		ASC	*nserver,
		int	 bitopts,
		void	*cbrtn,
		void	*cbarg,
		void	**ctx
);

/*
 *  DESCRIPTION: BAGENT's API routine: start executing of control thread with the has been created context.
 *
 *  INPUTS:
 *	ctx:	context has been intialized by bagent_init()
 *
 *  OUTPUTS: NONE
 *
 *  RETURNS:
 *	Condition code, see STS$K_* constants.
 */

int	bagent_start	(
		void	*ctx
);

/*
 *  DESCRIPTION: BAGENT's API routine: set global 'stop all threads' flag, if wait flag is non-zero - take some seconds to
 *	finishing threads.
 *
 *  INPUTS:
 *	ctx:	context has been intialized by bagent_init()
 *
 *  OUTPUTS: NONE
 *
 *  RETURNS:
 *	Condition code, see STS$K_* constants.
 */
int	bagent_stop	(
		void	*ctx,
		int	wait_flag
);

/*
 *  DESCRIPTION: BAGENT's API routine: set global 'stop all threads' flag, if wait flag is non-zero - take some seconds to
 *	finishing threads.
 *
 *  INPUT:
 *	ctx:	context has been intialized by bagent_init()
 *
 *  OUTPUT: NONE
 *
 *  RETURN:
 *	Condition code, see STS$K_* constants.
 */
int	bagent_shut	(
		void	*ctx,
		int	wait_flag
);

/*
 *  DESCRIPTION: BAGENT's API routine: retrieve identification and version string.
 *
 *  INPUTS:
 *	NONE
 *
 *  OUTPUTS:
 *	ident:	BAGENT API Identification string
 *	ver:	Version/revision code
 *
 *  RETURNS:
 *	Condition code, see STS$K_* constants.
 */

int	bagent_info	(
		ASC	*ident,
		ASC	*rev
);


#ifdef __cplusplus
	}
#endif



#endif		/* __BAGENT$DEF__ */
