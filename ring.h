#ifndef	__RING$DEF__
#define	__RING$DEF__	1

#ifdef	__GNUC__
	#pragma GCC diagnostic ignored  "-Wparentheses"
	#pragma	GCC diagnostic ignored	"-Wdate-time"
	#pragma	GCC diagnostic ignored	"-Wunused-variable"
#endif


#ifdef _WIN32
	#pragma once
	#define WIN32_LEAN_AND_MEAN             // Exclude rarely-used stuff from Windows headers
#endif


/*
**++
**
**  FACILITY:  BroProxy - A yet another SOCKS5(4), HTTP(s) high performance proxy server
**
**  DESCRIPTION: This module contains data structures routines to work with ring buffers.
**
**
**  AUTHORS: Ruslan R. (The BadAss SysMan) Laishev
**
**  CREATION DATE:  22-FEB-2019
**
**  MODIFICATION HISTORY:
**
**
**--
*/

#include	"utility_routines.h"


#ifdef __cplusplus
extern "C" {
#endif


#pragma	pack	(push, 4)

/* Ring Buffer structure */
typedef	struct	__bp_ring_	{
	ENTRY	ent;		/* To help making pool of the ring buffers */

	char	*ffb,		/* A pointer to first free byte	*/
		*fdb;		/* A pinter to first data byte	*/
	char	*buf;		/* An address of the buffer space*/
	size_t	 bufsz;		/* A size of the buffer space	*/
} BP_RING;


/*
 *  DESCRIPTION: Initialize ring buffer structure with given parameters.
 *
 *  INPUT:
 *	rbuf	- an address of the RING Buffer structure
 *	bufp	- an address of the continuous space
 *	bufsz	- a size of the buffer space
 *
 *  RETURN:
 *	condition code, see STS$K_* constants
 */

static	inline	void	rbuf_init	(
			BP_RING		*rbuf,
			void		*bufp,
			int		bufsz
					)
{
	/* Buffer space address and size */
	rbuf->buf = bufp;
	rbuf->bufsz = bufsz;

	/* Point head & tail pointer to buffer space */
	rbuf->ffb = rbuf->fdb = bufp;
}


/*
 *  DESCRIPTION: rbuf_get_free - get free space of requested size (or from the ring buffer.
 *
 *  INPUT:
 *	rbuf	- an address of the RING Buffer structure
 *	reqsz	- a requested size of the free space
 *
 *  OUTPUT:
 *	bufp	- a returned pointer to first free byte in the ring buffer
 *	bufsz	- a returned actual len of the free space area
 *
 *  RETURN:
 *	condition code, see STS$K_* constants
 */
static	inline	int	rbuf_get_free	(
			BP_RING		*rbuf,
				int	reqsz,
				void	**bufp,
				int	*bufsz
				)
{
int	len;

	/*
	 * Simplest situation: ffb is not overrun a border of the buffer space
	 *
	 * buf|                                        | buf + bufz
	 *    +----------------------------------------+
	 *    | --- data ----|     ---    free     --- |
	 *    +----------------------------------------+
	 *    ^              ^
	 *    |              |
	 *    fdb            ffb
	 */
	if ( rbuf->fdb <= rbuf->ffb )
		{
		*bufp = rbuf->ffb;

		/* Compute continuous free space */
		len = rbuf->bufsz - (rbuf->ffb - rbuf->fdb);
		*bufsz = $MIN(len, reqsz);

		return	STS$K_SUCCESS;
		};


	/*
	 * No simple situation: ffb is at front of data area
	 *
	 * buf|                                        | buf + bufz
	 *    +----------------------------------------+
	 *    | --- free ----|     ---    data     --- |
	 *    +----------------------------------------+
	 *    ^              ^
	 *    |              |
	 *    ffb            fdb
	 */
	if ( rbuf->fdb < rbuf->ffb )
		{
		*bufp = rbuf->ffb;

		/* Compute continuous free space */
		len = rbuf->fdb - rbuf->ffb;
		len--;	/* !!! */
		*bufsz = $MIN(len, reqsz);

		return	STS$K_SUCCESS;
		};


	/* Undefined situation ?	*/
	return	STS$K_ERROR;

}


/*
 *  DESCRIPTION: return a continuous data block from the ring buffer.
 *
 *  INPUT:
 *	rbuf	- an address of the RING Buffer structure
 *	reqsz	- a requested size of the free space
 *
 *  OUTPUT:
 *	bufp	- a returned pointer to first data byte in the ring buffer
 *	bufsz	- a returned actual len of the continuous data block
 *
 *  RETURN:
 *	condition code, see STS$K_* constants
 */
static	inline	int	rbuf_get_data	(
			BP_RING		*rbuf,
				int	reqsz,
				void	**bufp,
				int	*bufsz
				)
{
int	len;

	/* No data ?! */
	if ( rbuf->fdb == rbuf->ffb )
		{
		*bufsz = 0;

		return	STS$K_SUCCESS;
		}

	/*
	 * Simplest situation: fdb is not overrun a border of the buffer space
	 *
	 * buf|                                        | buf + bufz
	 *    +----------------------------------------+
	 *    | --- data ----|     ---    free     --- |
	 *    +----------------------------------------+
	 *    ^              ^
	 *    |              |
	 *    fdb            ffb
	 */
	if ( rbuf->fdb < rbuf->ffb )
		{
		*bufp = rbuf->fdb;

		/* Compute continuous data block */
		len = rbuf->ffb - rbuf->fdb;
		*bufsz = $MIN(len, reqsz);

		return	STS$K_SUCCESS;
		};


	/*
	 * No simple situation: fdb has overrun buffer space border
	 *
	 * buf|                                        | buf + bufz
	 *    +----------------------------------------+
	 *    | --- free ----|     ---    data     --- |
	 *    +----------------------------------------+
	 *    ^              ^
	 *    |              |
	 *    ffb            fdb
	 */
	if ( rbuf->fdb > rbuf->ffb )
		{
		*bufp = rbuf->fdb;

		/* Compute continuous free space */
		len = rbuf->bufsz - (rbuf->fdb - rbuf->ffb);
		*bufsz = $MIN(len, reqsz);

		return	STS$K_SUCCESS;
		};


	/* Undefined situation ?	*/
	return	STS$K_ERROR;

}


/*
 *  DESCRIPTION: Adjust first free byte pointer according 'bcnt', wrapping pointer if need
 *
 *  INPUT:
 *	rbuf	- an address of the RING Buffer structure
 *	bcnt	- a requested size of the free space
 *
 *  OUTPUT:
 *	NONE
 *
 *  RETURN:
 *	NONE
 */
static	inline	void	rbuf_adjust_data	(
			BP_RING		*rbuf,
				int	bcnt
				)
{
	if ( rbuf->ffb != rbuf->fdb)
		{
		/* Sanity check */
		assert( (rbuf->fdb + bcnt) > (rbuf->buf + rbuf->bufsz) );
		}

	rbuf->fdb = ((rbuf->fdb + bcnt) == (rbuf->buf + rbuf->bufsz)) ? (rbuf->buf) : (rbuf->fdb + bcnt);

}


/*
 *  DESCRIPTION: Adjust first data byte pointer according 'bcnt', wrapping pointer if need
 *
 *  INPUT:
 *	rbuf	- an address of the RING Buffer structure
 *	bcnt	- a requested size of the free space
 *
 *  OUTPUT:
 *	NONE
 *
 *  RETURN:
 *	NONE
 */

static	inline	void	rbuf_adjust_free	(
			BP_RING		*rbuf,
				int	bcnt
				)
{
	if ( rbuf->ffb != rbuf->fdb)
		{
		/* Sanity check */
		assert( (rbuf->ffb + bcnt) > (rbuf->buf + rbuf->bufsz) );
		}

	rbuf->ffb = ((rbuf->ffb + bcnt) == (rbuf->buf + rbuf->bufsz)) ? (rbuf->buf) : (rbuf->ffb + bcnt);

}

#ifdef __cplusplus
	}
#endif

#endif	/* __RING$DEF__ */
