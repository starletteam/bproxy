#!/bin/bash

#
#  Performs a startup action before run a main procedure.
#

	EXEDIR=$(cd $(dirname $0) && pwd)
	LOGDIR='/var/log/bproxy'

	$EXEDIR/bproxy_run.sh >$LOGDIR/bproxy.log &
	echo "PID of detached process is $!"

	exit 0
