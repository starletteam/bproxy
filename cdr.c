#define	__MODULE__	"CDR"
#define	__IDENT__	"X.00-01"

#ifdef	__GNUC__
	#pragma GCC diagnostic ignored  "-Wparentheses"
	#pragma	GCC diagnostic ignored	"-Wunused-variable"
#endif

#ifdef _WIN32
	#pragma once
	#define WIN32_LEAN_AND_MEAN             // Exclude rarely-used stuff from Windows headers
#endif

/*
**++
**
**  FACILITY:  BroProxy - A yet another SOCKS5(4), HTTP(s) high performance proxy server
**
**  DESCRIPTION: This module contains routines to saving accounting information - CDR (charge data record)
**
**  DESIGN ISSUE: {tbs}
**
**  AUTHORS: Ruslan R. (The BadAss SysMan) Laishev
**
**  CREATION DATE:  15-MAR-2019
**
**  MODIFICATION HISTORY:
**
**
**--
*/

#include	<stdio.h>
#include	<stdlib.h>
#include	<unistd.h>
#include	<errno.h>
#include	<fcntl.h>
#include	<memory.h>
#include	<arpa/inet.h>
#include	<time.h>
#include	<stdatomic.h>
#include	<pthread.h>
#include	<sys/stat.h>
#include	<sys/types.h>



#define		__FAC__	"BPROXY"
#define		__TFAC__ __FAC__ ": "		/* Special prefix for $TRACE			*/
#include	"utility_routines.h"

#include	"bproxy.h"

extern int	g_exit_flag, g_trace, g_statdelta;
extern ASC	q_csvcdr, q_t4stat, q_statfspec;





const char t4hdr [] =	"BroProxy - Stream accounting," CRLF \
			"%02d-%02d-%04d," CRLF \
			"%02d:%02d:%02d," CRLF ,

	t4rechdr [] =	"$$$ START COLUMN HEADERS $$$" CRLF \
			"Sample Date" CRLF \
			"Sample Time" CRLF \
			"User" CRLF \
			"Source IP" CRLF \
			"Destination Host[:port]" CRLF \
			"Destination IP" CRLF \
			"I/O count" CRLF \
			"In octets" CRLF \
			"Out octets" CRLF \
			"Last I/O Date" CRLF \
			"Last I/O Time" CRLF \
			"$$$ END COLUMN HEADERS $$$" CRLF ,

	t4rec []  =	"%02d-%02d-%04d, %02d:%02d:%02d, %.*s, %s, %.*s, %s, %llu, %llu, %llu, %02d-%02d-%04d, %02d:%02d:%02d" CRLF,

	csvhdr [] =	"Date;" \
			"Time;" \
			"User;" \
			"Source IP;" \
			"Destination Host[:port];" \
			"Destination IP;" \
			"In octets;" \
			"Out octets" CRLF,

	csvrec []  =	"%02d-%02d-%04d; %02d:%02d:%02d; %.*s; %s; %.*s; %s; %llu; %llu" CRLF;

static int	csvfd = -1, t4fd = -1;
static	char	csvname[NAME_MAX], t4name[NAME_MAX];

static const mode_t fileprot =  ( S_IREAD | S_IWRITE) | (S_IREAD >> 3) | (S_IREAD >> 6) ;

/*
 *
 */
static	int	t4_write	(
			char		*fn,
			int		fd,
			BP_STREAM	*stm
			)
{
struct tm cretm, endtm;
int	status, len;
struct	timespec stime = {0, 0};
char	buf[512], sia[32], dia[32];

	localtime_r((time_t *)&stm->cretim, &cretm);
	localtime_r((time_t *)&stm->lastio, &endtm);

	inet_ntop(stm->skin.sk.sin_family, &stm->skin.sk.sin_addr, sia, sizeof(sia));
	inet_ntop(stm->skout.sk.sin_family, &stm->skout.sk.sin_addr, dia, sizeof(dia));

	len = sprintf(buf, t4rec, cretm.tm_mday, cretm.tm_mon + 1, 1900 + cretm.tm_year,
		cretm.tm_hour, cretm.tm_min, cretm.tm_sec,
		$ASC(&stm->user), sia, $ASC(&stm->dhost), dia,
		stm->skin.iocnt, stm->skin.inbcnt, stm->skin.outbcnt,
		endtm.tm_mday, endtm.tm_mon + 1, 1900 + endtm.tm_year,
		endtm.tm_hour, endtm.tm_min, endtm.tm_sec);

	if ( len != (status = write(fd, buf, len)) )
		return	$LOG(STS$K_ERROR, "CSV - write(%s, %d octets)->%d, errno=%d", fn, len, status, errno);

	return	STS$K_SUCCESS;
}

/*
 *
 */
static	int	csv_write	(
			char		*fn,
			int		fd,
			BP_STREAM	*stm
			)
{
struct tm cretm;
int	status, len;
struct	timespec stime = {0, 0};
char	buf[512], sia[32], dia[32];

	localtime_r((time_t *)&stm->cretim, &cretm);

	inet_ntop(stm->skin.sk.sin_family, &stm->skin.sk.sin_addr, sia, sizeof(sia));
	inet_ntop(stm->skout.sk.sin_family, &stm->skout.sk.sin_addr, dia, sizeof(dia));

	len = sprintf(buf, csvrec, cretm.tm_mday, cretm.tm_mon + 1, 1900 +cretm.tm_year,
		cretm.tm_hour, cretm.tm_min, cretm.tm_sec,
		$ASC(&stm->user), sia, $ASC(&stm->dhost), dia,
		stm->skin.inbcnt, stm->skin.outbcnt);

	if ( len != (status = write(fd, buf, len)) )
		return	$LOG(STS$K_ERROR, "CSV - write(%s, %d octets)->%d, errno=%d", fn, len, status, errno);

	return	STS$K_SUCCESS;
}


static	int	csv_open	(
		char	*fmask,
		char	*fspec,
		int	*fd
			)
{
int	status, len;
struct	tm	_tm;

	$IFTRACE(g_trace, "Open/create CDR file '%s'", fmask);

	__util$timbuf (NULL, &_tm);

	sprintf(fspec, fmask, _tm.tm_year, _tm.tm_mon, _tm.tm_mday);

	if ( 0 > (*fd = open(fspec, O_WRONLY | O_CREAT, fileprot)) )
		return	$LOG(STS$K_ERROR, "Error open/create file '%s', errno=%d", fspec, errno);

	if ( 0 > (status = lseek(*fd, 0, SEEK_END)) )
		{
		close(*fd);
		return	$LOG(STS$K_ERROR, "Error seek to EOF in file '%s', errno=%d", fspec, errno);
		}

	$IFTRACE(g_trace, "CDR file '%s' - has been %s", fspec, !status ? "created" : "opened for append");

	if ( status )	/* Looks like file is not empty */
		return	STS$K_SUCCESS;

	/* File is just has been created - need write a header */
	$IFTRACE(g_trace, "Write header to CDR file '%s'",  fspec);

	len = sizeof(csvhdr) - 1;
	if ( len != (status = write(*fd, csvhdr, len)) )
		{
		close(*fd);
		return	$LOG(STS$K_ERROR, "Error write header to '%s', write(%d octets)->%d, errno=%d", fspec, len, status, errno);
		}

	return	STS$K_SUCCESS;
}


static	int	t4_open	(
		char	*fmask,
		char	*fspec,
		int	*fd
			)
{
int	status, len;
struct	tm	_tm;
char	buf[512];

	$IFTRACE(g_trace, "Open/create CDR file '%s'", fmask);

	__util$timbuf (NULL, &_tm);

	sprintf(fspec, fmask, _tm.tm_year, _tm.tm_mon, _tm.tm_mday);

	if ( 0 > (*fd = open(fspec, O_WRONLY | O_CREAT, fileprot)) )
		return	$LOG(STS$K_ERROR, "Error open/create file '%s', errno=%d", fspec, errno);

	if ( 0 > (status = lseek(*fd, 0, SEEK_END)) )
		{
		close(*fd);
		return	$LOG(STS$K_ERROR, "Error seek to EOF in file '%s', errno=%d", fspec, errno);
		}


	$IFTRACE(g_trace, "CDR file '%s' - has been %s", fspec, !status ? "created" : "opened for append");

	if ( status )	/* Looks like file is not empty */
		return	STS$K_SUCCESS;

	/* File is just has been created - need write a header */
	$IFTRACE(g_trace, "Write header to CDR file '%s'",  fspec);

	len = sprintf(buf, t4hdr, _tm.tm_mday, _tm.tm_mon,_tm.tm_year,
		_tm.tm_hour, _tm.tm_min, _tm.tm_sec);

	if ( len != (status = write(*fd, buf, len)) )
		{
		close(*fd);
		return	$LOG(STS$K_ERROR, "Error write header to '%s', write(%d octets)->%d, errno=%d", fspec, len, status, errno);
		}

	len = sizeof(t4rechdr) - 1;
	if ( len != (status = write(*fd, t4rechdr, len)) )
		{
		close(*fd);
		return	$LOG(STS$K_ERROR, "Error write header to '%s', write(%d octets)->%d, errno=%d", fspec, len, status, errno);
		}

	return	STS$K_SUCCESS;
}




int	cdr_init	(void)
{
int	status	= 0;

	if ( $ASCLEN(&q_csvcdr) && (csvfd == -1) )
		status = csv_open($ASCPTR(&q_csvcdr), csvname, &csvfd);

	if ( $ASCLEN(&q_t4stat) && (t4fd == -1) )
		status = t4_open($ASCPTR(&q_t4stat), t4name, &t4fd);

	return	status;
}


int	cdr_save	(BP_STREAM *stm)
{
int	status;

	if ( csvfd != -1 )
		status = csv_write(csvname, csvfd, stm);

	if ( t4fd != -1 )
		status = t4_write(t4name, t4fd, stm);

	return	status;
}

/*
 *   DESCRIPTION: Generate and write stat record to file, is supposed to be called at regular basis
 *
 *   IMPLICIT INPUT:
 *	g_statfspe, g_statdelta
 *
 *   RETURN:
 *	condition code
 */
BP_QUOTAS	quota_usage = {0};
static unsigned	inbcnt, outbcnt;
static struct timespec stat_tm = {0};
static ASC	user = {0};

int	stat_write	(BP_STREAM *stm)
{
int	status = 0, fd = -1;
struct timespec now;
// static unsigned	in, out;

	if ( !$ASCLEN(&q_statfspec) )
		return	STS$K_SUCCESS;

	atomic_fetch_add(&inbcnt, stm->skin.inbcnt);
	atomic_fetch_add(&outbcnt, stm->skin.outbcnt);

	user = stm->user;

//	in = atomic_load(&inbcnt);
//	out = atomic_load(&outbcnt);

	/* Compute an end of I/O operation time	*/
	if ( status = clock_gettime(CLOCK_REALTIME, &now) )
		return	$LOG(STS$K_ERROR, "clock_gettime()->%d, errno=%d", status, errno);


	if ( 0 > (fd = open($ASCPTR(&q_statfspec), O_WRONLY | O_CREAT | O_TRUNC, fileprot)) )
		return	$LOG(STS$K_ERROR, "Error open/create file '%s', errno=%d", $ASCPTR(&q_statfspec), errno);


	return	STS$K_SUCCESS;
}

int	stat_init	(void)
{
	atomic_store(&inbcnt, 0);
	atomic_store(&outbcnt, 0);

	return	STS$K_SUCCESS;
}
