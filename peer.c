#define	__MODULE__	"PEER"
#define	__IDENT__	"X.00-01"

#ifdef	__GNUC__
	#pragma GCC diagnostic ignored  "-Wparentheses"
	#pragma	GCC diagnostic ignored	"-Wunused-variable"
#endif

#ifdef _WIN32
	#pragma once
	#define WIN32_LEAN_AND_MEAN             // Exclude rarely-used stuff from Windows headers
#endif

/*
**++
**
**  FACILITY:  BroProxy - A yet another SOCKS5(4), HTTP(s) high performance proxy server
**
**  DESCRIPTION: This module contains routines to cover : loading and process BroProxy's Peer configuration file,
**	process and validate configuration options, build peer's list, set peer selection strategy based on the configuration option 'usage'.
**
**  DESIGN ISSUE: {tbs}
**
**  AUTHORS: Ruslan R. (The BadAss SysMan) Laishev
**
**  CREATION DATE:  21-MAY-2019
**
**  MODIFICATION HISTORY:
**
**--
*/

#include	<stdlib.h>
#include	<stdio.h>
#include	<errno.h>
#include	<strings.h>
#include	<unistd.h>
#include	<sys/types.h>
#include	<sys/stat.h>
#include	<pthread.h>
#include	<arpa/inet.h>

#define		__FAC__	"BPROXY"
#define		__TFAC__ __FAC__ ": "		/* Special prefix for $TRACE			*/
#include	"utility_routines.h"


#include	"bproxy.h"


extern int	g_exit_flag,
		g_trace;


static BP_PEER	*peer_data = NULL;
static int	peer_data_count = 0,
		peer_usage = BP$K_FALLBACK;
static	time_t	peer_mtime = {0};
pthread_rwlock_t peer_lock = PTHREAD_RWLOCK_INITIALIZER;


/*
 *   DESCRIPTION: Read and parse line-by-line file
 *	*beeline*
 *	*yandex.ru
 *	*yand%%.ru
 *	skip empty or unrecognized/illformed lines.
 *
 *   INPUT:
 *	dbsource:	File specification to be processed
 *
 *   IMPLICIT OUTPUT:
 *	ban_data, ban_data_count
 *
 *   RETURN:
 *	condition code
 */
int	peer_load	(
		ASC	*dbsource
			)
{
int	status = 0, count, i, lineno;
FILE	*finp;
char	buf[128], p1[ASC$K_SZ], p2[ASC$K_SZ], p3[ASC$K_SZ], p4[ASC$K_SZ], pname[32], c;
BP_PEER	*ppeer, *ppeer_data = NULL;
struct	stat	sb = {0};
struct	timespec stime = {0, 0}, etime = {0, 0}, delta = {10, 0};

	if ( !$ASCLEN(dbsource) )
		return	STS$K_SUCCESS;

	/* Check firstly that the ban file is newer then local context */
	if ( 0 > (stat($ASCPTR(dbsource), &sb)) )
		$LOG(STS$K_ERROR, "PEER - Error access file '%.*s', errno=%d", $ASC(dbsource), errno);

	if ( !(sb.st_mtim.tv_sec >  peer_mtime) )
		return	STS$K_SUCCESS;

	if ( peer_mtime )
		$LOG(STS$K_INFO, "PEER - Update PEERs from '%.*s'", $ASC(dbsource) );

	if ( !(finp = fopen ($ASCPTR(dbsource), "r")) )
		return	$LOG(STS$K_ERROR, "PEER - Error open '%.*', errno=%d", $ASC(dbsource), errno);

	/* Allocate memory for new PEER's list s */
	 if ( !(ppeer_data = calloc(BP$K_MAXPEER, sizeof(BP_PEER))) )
		 {
		 fclose(finp);
		 return	$LOG(STS$K_ERROR, "PEER - calloc(), errno=%d",  errno);
		 }

	 for ( lineno = 1, count = 0, ppeer = ppeer_data; count <= BP$K_MAXPEER && !feof(finp); lineno++)
		{
		/* Get string from file */
		fgets(buf, sizeof(buf), finp);

		if ( !(status = __util$uncomment (buf, strnlen(buf, sizeof(buf)), '!')) )
			continue;

		if ( !(status = __util$collapse (buf, strnlen(buf, sizeof(buf)))) )
			continue;

		buf[status] = '\0';
		pname[0] = 0;
		p1[0] = 0;

		/* Try to catch USAGE=FB|RR|LB */
		if ( 2 == (status = sscanf(buf, "%5[^=]=%2[A-Za-z]", pname, p1)) )
			{
			if ( !strncasecmp(pname, "usage", 5) )
				{
				c = toupper(p1[0]);

				if ( c == 'F' )
					{
					peer_usage = BP$K_FALLBACK;
					continue;
					}
				else if ( c == 'R' )
					{
					peer_usage = BP$K_ROUNDROBIN;
					continue;
					}
				else if ( c == 'L' )
					{
					peer_usage = BP$K_LOADBALANCE;
					continue;
					}
				else	{
					$LOG(STS$K_ERROR, "PEER - Unrecognized 'USAGE' keyword '%s', lineno #%d", pname, lineno);
					continue;
					}
				}
			}

		/*
		 * PEER=172.16.1.1, 4,broBudd:MyCoolPa$$vv0r9
		 * PEER=172.16.0.250:4343, H
		 */
		pname[0] = 0;
		p1[0] = p2[0] = p3[0] = p4[0] = 0;

		//if ( 2 == (status = sscanf(buf, "%4[^=]=%17[\-\_A-Za-z0-9\.\*\%]", param, sarg)) )
		if ( 2 <= (status = sscanf(buf, "%4[^=]=%253[^,],%253[^,],%253[^,]", pname, p1, p2, p3)) )
			{
			if ( strncasecmp(pname, "peer", 4) )
				continue;

			c = toupper(p2[0]);
			switch ( c )
				{
				case	0:
				case	'H':	ppeer->proto = BP$K_PROTO_HTTP;
						break;
				case	'S':	ppeer->proto = BP$K_PROTO_HTTPS;
						break;
				case	'4':	ppeer->proto = BP$K_PROTO_SOCKS4;
						break;
				case	'5':	ppeer->proto = BP$K_PROTO_SOCKS5;
						break;
				default:
					$LOG(STS$K_ERROR, "PEER - Unrecognized PEER's proto '%s', lineno #%d", p2, lineno);
					continue;
				}

			/* Copy username:password pair AS IS */
			__util$str2asc (p3, &ppeer->auth);

			p2[0] = p3[0] = 0;

			/* Process IP[:port] ... */
			if ( !sscanf(p1, "%128[^:]:%[0-9]", p2, p3) )
				{
				$LOG(STS$K_ERROR, "PEER - Ilformed PEER's IP:[port] field (%s), lineno #%d", p1, lineno);
				continue;
				}

			if ( 1 != inet_pton(ppeer->sk.sin_family = AF_INET, p2, &ppeer->sk.sin_addr) )
				{
				$LOG(STS$K_ERROR, "PEER - Cannot convert '%s', lineno #%d", p2, lineno);
				continue;
				}

			i = atoi(p3);
			ppeer->sk.sin_port = htons( i ? i : BP$K_DEFPORT);


			count++;
			ppeer++;
			}
		}

	fclose(finp);

	$IFTRACE(g_trace, "USAGE=%c", peer_usage);

	for ( ppeer = ppeer_data, i = count; g_trace && (i--); ppeer++ )
		{
		inet_ntop(ppeer->sk.sin_family, &ppeer->sk.sin_addr, p1, sizeof(p1));
		$IFTRACE(g_trace, "PEER %s:%d, %d, %.*s", p1, ntohs(ppeer->sk.sin_port), ppeer->proto, $ASC(&ppeer->auth) );
		}

	/*
	 * Acuire exclusive access to the BAN ACEs stuff
	 */
	if ( status = clock_gettime(CLOCK_REALTIME, &stime) )
		{
		free(ppeer_data);
		return	$LOG(STS$K_ERROR, "PEER - clock_gettime->%d", status);
		}

	__util$add_time (&stime, &delta, &etime);

	if ( status = pthread_rwlock_timedwrlock(&peer_lock, &etime) )
		{
		free(ppeer_data);
		return	$LOG(STS$K_ERROR, "PEER - pthread_rwlock_timedwrlock()->%d, errno=%d", status, errno);
		}

	if ( peer_data )
		free(peer_data);

	/* Save modification time for future checking */
	peer_mtime = sb.st_mtim.tv_sec;

	peer_data = ppeer_data;
	peer_data_count = count;

	if ( status = pthread_rwlock_unlock(&peer_lock) )
		$LOG(STS$K_WARN, "PEER - pthread_rwlock_unlock()->%d, errno=%d", status, errno);

	return	$LOG(STS$K_SUCCESS, "PEER - %d PEERs has been loaded", peer_data_count);
}



/*
 *   DESCRIPTION: Select a peer according current 'usage' discipline. Adjust load counters and so on ....
 *
 *   OUTPUT:
 *	peer:	an address of buffer to accept peer's data
 *
 *   RETURN:
 *	condition code
 */
int	peer_get(
	BP_PEER	*peer
		)
{
	if ( peer_data_count )
		{
		*peer = *peer_data;
		return	STS$K_SUCCESS;
		}

	return	STS$K_ERROR;
}
