#define	__MODULE__	"STM"
#define	__IDENT__	"X.00-04"

#ifdef	__GNUC__
	#pragma GCC diagnostic ignored  "-Wparentheses"
	#pragma	GCC diagnostic ignored	"-Wdate-time"
	#pragma	GCC diagnostic ignored	"-Wunused-variable"
#endif


#ifdef _WIN32
	#pragma once
	#define WIN32_LEAN_AND_MEAN             // Exclude rarely-used stuff from Windows headers
#endif


/*
**++
**
**  FACILITY:  BroProxy - A yet another SOCKS5(4), HTTP(s) high performance proxy server
**
**  DESCRIPTION: This module contains routines related to the stream pool maintenance.
**
**  AUTHORS: Ruslan R. (The BadAss SysMan) Laishev
**
**  CREATION DATE:  22-FEB-2019
**
**  MODIFICATION HISTORY:
**
**	13-MAR-2019	RRL	Redesigned.
**
**	 3-FEB-2019	RRL	Correct defaults in the stm_params vector.
**
**	24-MAR-2020	RRL	Recoded a logic of the stm_get(): now we wait for some timeout for free context.
**--
*/

#include	<stdlib.h>
#include	<string.h>
#include	<stdio.h>
#include	<errno.h>
#include	<pthread.h>
#include	<stdatomic.h>


#define		__FAC__	"BPROXY"
#define		__TFAC__ __FAC__ ": "		/* Special prefix for $TRACE			*/
#include	"utility_routines.h"

#include	"bproxy.h"


extern	int	g_trace, g_iobufsz;

static __QUEUE	g_stm_pool = QUEUE_INITIALIZER;

//static BP_DPOOL g_stm_params = {.def = 1024, .min = 512, .max = 8192, .inc = 128, .dec = 32, .delta = 10, .hrate = 128, .lrate = 32};
static BP_DPOOL g_stm_params = {.def = 128};
pthread_mutex_t	g_stm_lock = PTHREAD_MUTEX_INITIALIZER;
pthread_cond_t	g_stm_cond = PTHREAD_COND_INITIALIZER;


static struct timespec	ts_last_zero,		/* A time stamp of last I/O threads crew adjustment */
		ts_delta = {10, 1};		/* Delta time in the timespec format */
static atomic_int	g_stm_count = 0,	/* Current size of the STREAM's pool */
		g_cnt_stm_hit = 0;		/* A number of request of STREAM context since last zero */

static inline int	__stm_alloc	(
		BP_STREAM	**stm,
			int	bufsz
			)
{
int	memsz;

	memsz = bufsz + sizeof(BP_STREAM);
	memsz += 511;
	memsz = (memsz / 512) * 512;

	if ( !(*stm = calloc (1, memsz)) )
		return	$LOG(STS$K_ERROR, "Cannot allocate %d octets for stream context, errno=%d", memsz, errno);

	return	STS$K_SUCCESS;
}


int	stm_get		(
		BP_STREAM	**stm
			)
{
int	status, count;
struct	timespec stime = {0, 0}, etime = {0, 0}, delta = {g_stm_params.delta, 0};

	/* Adjust request counter, it's supposed to be used in the
	 * auto-adjustment of STREAM pool
	 */
	atomic_fetch_add(&g_cnt_stm_hit, 1);

	/* Try to get conext ... */
	if ( 1 & (status = $REMQHEAD(&g_stm_pool, stm, &count)) )
		if ( count )
			return	STS$K_SUCCESS;

	/* No free context in the pool, so we will wait for releasing this one */
	if ( status = clock_gettime(CLOCK_REALTIME, &stime) )
	       return	$LOG(STS$K_ERROR, "clock_gettime->%d", status);

	__util$add_time (&stime, &delta, &etime);

       if ( status = pthread_mutex_lock(&g_stm_lock) )
		return	$LOG( STS$K_ERROR, "pthread_mutex_lock()->%d, errno=%d", status, errno);


	$LOG(STS$K_WARN,  "Wait %d seconds for free STREAM context ... ",  g_stm_params.delta);

	status = pthread_cond_timedwait(&g_stm_cond, &g_stm_lock, &etime);

	if ( status && (status != ETIMEDOUT) )
		status = $LOG( STS$K_ERROR, "pthread_cond_timedwait()->%d, errno=%d", status, errno);
	else if ( status == ETIMEDOUT )
		status = $LOG( STS$K_ERROR, "Cannot allocate a STREAM context in %d second!", g_stm_params.delta);

	/* Try to get conext ... */
	if ( 1 & (status = $REMQHEAD(&g_stm_pool, stm, &count)) )
		status = count ? STS$K_SUCCESS : STS$K_ERROR;

	if ( count = pthread_mutex_unlock(&g_stm_lock) )
		$LOG( STS$K_ERROR, "pthread_mutex_unlock()->%d, errno=%d", count, errno);

	if ( !(1 & status) )
		$LOG( STS$K_ERROR, "Cannot allocate a STREAM context in %d second!", g_stm_params.delta);

	return	status;
}


int	stm_free	(
		BP_STREAM	*stm
			)
{
int	status, count;

	/* Insert into the double-linked list */
	$INSQTAIL(&g_stm_pool, stm, &count);

	/* Send signal to wake-up other waiter for free STREAM context ! */
	if ( status = pthread_cond_signal(&g_stm_cond) )
		$LOG( STS$K_ERROR, "pthread_cond_signal()->%d, errno=%d", status, errno);

	return	STS$K_SUCCESS;
}


int	stm_pool_init	(
		ASC	*	params,
			int	bufsz
			)
{
int	i, count;
BP_STREAM *stm;
BP_DPOOL *p = &g_stm_params;

	sscanf ($ASCPTR(params), "%d,%d,%d,%d,%d,%d,%d,%d", &p->def, &p->min, &p->max, &p->inc, &p->dec,
		&p->delta, &p->hrate, &p->lrate);

	$LOG(STS$K_INFO, "Create streams pool (sz=%d octets, def/min/max/inc/dec/delta/hrate/lrate=%d/%d/%d/%d/%d/%d/%d/%d)",
		bufsz, p->def, p->min, p->max, p->inc, p->dec,
		p->delta, p->hrate, p->lrate);

	/* Create context pool with 'default' quantity of buffers */
	for ( i = p->def; i; i--)
		{
		if ( !(1 & __stm_alloc(&stm, bufsz)) )
			break;

		/* Insert into the double-linked list */
		$INSQTAIL(&g_stm_pool, stm, &count);
		atomic_fetch_add(&g_stm_count, 1);
		}

	if ( i )
		return	$LOG(STS$K_ERROR, "Error creating STREAM pool!");

	return	$LOG(STS$K_SUCCESS, "Created streams context pool with %d entries", atomic_load(&g_stm_count));
}


/*
 *   DESCRIPTION: I/O processor threads manager - analyze counters, start new threads, adjust target thread's quantity
 *	according DPOOL's configuration vector. This routine is supposed to be called at regular basis with interval has been
 *	defined in the configuration vector.
 *
 *
 *   IMPLICIT INPUT:
 *
 *   IMPLICIT OUTPUT:
 *
 *   RETURN:
 *	condition code
 */
int	stm_pool_manager	(void)
{
struct timespec now = {0}, etime = {0};
int	status, rate, cur, tg, count;
BP_STREAM	*stm;

	/* Is there auto-adjustment is active ? */
	if ( (!g_stm_params.delta) || ((!g_stm_params.hrate) && (!g_stm_params.lrate)) )
		return	STS$K_SUCCESS;

	/* Is we reach an end of interval of checking ?*/
	clock_gettime(CLOCK_REALTIME, &now);

	__util$add_time(&ts_last_zero, &ts_delta, &etime);

	if ( 0 > __util$cmp_time (&now, &etime) )
		return	STS$K_SUCCESS;

	/* Performs rate computaion and checks */
	rate = atomic_load(&g_cnt_stm_hit);
	cur = atomic_load(&g_stm_count);

	$IFTRACE(g_trace, "Free/Current/requested(in %d seconds)=%d/%d/%d", g_stm_params.delta, g_stm_pool.count, cur, rate);
	//rate /= ts_delta.tv_sec;

	/* Reset hit counter, reset last zero time */
	atomic_store(&g_cnt_stm_hit, 0);
	clock_gettime(CLOCK_REALTIME, &ts_last_zero);

	/* Check that current rate is between <lowrate> and <highrate> */
	if ( __util$isinrange  (rate, g_stm_params.lrate, g_stm_params.hrate) )
		return	STS$K_SUCCESS;	/* Nothing to do !*/

	if ( rate > g_stm_params.hrate )
		tg = cur + g_stm_params.inc;
	else if ( rate < g_stm_params.lrate )
		tg = cur - g_stm_params.dec;
	else	return	STS$K_SUCCESS;

	tg = __util$range (tg, g_stm_params.min, g_stm_params.max);

	if ( cur == tg )	/* Nothing to do ! Just exit */
		return	STS$K_SUCCESS;

	$IFTRACE(g_trace, "Current/target=%d/%d", cur, tg);

	/* Do we need allocate additional STREAM context  ? */
	if ( cur < tg )
		{
		for ( count = tg - cur; count; count--)
			{
			if ( !(1 & __stm_alloc(&stm, g_iobufsz)) )
				break;

			$INSQTAIL(&g_stm_pool, stm, &status);
			atomic_fetch_add(&g_stm_count, 1);
			}

		$IFTRACE(g_trace, "Allocated=%d", tg - cur);
		}

	/* Do we need to reduce a number of STREAM context in the pool ? */
	else if ( cur > tg )
		{
		for ( count = cur - tg; count; count--)
			{
			if ( !(1 & stm_get(&stm)) )
				break;

			free(stm);
			atomic_fetch_sub(&g_stm_count, 1);
			}

		$IFTRACE(g_trace, "Released=%d", cur - tg);
		}

	$IFTRACE(g_trace, "Current=%d", atomic_load(&g_stm_count));

	return	STS$K_SUCCESS;
}
