#ifndef	__BPROXY$DEF__
#define	__BPROXY$DEF__	1

#ifdef	__GNUC__
	#pragma GCC diagnostic ignored  "-Wparentheses"
	#pragma	GCC diagnostic ignored	"-Wdate-time"
	#pragma	GCC diagnostic ignored	"-Wunused-variable"
#endif


#ifdef _WIN32
	#pragma once
	#define WIN32_LEAN_AND_MEAN             // Exclude rarely-used stuff from Windows headers
#endif

/*
**++
**
**  FACILITY:  BroProxy - A yet another SOCKS5(4), HTTP(s) high performance proxy server
**
**  DESCRIPTION: This module contains data structures and constant definition is supposed to be used
**	in the BroProxy server modules.
**
**  AUTHORS: Ruslan R. (The BadAss SysMan) Laishev
**
**  CREATION DATE:  21-FEB-2019
**
**  MODIFICATION HISTORY:
**
**	 1-MAY-2019	RRL	Added .iobuf in to the BP_STREAM.
**
**	31-MAY-2019	RRL	Added routines declaration.
**
**	 6-JUN-2020	RRL	Increased TCP Backlog parameter from 32 to 128.
**
**	16-JUN-2020	RRL	Changed TCP Backlog parameter back to 32
**
**	 5-FEB-2021	RRL	Added constant BP$K_PROTO_PRIVATE to designated an custom-specific protocol betwwen BtoProxy and BroAgent
**
**	28-MAR-2021	RRL	Added constant definitions to help encrypt/decrypt data traffic between master and agent.
**
**	29-JUN-2021	RRL	Added protypes.
**
**--
*/

#ifndef WIN32
#include		<netinet/in.h>
#endif // !WIN32

#include		<time.h>

#include		"utility_routines.h"

#ifdef __cplusplus
extern "C" {
#endif

#define	BP$K_IOBUFSZ	(32*1024)	/* Default buffer size of the I/O */
#define	BP$K_IOMAXEVTS	8192
#define	BP$K_TCPBACKLOG	32
#define	BP$SZ_USER	32
#define	BP$SZ_PASS	32
#define	BP$K_MAXACE	128		/* A number of IP ACE, see auth.c	*/
#define	BP$K_MAXBAN	128		/* A number of Site ACE, see ban.c	*/
#define	BP$K_HTTP_PORT	80
#define	BP$K_HTTPS_PORT	443
#define	BP$K_MAXNS	16		/* Maximum number of NS in configuration*/
#define	BP$K_MAXPEER	16		/* Maximum number of peer proxy		*/
#define	BP$K_DEFPORT	3128		/* A default port of peer proxy		*/


#define	BP$SZ_MINLEN	3		/* Minimum octets to be read from sockets buffer to performs
					recognizing protocol. At the moment it's fixed part of the SOCKS5
					protocol */



					/* Macro to convert ident string to binary
					 * representative to perform comparing and so on ...
					 *  X.vv-ss[ECOff]
					 *	X - Code
					 *	vv- Main version number
					 *	ss- Subversion number
					 *	rr- fix number
					 */
#define	$VERSION(code, ver,  subver, eco)	(((code)<<24)|((ver)<<16)|((subver)<<8)|(eco))

enum	{
	/* Incoming protocol */
	BP$K_PROTO_SOCKS4 = 4,
	BP$K_PROTO_SOCKS5 = 5,
	BP$K_PROTO_HTTP,
	BP$K_PROTO_HTTPS,

	BP$K_PROTO_PRIVATE,		/* Special encapsulation for obsucation of trafic */
};

enum	{
	/* Data buffer processing flags */
	BP$K_CRYPTNON = 0,		/* Data should be re-send AS-IS */
	BP$K_DECRYPT = 1,		/* Decrypt received data - send plain */
	BP$K_ENCRYPT = 2		/* Received plain - encrypt - send */
};


enum	{
	BP$K_AUTH_UNDEF = -1,
	BP$K_AUTH_LOGIN = 1,
	BP$K_AUTH_IP = 2
};




enum	{
	/* SOCKS4/5 Command/request codes	*/
	BP$K_SOCKS_CONN = 1 ,
	BP$K_SOCKS_BIND ,
	BP$K_SOCKS_ASGN ,

	/* SOCKS4 Answers */
	BP$K_SOCKS4_OK = 0x5a ,		/* Request has been successfully processed */
	BP$K_SOCKS4_ERR ,		/* Request illegal or rejected */
	BP$K_SOCKS4_NA ,		/* IdentD is not avaiable */
	BP$K_SOCKS4_NOAUTH ,		/* Authenciation error */

	/* SOCKS5 Authentication methods */
	BP$K_SOCKS_NOAUTH = 0 ,		/* No authenticaion is required */
	BP$K_SOCKS_GSSAPI ,		/* GSSAPI ... */
	BP$K_SOCKS_PLAIN ,		/* Username/password pair	*/
	BP$K_SOCKS5_BADAUTH = 0xFF,	/* No common authentication method */
	BP$K_SOCKS5_VERAUTH = 1,	/* The VER field contains the current version of the subnegotiation, which is X'01' */

	/* SOCKS5 Answers */
	BP$K_SOCKS5_OK = 0,
	BP$K_SOCKS5_ERR,
	BP$K_SOCKS5_NOAUTH,
	BP$K_SOCKS5_BADNET,
	BP$K_SOCKS5_BADHOST,
	BP$K_SOCKS5_REJ,
	BP$K_SOCKS5_TTL,
	BP$K_SOCKS5_BADREQ,
	BP$K_SOCKS5_BADADDR,

	/* SOCKS5 Address format */
	BP$K_SOCKS5_IP4 = 1,
	BP$K_SOCKS5_FQDN = 3,
	BP$K_SOCKS5_IP6 = 4
};


enum {
	HTTP$K_407 = 407,
	HTTP$K_403 = 403,
	HTTP$K_404 = 404,
	HTTP$K_200 = 200
};


#pragma	pack	(push, 1)


typedef struct	__bp_socks4__
	{
	unsigned char	proto,
			cmd;
	unsigned short	dpn;
	unsigned	dia;

	unsigned char	user[0];
} BP_SOCKS4;



typedef struct	__bp_socks5__
	{
#define	BP$SZ_SOCKS5_ANS	2

	unsigned char	proto,
			cmd,


			rsv;

	unsigned char	atyp;

	union	{
		struct in_addr	dia;
		unsigned char	sdia[0];
		struct in6_addr	dia6;
	};

	unsigned short	dpn;
} BP_SOCKS5;


#pragma	pack	(pop)

#pragma	pack	(push, 4)

/* A structure of cache entry is supposed to be used in the NS.C to keep DNS RR */
typedef	struct	__bp_nsentry__{
			int	af;		/* AF_INET, AF_INET6	*/
			int	ttl;		/* DNS's RR TTL		*/
			int	cretm;		/* Record create time	*/

			union {
				struct	in_addr	ip;
				struct	in6_addr ip6;
			};

			ASC	name;
} BP_NSENTRY;

typedef struct	__bp_dpool__ {
			int	def,		/* Default quantity of	..	*/
				min,		/* Minimum quantity ...		*/
				max,		/* Maximum ...			*/
				inc,
				dec,
				delta,
				hrate,
				lrate;
} BP_DPOOL;



typedef	struct	__bp_lsnr__ {
	int			lock;

	struct sockaddr_in	lsnr;	/* IP:PORT to listen on		*/
	int			sd;	/* Socket descriptor		*/
} BP_LSNR;

typedef	struct	__bp_ext__ {
	int			lock;

	struct sockaddr_in	tunx;	/* To bind outgoing socket	*/
	int			sd;	/* Socket descriptor		*/
} BP_EXT;

typedef struct	__bp_sock__ {
		int		sd;
	struct sockaddr_in	sk;

	unsigned long long	iocnt,
				inbcnt,
				outbcnt;
} BP_SOCK;


typedef struct	__bp_stream__ {
		ENTRY		ent;
	pthread_mutex_t		mtx;
		unsigned	proto;	/* See BP$K_PROTO_* constants	*/

		unsigned	events;	/* Copy of the events mask from epoll() */

		BP_SOCK		skin;	/* Client's socket		*/
		BP_SOCK		skout;	/* Server socket		*/

	struct timespec		cretim,	/* Stream creation date&time	*/
				lastio;	/* Last I/O time stamp		*/

	ASC			dhost,	/* A destination host specification	*/
				user;	/* User if authorization is == LOGIN	*/

		char		iobuf[0]; /* A place holder of the I/O buffer area */
} BP_STREAM;




/* PEER Proxy manage discipline code */
enum	{
	BP$K_FALLBACK = 'F',
	BP$K_ROUNDROBIN = 'R',
	BP$K_LOADBALANCE = 'L'
};

/* Chained proxy parameters entry structure */
typedef struct	__bp_peer__	{
		ENTRY		ent;
	pthread_mutex_t		mtx;
		int		load;	/* PEER's usage algorithm	*/
		unsigned	proto;	/* See BP$K_PROTO_* constants	*/
		ASC		auth;	/* username:password pair	*/
	struct sockaddr_in	sk;

} BP_PEER;

#pragma	pack	(pop)


#define	BP$M_ACE_ACCEPT	1
#define	BP$M_ACE_REJECT	2
#define	BP$M_ACE_USER	4				/* Account record is USER */
#define	BP$M_ACE_AGENT	8				/* Account record is for AGENT */

typedef	union __bp_ban_ace__	{
	int		flags;	/* See BP$K_ACE_*	*/
	ASC		site;
} BP_BAN_ACE;


typedef	struct __bp_auth_data__	{
	int		flags;	/* See BP$K_ACE_*	*/

	union {
		struct	{
			ASC	user,
				pass;
		};

	struct	{
		in_addr_t	net,
				mask;
		};
	};
} BP_AUTH_DATA;




/* Structure to keep quota limits */
typedef	struct __bp_quotas__	{

	int	inband,		/* Input bandwidth quota		*/
		outband,	/* Output bandwith quota		*/
		conn;		/* Incomming connections quota limit	*/

	unsigned long long	traffic;

} BP_QUOTAS;


/* Structure to keep timers information */
typedef	struct __bp_timers__	{

	struct timespec	t_seq,	/* Interval to get initial mimimal length sequence to recognize protocol */
			t_req,	/* Interval to get whole request (HTTP's header)	*/
			t_conn,	/* Interval to connect destination host	*/
			t_xmit;	/* Interval to xmit has been received portion of the data */

} BP_TIMERS;



/* Routines declaration section	*/

inline static int timespec2msec (
		struct timespec *src)
{
	return (src->tv_sec  * 1024) + (src->tv_nsec / 1024);
}


int	auth_init (ASC *, ASC *);
int	auth_check (ASC *auth, struct in_addr *ia, int *flags);
int	auth_check2 (ASC *user, ASC *pass, int *flags);
int	auth_uname_check (ASC *user);

int	stm_pool_init (ASC *, int);
int	stm_get	(BP_STREAM **stm);
int	stm_free(BP_STREAM *stm);
int	stm_pool_manager (void);

int	io_crew_init (ASC *);
int	io_crew_manager	(void);
int	io_enq	(int , struct sockaddr_in *, int , struct sockaddr_in *, ASC *, ASC *, int proto);
int	io_deq	(BP_STREAM *stm);

int	rq_crew_init(ASC *, ASC *);
int	rq_crew_manager	(void);


int	ban_load (ASC *);
int	ban_check (ASC *site, struct in_addr *ia);

int	quota_load (ASC *);
int	cdr_init(void);
int	cdr_save (BP_STREAM *stm);

int	peer_load(ASC *);
int	peer_get(BP_PEER	*peer);

int	xmit_n (int sd, void *buf, int bufsz);
int	recv_n (int sd, void *buf, int bufsz, struct timespec	*delta);

int	ns_init	(void**nsctx);
int	ns_name2ip(void *nsctx, char *name, char *service, struct in_addr *addr);
int	ns_cleanup (void *nsctx);


int	__connect_by_sock	(struct	sockaddr_in *sk, int *sd);

int	agent_ctl_open (char *bufp, int buflen, int sd, struct sockaddr_in *sk, int proto);
int	agent_ctl_check (void);
int	agent_ctl_shut (void);

int	req_to_agent   (void *bufp, int buflen, int sd, struct sockaddr_in *sk);
int	ans_from_agent (void *bufp, int buflen, int sd, struct sockaddr_in *sk, ASC *rqid, int proto);



#ifndef ANDROID
#ifndef  __APPLE__
/* A simple replacement of the memmem() routine from POSIX */
static	inline	char *__memmem	(
			char	*haystack,
			size_t	hlen,
			char	*needle,
			size_t	nlen
				)
{
char	*hlimit = haystack + hlen - nlen + 1;

	if ( !nlen )
		return haystack;/* degenerate edge case */

	if ( hlen < nlen )
		return 0;	/* another degenerate edge case */

	while ( haystack = memchr(haystack, *needle, hlimit - haystack) )
		{
		if ( !memcmp(haystack, needle, nlen) )
			return haystack;

		haystack++;
		}


	return 0;
}
#endif  //__APPLE__
#endif // ANDROID



/*
 *   DESCRIPTION:  Find first occurent of the HTTP's field/attribute in the given buffer,
 *	copy attribute's value into the destination buffer.
 *
 *   INPUT:
 *	bufp:	buffer wth HTTP header
 *	buflen:	A length of the HTTP header
 *	fld:	HTTP's sield string
 *	fldlen:	A length of the HTTP's field string
 *
 *   OUTPUT:
 *	dst:	A buffer to accept value of the HTTP's field
 *
 *   RETURNS:
 *	condition code
 */

static inline int	__http_get_field(
			char	*bufp,
			int	 buflen,
			char	*fld,
			int	 fldlen,
			ASC	*dst
				)
{
char	*cp1, *cp2;
int	len;

	/* Scan for first occurece of the field name string */
	if ( !(cp1 = __memmem(bufp, buflen, fld, fldlen)) )
		return	STS$K_ERROR;

	/* Advance cp1 to begin og the field's value */
	cp1  += fldlen;

	/* Scan for termination sequence of value string,
	 * compute a length of the value string
	 */
	if ( (cp2 = __memmem(cp1, cp1 - bufp, "\r\n", 2)) )
		len = cp2 - cp1;
	else	len = buflen - (bufp - cp1);

	$ASCLEN(dst) = (unsigned char) (len);
	memcpy ($ASCPTR(dst), cp1, len);

	dst->sts[len] = '\0';

	return	STS$K_SUCCESS;
}





/*
 *   DESCRIPTION:  Performs a relaying data from one socket to other.
 *
 *   INPUT:
 *	iobuf:	A buffer for I/O
 *	skin:	A descriptor of source data
 *	skout:	Descriptor for receipient of data
 *	endec:	A flag to encrypt/decrypt data buffer
 *
 *   OUTPUT:
 *	NONE
 *
 *   RETURNS:
 *	condition code
 */
static inline	void __swap_nibbles__ (
				void	*buf,
				int	buflen
				)
{
unsigned char *pbuf;

	/* Swap nibbles of octet high <-> low */
	for (pbuf = (unsigned char*)buf; buflen--; pbuf++)
		*pbuf = (*pbuf >> 4) | (*pbuf << 4);
}


#ifdef __cplusplus
	}
#endif

#endif	/* #ifndef	__BPROXY$DEF__	*/
