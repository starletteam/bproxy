#define	__MODULE__	"REQ"
#define	__IDENT__	"X.00-30"

#ifdef	__GNUC__
	#pragma GCC diagnostic ignored  "-Wparentheses"
	#pragma	GCC diagnostic ignored	"-Wunused-variable"
	#pragma	GCC diagnostic ignored	"-Wunused-parameter"
	#pragma	GCC diagnostic ignored	"-Wdiscarded-qualifiers"
#endif

#ifdef _WIN32
	#pragma once
	#define WIN32_LEAN_AND_MEAN             // Exclude rarely-used stuff from Windows headers
#endif

/*
**++
**
**  FACILITY:  BroProxy - A yet another SOCKS5(4), HTTP(s) high performance proxy server
**
**  DESCRIPTION: This module contains routines to implement request processing.
**
**  DESIGN ISSUE: Get mininal octet sequence wich is enough to recognize a protocol (HTTP/HTTPs/SOCKS4/SOCKS5)
**		Dispatch processing to corresponding routine
**		Performs protocol's specific handshaking and error diagnoze
**		Establishing outgoing connection
**		Place new stream context into the Stream pool to be handled by I/O processor
**
**
**  AUTHORS: Ruslan R. (The BadAss SysMan) Laishev
**
**  CREATION DATE:  4-MAR-2019
**
**  MODIFICATION HISTORY:
**
**	20-APR-2019	SYS	Fixed incorrect final answer in the SOCKS5 processor routine.
**
**	21-APR-2019	RRL	Correct handling of HTTP' CONNECT request.
**
**	23-APR-2019	RRL	Recoded to use our own routines to resolving Name-to-IP (see NS.C)
**
**	26-APR-2019	RRL	Move HTTP's request processing to separated routine.
**
**	29-APR-2019	RRL	Redesigned code to use thread's specific context for NS stuff;
**
**	30-APR-2019	RRL	Added binding of outgoing socket to specified IP address;
**				Fixed SIGPIPE on send();
**
**	 1-MAY-2019	RRL	Changed I/O timeout handling in the __xmit_n();
**				fixed buug with time computation;
**
**	24-MAY-2019	RRL	Fixed incorrect sending of 200 in case of using proxy peers.
**
**	 3-JUN-2019	RRL	Fixed bug with sending extra CRLF-s;
**				support master-agent mode;
**
**	 2-DEC-2019	RRL	X.00-11 : Connection back mode: send answer (200 or 504) to browser depending of condition of
**				processing request with Agent.
**
**	25-DEC-2019	RRL	X.00-12 : Fixed logical bug in the __connect() routine: is used default TCP port number
**				instead of port has been  extracted from target URL in case if target host is name.
**
**	30-DEC-2019	RRL	X.00-13 : Improved checking of condition to start.
**
**	 2-FEB-2020	RRL	X.00-14 : Resolved problem with consuming VM if calloc() is called from thread,
**				moved ns_init() from thread to thread's creation routine.
**
**	21-FEB-2020	RRL	X.00-14 : Fixed incorrect passing of optval in setsockopt() calls.
**
**	19-MAR-2020	RRL	X.00-16 : Refactoring to move report by HTTP's 200 on successfull HTTP CONNECT to BroAgent side.
**
**	29-MAR-2020	RRL	X.00-17 : Changed HTTP's statuses according to RFC
**
**	24-APR-2020	RRL	X.00-18 : Fixed SIGSEGV in the __parse_http_header()
**
**	21-JUL-2020	RRL	X.00-20 : Recoded rq_thread() to improve auto-adjustment.
**
**	 7-FEB-2021	RRL	X.00-21 : Added support of PRIVATE request is used  to communicate between master and agent .
**
**	13-FEB-2021	RRL	X.00-22 : Small fixes, improved process_private() routine by using embedded lambda-routine.
**
**	 2-APR-2021	RRL	X.00-23 : Small fixes related data traffic obfuscation.
**
**	 9-APR-2021	RRL	X.00-24 : Small fixes related data traffic obfuscation;
**				replace XOR-ing with the nibble swaping.
**
**	16-MAY-2021	RRL	X.00-25 : Replace with parameter the <backlog> argument of the listen()
**
**	26-OCT-2021	RRL	X.00-27 : Improve error diagnostic in the recv_n()
**
**	19-NOV-2021	RRL	X.00-28 : Fixed a bug of premature cancelation of the req_thread().
**
**	20-DEC-2021	RRL	X.00-29 : Patching URI for HTTP ( !CONNECT) proxying;
**				some other code reorganazing.
**
**	21-DEC-2021	RRL	X.00-30 : SOCKS5 proxying in the master-agent mode.
**--
*/

#include	<stdlib.h>
#include	<stdio.h>
#include	<pthread.h>
#include	<errno.h>
#include	<poll.h>
#include	<fcntl.h>
#include	<memory.h>
#include	<arpa/inet.h>
#include	<strings.h>
#include	<unistd.h>
#include	<time.h>
#include	<sys/types.h>
#include	<sys/socket.h>
#include	<netdb.h>
#include	<sys/ioctl.h>
#include	<net/if.h>
#include	<resolv.h>
#include	<netinet/tcp.h>
#include	<stdatomic.h>

#define		__FAC__	"BPROXY"
#define		__TFAC__ __FAC__ ": "		/* Special prefix for $TRACE			*/
#include	"utility_routines.h"

#include	"bproxy.h"
#include	"base64.h"

extern int	g_exit_flag,
		g_trace, g_authmode, g_connback, g_thstack, g_backlog;
static int	g_agent_ver;
extern	ASC	q_extbind, q_peers;

extern struct sockaddr_in g_extbind_sock;

extern	BP_QUOTAS	g_quota;		/* Quotas: BW, Traffic Volume, Connections	*/
extern	BP_TIMERS	g_timers_set;

extern int	io_enq	(int, struct sockaddr_in *, int , struct sockaddr_in *, ASC *, ASC *, int proto);

static const	int one = 1, off = 0;
static const	socklen_t slen = sizeof(struct sockaddr);

typedef struct __rq_ctx__ {
	int	sd;

	unsigned long long	inreqs;		/* ??? For what this field ???			*/
	void	*	nsctx;			/* Area to keep NS-servers information		*/

} RQ_CTX;

static	BP_DPOOL rq_params;
static struct timespec	ts_last_zero,		/* A time stamp of last I/O threads crew adjustment	*/
		ts_delta = {0};			/* Delta time in the timespec format		*/
static atomic_int	cnt_th_current = 0,	/* Current number of the I/O threads in crew	*/
		cnt_th_target = 0,		/* Target number of I/O threads in crew		*/
		cnt_rq = 0;			/* A number of I/O since last zeroing		*/


static	pthread_attr_t th_tattr;

						/* Mutex is supposed to be used when thread want
						 * to performs cancelation check as a part of
						 * dynamic adjustment logic
						 */
static	pthread_mutex_t	th_exit_lock = PTHREAD_MUTEX_INITIALIZER;


/* HTTP's methods */
struct	proto_tbl	{
	ASC	req;
	int	code;
};

const struct proto_tbl reqs_sig []  = {
		{{ $ASCINI("get")},	BP$K_PROTO_HTTP},
		{{ $ASCINI("post") },	BP$K_PROTO_HTTP},
		{{ $ASCINI("head") },	BP$K_PROTO_HTTP},
		{{ $ASCINI("put") },	BP$K_PROTO_HTTP},
		{{ $ASCINI("delete") }, BP$K_PROTO_HTTP},
		{{ $ASCINI("trace") },	BP$K_PROTO_HTTP},

		{{ $ASCINI("connect") },BP$K_PROTO_HTTPS},

		{0}
};

#define	HTTP_AGENT	"StarLet/" __IDENT__ "/" __ARCH__NAME__  " (built  at "__DATE__ " " __TIME__ ")"

const char g_private_key[] = {						/* Shared key */
"Ezekiel 25:17. The path of the righteous man is beset on all sides by the" \
"inequities of the selfish and the tyranny of evil men. Blessed is he who, in" \
"the name of charity and good will, shepherds the weak through the valley of " \
"darkness, for he is truly his brother’s keeper and the finder of lost" \
"children. And I will strike down upon thee with great vengeance and furious" \
"anger those who attempt to poison and destroy my brothers. And you will know" \
"my name is the Lord when I lay my vengeance upon thee." };
const int g_private_key_sz = sizeof(g_private_key) - 1;

static const char http_host []={ "Host:"},
		http_auth [] = { "Proxy-Authorization:"},
		http_pa []  =  { "Proxy-Agent: "},
		http_rqid[]  = { "StarLet-Context: "},
		http_407 []  = { "HTTP/1.0 407 Proxy Authentication Required" CRLF "Proxy-Authenticate: Basic" CRLFCRLF},
		http_403 []  = { "HTTP/1.0 403 Forbidden" CRLF "Proxy-Agent: " HTTP_AGENT  CRLFCRLF},
		http_404 []  = { "HTTP/1.0 404 Not Found" CRLF "Proxy-Agent: " HTTP_AGENT  CRLFCRLF},
		http_200 []  = { "HTTP/1.0 200 OK" CRLF "Proxy-Agent: " HTTP_AGENT CRLFCRLF},
		http_504 []  = { "HTTP/1.0 504 Gateway Timeout" CRLF "Proxy-Agent: " HTTP_AGENT CRLFCRLF},
		http_connect []  = { "CONNECT %.*s:%d HTTP/1.1" CRLF "Host: %.*s:%d" CRLF "Proxy-Agent: " HTTP_AGENT CRLFCRLF};

static inline int	__set_nonbio_flag	(
		int	sd,
		int	flag
			)
{
int	status;

	if ( 0 > (status = fcntl(sd, F_GETFL, 0)) )
		return	$LOG(STS$K_ERROR, "fcntl(%d, F_GETFL)->%d, errno=%d", sd, status, errno);

	if ( flag  && !(status & O_NONBLOCK) )
		{
		if ( 0 > (status = fcntl(sd, F_SETFL, status | O_NONBLOCK)) )
			return	$LOG(STS$K_ERROR, "fcntl(%d, F_SETFL, 0x%08x)->%d, errno=%d", sd, errno, status | flag, errno);

		}
	else if ( (!flag) && (status & O_NONBLOCK) )
		{
		if ( 0 > (status = fcntl(sd, F_SETFL, status & (~O_NONBLOCK))) )
			return	$LOG(STS$K_ERROR, "fcntl(%d, F_SETFL, 0x%08x)->%d, errno=%d", sd, errno, status & (~O_NONBLOCK), errno );

		}

	return	STS$K_SUCCESS;
}

/*
 *   DESCRIPTION: Remove Proxy-Authorization field from the HTTP's header
 *
 *   INPUTS:
 *	bufp:	a buffer with the HTTP header
 *	buflen:	a lengrth of the data in the buffer
 *
 *   OUTPUTS:
 *	retlen:	a length of the modified data in the buffer
 *
 *   RETURNS:
 *	Condition code, see STS$K_* constants.
 */
static inline int	__remove_proxy_auth	(
			char	*bufp,
			int	 buflen,
			int	*retlen
				)
{
char	*cp1, *cp2;

	if ( !(cp1 = cp2 = strstr(bufp, http_auth)) )
		return	STS$K_SUCCESS;

	/* Proxy-Authorization: basic aGVsbG86d29ybGQ=<CR><LF>
	** ^
	** |
	** cp1
	*/
	cp2  += (sizeof(http_auth) - 1);

	if ( !(cp2 = strstr(cp2, "\r\n")) )
		return	STS$K_ERROR;

	cp2 += 2;


	/* Proxy-Authorization: basic aGVsbG86d29ybGQ=<CR><LF>
	** ^                                                  ^
	** |                                                  |
	** cp1                                               cp2
	*/
	memmove (cp1, cp2, buflen - (cp2 - bufp));

	*retlen = buflen - (cp2 - cp1);
	*(bufp + *retlen) = '\0';

	return	STS$K_SUCCESS;

}

/*
 *  DESCRIPTION: Create socket and try to establishing TCP-connection with specified by socket remote end-point.
 *
 *  INPUT:
 *	sk:	a socket of remote end-point (ip:port)
 *
 *  OUTPUT:
 *	sd:	network socket descriptor
 *
 *  RETURN:
 *	Condition code, see STS$K_* constants.
 */
int	__connect_by_sock	(
	struct	sockaddr_in	*sk,
			int	*sd
				)
{
int	status;
struct pollfd pfd = {0, POLLOUT, 0};
char	buf[512], tmp[128];
struct	sockaddr_in	lsock = {0};

	/* Create handle of socket */
	if ( 0 > (pfd.fd = socket(AF_INET, SOCK_STREAM, 0)) )
		return	$LOG(STS$K_ERROR, "socket(AF_INET, SOCK_STREAM)->%d, errno=%d", pfd.fd, errno);


	/* Do we need to bind outgoing socket to a specific IP address  ?*/
	if ( $ASCLEN(&q_extbind) && g_extbind_sock.sin_addr.s_addr )
		{
		$IFTRACE(g_trace, "Do EXTIBIND to %.*s ...", $ASC(&q_extbind));

		if ( !inet_pton(lsock.sin_family = AF_INET, $ASCPTR(&q_extbind), &lsock.sin_addr) )
			{
			close(pfd.fd);
			return	$LOG(STS$K_ERROR, "inet_pton(%.*s), errno=%d", $ASC(&q_extbind), errno);
			}

		if ( 0 > bind(pfd.fd, (struct sockaddr*) &lsock, slen) )
			{
			close(pfd.fd);
			return	$LOG(STS$K_ERROR, "bind(%d, %.*s:%d), errno=%d", pfd.fd, $ASC(&q_extbind), ntohs(lsock.sin_port), errno);
			}
		}

	/* Do we need to bind outgoing socket to a specific network device ?*/
	else if ( $ASCLEN(&q_extbind) )
		{
		struct ifreq if_bind = {0};
		memcpy(if_bind.ifr_name, $ASCPTR(&q_extbind), $ASCLEN(&q_extbind));

		if( 0 > setsockopt(pfd.fd, SOL_SOCKET, SO_BINDTODEVICE, &if_bind,  sizeof(if_bind)) )
			{
			close(pfd.fd);
			return	$LOG(STS$K_ERROR, "setsockopt(#%d, SO_BINDTODEVICE->%.*s), errno=%d", pfd.fd, $ASC(&q_extbind), errno);
			}
		}





	/* Set NONBIO flag - we suppose performs timeout processing */
	if ( !( 1 & __set_nonbio_flag (pfd.fd, one)) )
		{
		close (pfd.fd);
		return	$LOG(STS$K_ERROR, "fcntl(%d)", pfd.fd);
		}

	/* Try to connect to remote host */
	inet_ntop(sk->sin_family, &sk->sin_addr, buf, sizeof (buf));
	$IFTRACE (g_trace, "[#%d] Connecting to %s:%d (timeout is %d msec) ...", pfd.fd, buf, ntohs(sk->sin_port), timespec2msec (&g_timers_set.t_conn));

	if ( 0 > (status = connect(pfd.fd, (struct sockaddr *) sk, slen))
		&& (errno != EINPROGRESS) && (errno != EALREADY) )
		$LOG(STS$K_ERROR, "connect(%s:%d)->%d, errno=%d", buf, ntohs(sk->sin_port), status, errno);
	else	{
		/* Start waiting for establishing TCP-connection ... */
		if( 0 >  (status = poll(&pfd, 1, timespec2msec (&g_timers_set.t_conn))) )
			$LOG(STS$K_ERROR, "poll(%d, POLLOUT)->%d, errno=%d", pfd.fd, status, errno);
		else if ( (status == 1) && (pfd.revents & POLLOUT) )
			status = 0;	/* Connected !!! */
		else	{
			$IFTRACE(g_trace, "sd=%d, status=%d, errno=%d, pfd.revents=%08x",
				pfd.fd, status, errno, pfd.revents);

			status = $LOG(STS$K_ERROR, "[#%d] Timeout in connection request to %s:%d.", pfd.fd, buf, ntohs(sk->sin_port));
			}
		}

	/*
	 * Check result of establishing of TCP-connection
	 */
	if ( status )
		{
		close(pfd.fd);
		return STS$K_ERROR;
		}

	if ( !( 1 & __set_nonbio_flag (pfd.fd, off)) )
		{
		close (pfd.fd);
		return	$LOG(STS$K_ERROR, "fcntl(%u)", pfd.fd);
		}

	status = sizeof(struct sockaddr);
	getsockname (pfd.fd, (struct sockaddr *) &lsock,  (socklen_t *) &status);

	if ( status = setsockopt(pfd.fd, IPPROTO_TCP, TCP_NODELAY, &one, sizeof(one)) )
		$LOG(STS$K_WARN, "setsockopt(#%d, IPPROTO_TCP, TCP_NODELAY)->%d, errno=%d", pfd.fd, errno);


	$IFTRACE(g_trace, "[#%d] Connection %s:%d->%s:%d has been established", pfd.fd,
		inet_ntop(AF_INET, &lsock.sin_addr, tmp, sizeof (tmp)), ntohs(lsock.sin_port), buf, ntohs(sk->sin_port));

	*sd = pfd.fd;

	return	STS$K_SUCCESS;
}




/*
 *  DESCRIPTION: Initialize a context for network service, establish a connection
 *	to specified remote name or IP address and port.
 *
 *  INPUT:
 *	host:	A remote host IP address or name
 *	port:	A TCP port number
 *
 * OUTPUT:
 *	sd:	a socket descriptor, by address
 *	sk:	filled socket structure, by address
 *
 * RETURN:
 *	Condition code, see STS$K_* constants.
 */

int	__connect	(
		void	*	nsctx,
		ASC	*	host,
		unsigned short	port,
			int *	sd,
	struct sockaddr_in	*sk
			)
{
int	status;
struct addrinfo hints ={0}, *adilist = NULL, *adip = NULL;
char	buf[512], service[32] = {0}, *phost, host2[512];
struct	sockaddr_in	rsock = {0};

	$IFTRACE(g_trace, "Host's URL %.*s (default port=%d)", $ASC(host), port);

	host->sts[host->len] = '\0';
	memset(sk, 0, sizeof(struct sockaddr_in));

	if ( sscanf($ASCPTR(host), "%256[^:]:%6[0-9]", host2, service) )
		phost = host2;
	else	phost = $ASCPTR(host);

	if ( status = atoi(service) )
		rsock.sin_port = htons(status);
	else	rsock.sin_port = htons(port);

	rsock.sin_family = AF_INET;

	/* Is we got the IP address ? */
	if ( inet_pton(AF_INET, phost, &rsock.sin_addr) )
		{
		if ( 1 & (status = __connect_by_sock ( &rsock, sd)) )
			*sk = rsock;

		return	status;
		}

	sprintf(service, "%u", port);

	if ( !(1 & (status = ns_name2ip(nsctx, phost, service, &rsock.sin_addr))) )
		return	status;

	if ( 1 & (status = __connect_by_sock ( &rsock, sd)) )
			*sk = rsock;

	return	status;
}




int	__connect_peer	(
		BP_PEER		*peer,
		char		*bufp,
		int		 buflen,
		int		*sd,
	struct	sockaddr_in	*sk
			)
{
	__remove_proxy_auth (bufp, buflen, &buflen);

	if ( !(1 & __connect_by_sock(&peer->sk, sd)) )
		return	$LOG(STS$K_ERROR, "Cannot connect to peer proxy");

	memcpy(sk, &peer->sk, sizeof(peer->sk));

	xmit_n (*sd, bufp, buflen);

	return	STS$K_SUCCESS;
}



/*
 *   DESCRIPTION: Read NUL-terminated octets string from the network socket, wait if not all data has been got
 *		but no more then 13 seconds.
 *
 *   INPUT:
 *	sd:	Network socket descriptor
 *	buf:	A buffer to accept data
 *	bufsz:	A size of the buffer
 *
 *  OUTPUT:
 *	buf:	Received data
 *	buflen:	An actual data has been read
 *
 *  RETURN:
 *	condition code, see STS$K_* constant
 */
inline	static int __recv_z
		(
		int	sd,
		char	*buf,
		int	bufsz,
		int	*buflen,
	struct timespec	*delta
		)
{
int	status;
struct pollfd pfd = {sd, POLLIN, 0};
struct timespec	now, etime;

	/* Compute an end of I/O operation time	*/
	if ( status = clock_gettime(CLOCK_REALTIME, &now) )
		return	$LOG(STS$K_ERROR, "clock_gettime()->%d, errno=%d", status, errno);

	__util$add_time (&now, delta, &etime);

	for (*buflen = 0; bufsz; )
		{
		/* Do we reach the end of I/O time ?*/
		clock_gettime(CLOCK_REALTIME, &now);
		if ( (now.tv_sec >= etime.tv_sec) && (now.tv_nsec >= etime.tv_nsec) )
			break;

		if( 0 >  (status = poll(&pfd, 1, timespec2msec (delta))) && (errno != EINTR) )
			return	$LOG(STS$K_ERROR, "[#%d] poll/select()->%d, errno=%d", sd, status, errno);
		else if ( (status < 0) && (errno == EINTR) )
			{
			$LOG(STS$K_WARN, "[#%d] poll/select()->%d, errno=%d", sd, status, errno);
			continue;
			}

		if ( pfd.revents & (~POLLIN) )	/* Unexpected events ?!			*/
			return	$LOG(STS$K_ERROR, "[#%d] poll()->%d, .revents=%08x(%08x), errno=%d",
					sd, status, pfd.revents, pfd.events, errno);

		if ( !(pfd.revents & POLLIN) )	/* Non-interesting event ?		*/
			continue;

		/* Retrieve data from socket buffer	*/
		status = recv(sd, buf, 1, 0);

		if ( (0 >= status) && (errno != EINPROGRESS) )
			{
			$LOG(STS$K_ERROR, "[#%d] recv(1 octet)->%d, .revents=%08x(%08x), errno=%d",
					sd, status, pfd.revents, pfd.events, errno);
			break;
			}

		(*buflen)++;

		if ( ! (*buf) )
			return	STS$K_SUCCESS;

		bufsz--;
		buf++;
		}

	return	$LOG(STS$K_ERROR, "[#%d] Did not get NUL-terminated octet string", sd, bufsz);
}




/*
 *   DESCRIPTION: Read n bytes from the network socket, wait if not all data has been get
 *		but no more then 13 seconds.
 *
 *   INPUT:
 *	sd:	Network socket descriptor
 *	buf:	A buffer to accept data
 *	bufsz:	A number of bytes to be read
 *
 *  OUTPUT:
 *	buf:	Received data
 *
 *  RETURN:
 *	condition code, see STS$K_* constant
 */
inline	int recv_n
		(
		int	sd,
		void	*buf,
		int	bufsz,
	struct timespec	*delta
		)
{
int	status, restbytes = bufsz;
struct pollfd pfd = {sd, POLLIN, 0};
struct timespec	now, etime;
char	*bufp = (char *) buf;


	/* Compute an end of I/O operation time	*/
	if ( status = clock_gettime(CLOCK_REALTIME, &now) )
		return	$LOG(STS$K_ERROR, "clock_gettime()->%d, errno=%d", status, errno);

	__util$add_time (&now, delta, &etime);

	for ( restbytes = bufsz; restbytes; )
		{
		/* Do we reach the end of I/O time ?*/
		clock_gettime(CLOCK_REALTIME, &now);
		if ( (now.tv_sec >= etime.tv_sec) && (now.tv_nsec >= etime.tv_nsec) )
			break;

		if( 0 >  (status = poll(&pfd, 1, timespec2msec (delta))) && (errno != EINTR) )
			return	$LOG(STS$K_ERROR, "[#%d] poll/select()->%d, errno=%d, requested %d octets, rest %d octets", sd, status, errno, bufsz, restbytes);
		else if ( (status < 0) && (errno == EINTR) )
			{
			$LOG(STS$K_WARN, "[#%d] poll/select()->%d, errno=%d, requested %d octets, rest %d octets", sd, status, errno, bufsz, restbytes);
			continue;
			}

		if ( pfd.revents & (~POLLIN) )		/* Unexpected events ?!			*/
			return	$LOG(STS$K_ERROR, "[#%d] poll()->%d, .revents=%08x(%08x), errno=%d",
					sd, status, pfd.revents, pfd.events, errno);

		if ( !(pfd.revents & POLLIN) )		/* Non-interesting event ?		*/
			continue;

		/* Retrieve data from socket buffer	*/
		if ( restbytes == (status = recv(sd, bufp, restbytes, 0)) )
			return	STS$K_SUCCESS;		/* Bingo! We has been received a requested amount of data */
		else if ( (!status) && (!errno) )
			return	$LOG(STS$K_ERROR, "[#%d] Peer unexpectedly closed connection", sd);


		if ( (0 > status) && (errno != EINPROGRESS) )
			{
			$LOG(STS$K_ERROR, "[#%d] recv(%d octets)->%d, .revents=%08x(%08x), errno=%d",
					sd, restbytes, status, pfd.revents, pfd.events, errno);
			break;
			}

		/* Advance buffer pointer and decrease expected byte counter */
		restbytes -= status;
		bufp	+= status;
		}

	return	$LOG(STS$K_ERROR, "[#%d] Did not get requested %d octets in %d msecs, rest %d octets", sd, bufsz, timespec2msec (delta), restbytes);
}


/*
 *   DESCRIPTION: Write n bytes to the network socket, wait if not all data has been get
 *		but no more then 13 seconds;
 *
 *   INPUT:
 *	sd:	Network socket descriptor
 *	buf:	A buffer with data to be sent
 *	bufsz:	A number of bytes to be read
 *
 *  OUTPUT:
 *	NONE
 *
 *  RETURN:
 *	condition code, see STS$K_* constant
 */
inline	int xmit_n
		(
		int	sd,
		void	*buf,
		int	bufsz
		)
{
int	status, restbytes = bufsz;
struct pollfd pfd = {sd, POLLOUT, 0};
struct timespec	now, etime;
char	*bufp = (char *) buf;

	/* Compute an end of I/O operation time	*/
	if ( status = clock_gettime(CLOCK_REALTIME, &now) )
		return	$LOG(STS$K_ERROR, "clock_gettime()->%d, errno=%d", status, errno);

	__util$add_time (&now, &g_timers_set.t_xmit, &etime);

	for ( restbytes = bufsz; restbytes; )
		{
		/* Do we reach the end of I/O time ? */
		clock_gettime(CLOCK_REALTIME, &now);
		if ( (now.tv_sec >= etime.tv_sec) && (now.tv_nsec >= etime.tv_nsec) )
			break;

		if( 0 >  (status = poll(&pfd, 1, 1000)) && (errno != EINTR) )
			return	$LOG(STS$K_ERROR, "[#%d] poll/select()->%d, errno=%d, requested %d octets, rest %d octets", sd, status, errno, bufsz, restbytes);
		else if ( (status < 0) && (errno == EINTR) )
			{
			$LOG(STS$K_WARN, "[#%d] poll/select()->%d, errno=%d, requested %d octets, rest %d octets", sd, status, errno, bufsz, restbytes);
			continue;
			}

		if ( pfd.revents & (~POLLOUT) && (errno != EAGAIN) )	/* Unexpected events ?!			*/
			return	$LOG(STS$K_ERROR, "[#%d] poll()->%d, .revents=%08x(%08x), errno=%d",
					sd, status, pfd.revents, pfd.events, errno);

		if ( !(pfd.revents & POLLOUT) )	/* No interesting event			*/
			continue;

		/* Send data to socket buffer	*/
		if ( restbytes == (status = send(sd, bufp, restbytes, MSG_NOSIGNAL)) )
			return	STS$K_SUCCESS; /* Bingo! We has been sent a requested amount of data */

		if ( 0 >= status )
			{
			$LOG(STS$K_ERROR, "[#%d] send(%d octets)->%d, .revents=%08x(%08x), errno=%d",
					sd, restbytes, status, pfd.revents, pfd.events, errno);
			break;
			}

		/* Advance buffer pointer and decrease to be sent byte counter */
		restbytes -= status;
		bufp	+= status;
		}

	return	$LOG(STS$K_ERROR, "[#%d] Did not put requested %d octets, rest %d octets", sd, bufsz, restbytes);
}


/*
 *   DESCRIPTION: Parse HTTP header to extract target Host & Port , authorization vector
 *
 *   INPUT:
 *	bufp:	A buffer with the HTTP header to process
 *	bufsz:	A length of the HTTP header
 *	rqcode:	A has been recognized request code, see BP$K_PROTO_*
 *
 *   OUTPUT:
 *	host:	Target host name or IP address
 *	port	Target TCP port number
 *	auth:	Decoded from base64  authorization vector
 *
 *   RETURN:
 *	condition code
 */
inline static int	__parse_http_header	(
		char	*bufp,
		int	bufsz,
		int	rqcode,
		ASC	*host,
		int	*port,
		ASC	*auth
			)
{
int	buflen;
char	buf[512], *cp;

	$ASCLEN(host) = 0;
	$ASCLEN(auth) = 0;

	if ( rqcode == BP$K_PROTO_HTTPS )
		{
		/* CONNECT www.example.com:443 HTTP/1.1 */
		sscanf(bufp, "%*s %128s", buf );

		__util$str2asc (buf, host);

		*port = BP$K_HTTPS_PORT;
		}
	else	{/* rqcode == BP$K_PROTO_HTTP */
		/* Host: server.example.com:80 */
		if ( !(cp = strstr(bufp, http_host)) )
			return	$LOG(STS$K_ERROR, "No '%s' field in the '%.*s", http_host, bufsz, bufp);

		sscanf(cp, "%*s %128s", buf );

		__util$str2asc (buf, host);
		*port = BP$K_HTTP_PORT;
		}

	/* Try to extract a port number from the target host specs */
	sscanf(buf, "%*s:%d", port);

	if ( g_authmode != BP$K_AUTH_LOGIN )
		return	STS$K_SUCCESS;

	/* Proxy-Authorization: basic aGVsbG86d29ybGQ= */
	if ( !(cp = strstr(bufp, http_auth)) )
		return	$LOG(STS$K_WARN, "No '%s' field in the '%.*s", http_auth, bufsz, bufp);

	sscanf(cp, "%*s %*s %128[^\n ]", buf );

	__util$unbase64(buf, strlen(buf), $ASCPTR(auth), ASC$K_SZ, &buflen);

	$ASCLEN(auth) = (unsigned char) buflen;

	return	STS$K_SUCCESS;

}

/*
 *   DESCRIPTION: Read HTTP Header from the network socket.
 *
 *   INPUT:
 *	sd:	Network socket descriptor
 *	buf:	A buffer to accept data
 *	bufsz:	A number of bytes to be read
 *
 *  OUTPUT:
 *	buf:	Received data
 *
 *  RETURN:
 *	condition code, see STS$K_* constant
 */
inline	static int __recv_header
		(
		int	sd,
		char	*bufp,
		int	bufsz,
		int	*buflen
		)
{
int	status;
struct pollfd pfd = {sd, POLLIN, 0};
char	*cp;
struct timespec	now, etime;

	/* Compute an end of I/O operation time	*/
	if ( status = clock_gettime(CLOCK_REALTIME, &now) )
		return	$LOG(STS$K_ERROR, "clock_gettime(CLOCK_REALTIME_RAW)->%d, errno=%d", status, errno);

	__util$add_time (&now, &g_timers_set.t_req, &etime);

	for ( cp = bufp; bufsz; )
		{
		/* Do we reach the end of I/O time ?*/
		clock_gettime(CLOCK_REALTIME, &now);

		if ( 0 < __util$cmp_time(&now, &etime) )
			break;

		if( 0 >  (status = poll(&pfd, 1, timespec2msec (&g_timers_set.t_req))) && (errno != EINTR) )
			return	$LOG(STS$K_ERROR, "[#%d] poll/select()->%d, errno=%d", sd, status, errno);
		else if ( (status < 0) && (errno == EINTR) )
			{
			$LOG(STS$K_WARN, "[#%d] poll/select()->%d, errno=%d", sd, status, errno);
			continue;
			}

		if ( pfd.revents & (~POLLIN) )	/* Unexpected events ?!			*/
			return	$LOG(STS$K_ERROR, "[#%d] poll()->%d, .revents=%08x(%08x), errno=%d",
					sd, status, pfd.revents, pfd.events, errno);

		if ( !(pfd.revents & POLLIN) )	/* Non-interesting event ?		*/
			continue;

		/* Retrieve data from socket buffer	*/
		if ( 1 != (status = recv(sd, cp, 1, 0)) )
			{
			$LOG(STS$K_ERROR, "[#%d] recv(1 octet)->%d, .revents=%08x(%08x), errno=%d",
					sd, status, pfd.revents, pfd.events, errno);
			break;
			}

		cp++;
		bufsz--;

		if ( ((cp - bufp) > 4)
			&& (status = ((*((int *) (cp - sizeof(CRLFCRLF_LW)))) == CRLFCRLF_LW)) )
				{
				*buflen = cp - bufp;
				return	STS$K_SUCCESS;
				}
		}

	return	$LOG(STS$K_ERROR, "[#%d] Did not get whole HTTP request", sd);
}


/*
 *   DESCRIPTION: Matching given buffer against protocol's signatures.
 *
 *   INPUT:
 *	bufp:	A buffer with the data to matching
 *	bufsz:	A length of data in the buffer
 *
 *   OUTPUT:
 *	rqcode:	A recognized protocol code, see BP$K_PROTO_* constants
 *
 *   RETURN:
 *	STS$K_NORMAL - buffer data has been matched, write protocol code in the 'rqcode'
 *	condition code
 *
 */
inline static int proto_match	(
		char	*bufp,
		int	bufsz,
		int	*rqcode
			)
{
int	len;
struct proto_tbl *pt;
char	hexbuf[128];

	if ( !bufsz )
		return	STS$K_ERROR;

	/* Private protocol markers is octets' sequences: [1,3,5] or [3,5,1] or [5,1,3] */
	if ( (bufp[0] == 1) && (bufp[1] == 3) && (bufp[2] == 5) )
		return	*rqcode = BP$K_PROTO_PRIVATE, STS$K_SUCCESS;
	if ( (bufp[0] == 3) && (bufp[1] == 5) && (bufp[2] == 1) )
		return	*rqcode = BP$K_PROTO_PRIVATE, STS$K_SUCCESS;
	if ( (bufp[0] == 5) && (bufp[1] == 1) && (bufp[2] == 3) )
		return	*rqcode = BP$K_PROTO_PRIVATE, STS$K_SUCCESS;




	/* Is it SOCKS request ? */
	if ( *bufp == (*rqcode = BP$K_PROTO_SOCKS4) )
		return	STS$K_SUCCESS;

	if ( *bufp == (*rqcode = BP$K_PROTO_SOCKS5) )
		return	STS$K_SUCCESS;

	/* HTTP ? */
	for ( pt = reqs_sig; pt->code; pt++)
		{
		len = $MIN(bufsz, $ASCLEN(&pt->req));

		if ( !strncasecmp(bufp, $ASCPTR(&pt->req), len) )
			{
			*rqcode = pt->code;
			return	STS$K_SUCCESS;
			}
		}

	/* Illegal or unrecognized request */
	__util$bin2hex (bufp, hexbuf, bufsz);
	$LOG(STS$K_ERROR, "Illegal or unrecognized request [0:%d]=0x%s", bufsz, hexbuf);
	return	STS$K_ERROR;

}

static int	listener_init	(
		ASC	*s_bind,
		int	*sd
			)
{
int	status;
char	ia [32] = {0}, pn [32]={0};
unsigned short npn = 0;
struct sockaddr_in sk = {0};
socklen_t slen = sizeof(struct sockaddr);

	if ( sscanf($ASCPTR(s_bind), "%32[^:\n]:%8[0-9]", ia, pn) )
		{
		if (  (npn = atoi(pn)) )
			sk.sin_port = htons(npn);

		if ( 0 > (status = inet_pton(AF_INET, ia, &sk.sin_addr)) )
				return	$LOG(STS$K_ERROR, "inet_pton(%s)->%d, errno=%d", ia, status, errno);
		}
	else	return	$LOG(STS$K_ERROR, "Illegal or illformed IP:Port (%.*s)", $ASC(s_bind));

	inet_ntop(AF_INET, &sk.sin_addr, ia, slen);

	$LOG(STS$K_INFO, "Initialize listener on : %s:%d", ia, ntohs(sk.sin_port));

	sk.sin_family = AF_INET;

	if ( 0 > (*sd = socket(AF_INET, SOCK_STREAM, 0)) )
		return	$LOG(STS$K_ERROR, "socket(), errno=%d", errno);

	/* avoid EADDRINUSE error on bind() */
#ifdef	SO_REUSEADDR
	if( 0 > setsockopt(*sd, SOL_SOCKET, SO_REUSEADDR, (char *)&one, sizeof(one))  )
		$LOG(STS$K_WARN, "setsockopt(%d, SO_REUSEADDR), errno=%d", *sd, errno);
#endif	/* SO_REUSEADDR */


#ifdef	SO_REUSEPORT
	if( 0 > setsockopt(*sd, SOL_SOCKET, SO_REUSEPORT, (char *)&one, sizeof(one))  )
		$LOG(STS$K_WARN, "setsockopt(%d, SO_REUSEPORT), errno=%d", *sd, errno);
#endif	/* SO_REUSEADDR */


	if ( 0 > bind(*sd, (struct sockaddr*) &sk, slen) )
		{
		close(*sd);
		return	$LOG(STS$K_ERROR, "bind(%d, %s:%d), errno=%d", *sd, ia, ntohs(sk.sin_port), errno);
		}


	return	STS$K_SUCCESS;
}




int	req_process_socks4(
			RQ_CTX	*ctx,
			int	insd,
	struct sockaddr_in	*insk,
			char	*bufp,
			int	bufsz
			)
{
int	status, buflen = 0, outsd = -1, flags;
BP_SOCKS4 * so4 = (BP_SOCKS4 *) bufp, so4_ans = {0, BP$K_SOCKS4_NOAUTH, 0x0DEC, 0xDEADBEEF};
char	sia[32] = {0}, dia[32] = {0};
ASC	auth = {0};
struct sockaddr_in outsk = {0};

	if ( !(1 & recv_n (insd, bufp + BP$SZ_MINLEN, status = offsetof(BP_SOCKS4, user) - BP$SZ_MINLEN, &g_timers_set.t_seq)) )
		{
		close(insd);
		return	$LOG(STS$K_ERROR, "Error reading initial SOCKS4 sequence");
		}

	inet_ntop(AF_INET, &so4->dia, dia, sizeof (dia));
	$IFTRACE(g_trace, "%Request=%d, %s to %s:%d", so4->cmd,
		(so4->cmd == BP$K_SOCKS_CONN) ? "Connect" : "Bind", dia, ntohs(so4->dpn) );

	if ( so4->cmd != BP$K_SOCKS_CONN )
		{
		xmit_n (insd, &so4_ans, sizeof(BP_SOCKS4));
		close(insd);

		return	$LOG(STS$K_ERROR, "Unhandled SOKS4 request code=%d", so4->cmd);
		}

	/* We need to read username/password pair */
	if ( !(1 & (status = __recv_z (insd, $ASCPTR(&auth), ASC$K_SZ, &buflen, &g_timers_set.t_seq))) )
		{
		close(insd);
		return	$LOG(STS$K_ERROR, "Error reading username");
		}

	$ASCLEN(&auth) = strnlen($ASCPTR(&auth), buflen);

	/* Performs authorization by username or IP	*/
	if ( g_authmode == BP$K_AUTH_LOGIN )
		status = auth_uname_check(&auth);
	else	status = auth_check (&auth, &insk->sin_addr, &flags);

	if ( !(status & 1))
		{
		xmit_n (insd, &so4_ans, sizeof(BP_SOCKS4));
		close(insd);

		return	STS$K_ERROR;
		}


	/* Check against BAN  List */
	if ( !(1 & ban_check (&auth, &insk->sin_addr)) )
		{
		xmit_n (insd, &so4_ans, sizeof(BP_SOCKS4));
		close(insd);

		return	STS$K_ERROR;
		}


	if ( !$ASCLEN(&q_peers) )
		{
		/* Connect to target host */
		outsk.sin_family = AF_INET;
		outsk.sin_addr.s_addr = (in_addr_t) so4->dia;
		outsk.sin_port = so4->dpn;

		if ( !(status = __connect_by_sock (&outsk, &outsd)) )
			{
			xmit_n (insd, &so4_ans, sizeof(BP_SOCKS4));
			close(insd);

			return	status;
			}
		}
	else	{ /* Make outgoing TCP-connection to chained proxy */
		if ( !(1 & (status =  __connect (ctx->nsctx, &q_peers, 3128, &outsd, &outsk))) )
			{
			xmit_n (insd, &so4_ans, sizeof(BP_SOCKS4));
			close(insd);

			return	status;
			}
		}


	/* Finally we can transfer main I/O processing to I/O Engine */
	if ( 1 & (status = io_enq(insd, insk, outsd, &outsk, &auth, NULL, BP$K_PROTO_SOCKS4)) )
		{
		so4_ans.cmd = BP$K_SOCKS4_OK;
		xmit_n (insd, &so4_ans, sizeof(BP_SOCKS4));
		}

	return	status;
}



int	req_process_socks5(
			RQ_CTX	*ctx,
			int	insd,
	struct sockaddr_in	*insk,
			char	*bufp,
			int	bufsz
			)
{
int	status, buflen = 0, outsd = -1, i, flags;
BP_SOCKS5 * so5 = (BP_SOCKS5 *) bufp, so5_ans = {BP$K_PROTO_SOCKS5, BP$K_SOCKS5_ERR};
char	buf[512], oneoctet, *poctet, sia[32] = {0}, dia[32] = {0};
ASC	uname = {0}, pass = {0}, host = {0};
struct sockaddr_in outsk = {0};
unsigned short	port = 0;

	/*
	+----+----------+----------+
	|VER | NMETHODS | METHODS  |
	+----+----------+----------+
	| 1  |    1     | 1 to 255 |
	+----+----------+----------+
		+----+--------+
		|VER | METHOD |
		+----+--------+
		| 1  |   1    |
		+----+--------+
	*/

	/* Authorization methods: in case of using /AUTH=IP we sent - Authentication is no required */
	if ( g_authmode == BP$K_AUTH_IP)
		so5_ans.cmd = BP$K_SOCKS_NOAUTH;
	else	{
		/* We need check that plain-text authentication is supported by client */
		poctet = bufp + 2;
		for (status = i = 0; i < so5->cmd; i++ )
			{
			if ( status = (*poctet == BP$K_SOCKS_PLAIN) )
				break;
			}

		if ( !(1 & status) )
			{
			so5_ans.cmd = BP$K_SOCKS5_BADAUTH;
			xmit_n(insd, &so5_ans, BP$SZ_SOCKS5_ANS);
			close(insd);

			return $LOG(STS$K_ERROR, "Client does't support USERNAME/PASSWORD authentication");
			}


		so5_ans.cmd = BP$K_SOCKS_PLAIN;
		xmit_n(insd, &so5_ans, BP$SZ_SOCKS5_ANS);
		}


	if ( g_authmode == BP$K_AUTH_LOGIN )
		{
		/*
		+----+------+----------+------+----------+
		|VER | ULEN |  UNAME   | PLEN |  PASSWD  |
		+----+------+----------+------+----------+
		| 1  |  1   | 1 to 255 |  1   | 1 to 255 |
		+----+------+----------+------+----------+

			+----+--------+
			|VER | STATUS |
			+----+--------+
			| 1  |   1    |
			+----+--------+
		*/
		if ( !(1 & (status = recv_n (insd, &oneoctet, 1, &g_timers_set.t_seq))) )
			$LOG(STS$K_ERROR, "Error read auth.ver octet");
		else if ( oneoctet != BP$K_SOCKS5_VERAUTH )
			status = $LOG(STS$K_ERROR, "Invalid Ver octet %#x", oneoctet);
		else if ( !(1 & recv_n (insd, &oneoctet, 1, &g_timers_set.t_seq)) )
			status = $LOG(STS$K_ERROR, "Error read auth.ulen octet");
		else if ( !(1 & recv_n (insd, $ASCPTR(&uname), oneoctet, &g_timers_set.t_seq)) )
			status = $LOG(STS$K_ERROR, "Error read auth.uname  %d octets", oneoctet);

		if ( !(1 & status) )
			return	close(insd), status;

		$ASCLEN(&uname) = oneoctet;

		if ( !(1 & (status = recv_n (insd, &oneoctet, 1, &g_timers_set.t_seq))) )
			status = $LOG(STS$K_ERROR, "Error read auth.plen octet");
		else if ( !(1 & recv_n (insd, $ASCPTR(&pass), oneoctet, &g_timers_set.t_seq)) )
			status = $LOG(STS$K_ERROR, "Error read auth.passwd  %d octets", oneoctet);

		if ( !(1 & status) )
			return	close(insd), status;

		$ASCLEN(&pass) = oneoctet;

		so5_ans.cmd = BP$K_SOCKS5_NOAUTH;

		if ( !(1 & auth_check2 (&uname, &pass, &flags)) )
			{
			xmit_n (insd, &so5_ans, BP$SZ_SOCKS5_ANS);
			close(insd);

			return	STS$K_ERROR;
			}

		so5_ans.proto = BP$K_SOCKS5_VERAUTH;
		so5_ans.cmd = BP$K_SOCKS5_OK;
		status = xmit_n (insd, &so5_ans, BP$SZ_SOCKS5_ANS);
		}
	else	{
		if ( !(1 & auth_check (NULL, &insk->sin_addr, &flags)) )
			{
			so5_ans.cmd = BP$K_SOCKS5_NOAUTH;
			xmit_n (insd, &so5_ans, BP$SZ_SOCKS5_ANS);
			close(insd);

			return	STS$K_ERROR;
			}


		so5_ans.cmd = BP$K_SOCKS5_OK;
		xmit_n (insd, &so5_ans, BP$SZ_SOCKS5_ANS);
		}

	/* So we ready to accept CONNECT request */
	/*
	+----+-----+-------+------+----------+----------+
	|VER | CMD |  RSV  | ATYP | DST.ADDR | DST.PORT |
	+----+-----+-------+------+----------+----------+
	| 1  |  1  | X'00' |  1   | Variable |    2     |
	+----+-----+-------+------+----------+----------+
	*/
	if ( !(1 & recv_n (insd, so5, 5, &g_timers_set.t_seq)) )
		return	close(insd), $LOG(STS$K_ERROR, "Error read auth.ver octet");

	if ( (so5->proto != BP$K_PROTO_SOCKS5) && (so5->cmd != BP$K_SOCKS_CONN) || (so5->rsv) )
		return	close(insd), $LOG(STS$K_ERROR, "Illegal request");

	if ( so5->atyp == BP$K_SOCKS5_IP4 )
		buflen = 4 - 1;
	else if ( so5->atyp == BP$K_SOCKS5_IP6 )
		buflen = 16 - 1;
	else buflen = so5->sdia[0];	/* FQDN is a counted string (ASCIC) = octet length prefixed string */

	if ( !(1 & recv_n (insd, bufp + 5, buflen, &g_timers_set.t_seq)) )
		return	close(insd), $LOG(STS$K_ERROR, "Error read dst.addr field");

	if ( !(1 & recv_n (insd, &port, 2, &g_timers_set.t_seq)) )
		return	close(insd), $LOG(STS$K_ERROR, "Error read dst.port field");




	if ( g_connback )							/* If running mode is Master-Agent dispatch request processing to */
		{
		/* Construct an HTTP CONNECT request :
		 *	CONNECT server.example.com:80 HTTP/1.1
			Host: server.example.com:80
		*/
		if ( so5->atyp == BP$K_SOCKS5_IP4 )					/* Connect to destination host:port ... */
			{
			outsk.sin_family = AF_INET;
			outsk.sin_addr = so5->dia;
			outsk.sin_port = port;

			inet_ntop(AF_INET, &so5->dia, dia, sizeof (dia));

			buflen = snprintf(buf, sizeof(buf), http_connect, sizeof(dia), dia, ntohs(port), sizeof(dia), dia, ntohs(port));
			}
		else if ( so5->atyp == BP$K_SOCKS5_IP6 )
			{
			outsk.sin_family = AF_INET6;
			memcpy(&outsk.sin_addr, &so5->dia6,  sizeof(so5->dia6));
			outsk.sin_port = port;

			inet_ntop(AF_INET6, &so5->dia, dia, sizeof (dia));

			buflen = snprintf(buf, sizeof(buf), http_connect, sizeof(dia), dia, ntohs(port), sizeof(dia), dia, ntohs(port));
			}
		else	buflen = snprintf(buf, sizeof(buf), http_connect, $ASC(&so5->sdia), ntohs(port), $ASC(&so5->sdia), ntohs(port));


		$IFTRACE(g_trace, "Sending HTTP-like request to BroAgent (tm) instance :\n%.*s", buflen, buf);


		if ( !(1 & (status = req_to_agent(buf, buflen, insd, insk))) )
			{
			so5_ans.proto = BP$K_SOCKS5_ERR;
			so5_ans.cmd = BP$K_SOCKS5_REJ;
			status = xmit_n (insd, &so5_ans, BP$SZ_SOCKS5_ANS);

			return	close(insd), $LOG(STS$K_ERROR, "Error processing request:\n%.*s", buflen, buf);
			}
		}
	else if ( so5->atyp == BP$K_SOCKS5_IP4 )					/* Connect to destination host:port ... */
		{
		outsk.sin_family = AF_INET;
		outsk.sin_addr = so5->dia;
		outsk.sin_port = port;

		status = __connect_by_sock (&outsk, &outsd);
		}
	else if ( so5->atyp == BP$K_SOCKS5_IP6 )
		{
		outsk.sin_family = AF_INET6;
		memcpy(&outsk.sin_addr, &so5->dia6,  sizeof(so5->dia6));
		outsk.sin_port = port;

		status = __connect_by_sock (&outsk, &outsd);
		}
	else	{
		so5->sdia[1 + buflen] = '\0';

		__util$str2asc (so5->sdia, &host);
		status = __connect(ctx->nsctx, so5->sdia, ntohs(port), &outsd, &outsk);
		}

	if ( !(1 & status) )
		return	close(insd), status;

	/* Finally we can transfer main I/O processing to I/O Engine */
	if ( !g_connback && !(1 & io_enq(insd, insk, outsd, &outsk, &uname, &host, BP$K_PROTO_SOCKS5)) )
		return	STS$K_ERROR;

	/*
	+----+-----+-------+------+----------+----------+
	|VER | REP |  RSV  | ATYP | BND.ADDR | BND.PORT |
	+----+-----+-------+------+----------+----------+
	| 1  |  1  | X'00' |  1   | Variable |    2     |
	+----+-----+-------+------+----------+----------+
	*/

	so5_ans.proto = BP$K_PROTO_SOCKS5;
	so5_ans.cmd = BP$K_SOCKS5_OK;
	so5_ans.rsv = 0;
	so5_ans.atyp = outsk.sin_family == AF_INET ? BP$K_SOCKS5_IP4 : BP$K_SOCKS5_IP6;

	xmit_n (insd, &so5_ans, 4);

	xmit_n (insd, &outsk.sin_addr, 4); // so5_ans.atyp == BP$K_SOCKS5_IP4 ? sizeof(struct in_addr) : sizeof(struct in6_addr) );
	xmit_n (insd, &outsk.sin_port, 2);

	return	STS$K_SUCCESS;
}


/*
 *   DESCRIPTION: Process a back connection request:
 *	- initial Agent's connection request
 *	- a back connection as a part of normal procedure (it's an answer of has been processed HTTP request by Agent)
 *
 *   INPUTS:
 *
 *   OUTPUTS:
 *
 *   RETURNS:
 *	- STS$K_NORMAL
 *	- condition code
 *
 */
int	process_agent	(
			RQ_CTX	*ctx,
			char	*bufp,
			int	buflen,
			int	insd,
	struct sockaddr_in	*insk,
			int	proto
			)
{
ASC	pa = {0}, rqid = {0};

	__remove_proxy_auth	(bufp, buflen, &buflen);			/* Remove field Proxy-Authorization: basic aGVsbG86d29ybGQ=<CR><LF> */

	/* We expect to see a HTTP like request in form:
	 * CONNECT %s:%d HTTP/1.
	 *	Host: %s:%d
	 *	User Agent: StarLet ...
	 *	StarLet-Context: %#x:%#x
	 *
	 * Next hop of processing is depending by presence of 'StarLet-Context:' field,
	 * so we need to get this field in the request header.
	 *
	 */
	__http_get_field (bufp, buflen, http_rqid, sizeof(http_rqid) - 1, &rqid); /* Check StarLet-Context: StarLet ... */

	$IFTRACE(g_trace, "ReqId='%.*s'", $ASC(&rqid));

	/* So dispatch processing ...*/
	if ( $ASCLEN(&rqid) )
		ans_from_agent (bufp, buflen, insd, insk, &rqid, proto);		/* Process back-connection request */
	else	{
		char	vid[32] = {0}, vx[4] = {0}, vver[4] = {0}, vsub[4] = {0}, veco[4] = {0};

		/* Retreive BroAgent ident information from the 'Proxy-Agent' HTTP attribute */
		if ( 1 & __http_get_field (bufp, buflen, http_pa, sizeof(http_pa) - 1, &pa) )
			{
			/* Expect to see something like:
			 *  "StarLet/X.00-99/Win64 (built  at Mar 19 2020 11:59:55)"
			 */
			if ( sscanf($ASCPTR(&pa), "%*[^/]/%[^/]", vid) )
				if ( sscanf(vid, "%[^\.]\.%[0-9]\-%[0-9]ECO%[0-9]", vx, vver, vsub, veco) )
					{
					int	ivx = 0, ivver = 0, ivsub = 0, iveco = 0;

					ivx = toupper(vx[0]);
					ivver = atoi(vver);
					ivsub = atoi(vsub);
					iveco = atoi(veco);

					g_agent_ver = $VERSION(ivx, ivver, ivsub, iveco);
					}

			$LOG(STS$K_INFO, "Remote BroAgent: '%.*s', Ident=%08X ", $ASC(&pa), g_agent_ver);
			}
		else	$LOG(STS$K_WARN, "Cannot extract BroAgent Ident, work may be incorrect");


		agent_ctl_open (bufp, buflen, insd, insk, proto);			/* It's an initial connect request from Agent */
		}

	return	STS$K_SUCCESS;
}


static inline int	__patch_http_uri	(
			char	*bufp,
			int	 buflen,
			int	*retlen
				)
{
char	*cp1, *cp2;

	/* POST http://149.154.167.51:80/api HTTP/1.1 */
	for (cp1 = bufp; !isspace(*cp1); cp1++);

	for (; isspace(*cp1); cp1++);

	/* POST http://149.154.167.51:80/api HTTP/1.1
	**      ^
	**      |
	**      cp1
	*/
	if ( !(cp2 = strstr(cp1, "://")) )
		return	STS$K_ERROR;

	cp2 += 3;

	/* POST http://149.154.167.51:80/api HTTP/1.1
	**             ^
	**             |
	**             cp2
	*/
	if ( !(cp2 = strchr(cp2, '/')) )
		return	STS$K_ERROR;


	/* POST http://149.154.167.51:80/api HTTP/1.1
	**      ^                       ^
	**      |                       |
	**      cp1                     cp2
	*/
	memmove (cp1, cp2, buflen - (cp2 - bufp));

	*retlen = buflen - (cp2 - cp1);
	*(bufp + *retlen) = '\0';

	return	STS$K_SUCCESS;
}




int	req_process_http(
			RQ_CTX	*ctx,
			int	insd,
	struct sockaddr_in	*insk,
			char	*bufp,
			int	bufsz,
			int	proto
			)
{
int	status, outsd = -1, buflen, port = 0, flags;
ASC	host = {0}, auth = {0};
struct sockaddr_in outsk = {0};
BP_PEER	peer = {0};

	/* Read rest of the HTTP request */
	if ( !(1 & (status = __recv_header (insd, bufp + BP$SZ_MINLEN, bufsz - BP$SZ_MINLEN, &buflen))) )
		{
		close(insd);
		return	$LOG(STS$K_ERROR, "[#%d] Error receiving HTTP request", insd);
		}

	/* Now we have in the buffer HTTP header */
	buflen += BP$SZ_MINLEN;

	$IFTRACE(g_trace , "[#%d] Request %d octets\n%.*s", insd, buflen, buflen, bufp);

	/* Parse HTTP's request header, extract authorization vector */
	if ( !(1 & (status = __parse_http_header(bufp, buflen, proto, &host, &port,  &auth))) )
		xmit_n (insd, http_407, sizeof(http_407) - 1);
	/* Performs authorization by username or IP	*/
	else if ( !(1 & (status = auth_check (&auth, &insk->sin_addr, &flags))) )
		xmit_n (insd, http_407, sizeof(http_407) - 1);
	/* Check against BAN  List */
	else if ( !(1 & (status = ban_check (&auth, &insk->sin_addr))) )
		xmit_n (insd, http_403, sizeof(http_403) - 1);

	/* First set of checks has been completed, check status */
	if ( !(1 & status) )
		{
		close(insd);
		return	STS$K_ERROR;
		}


	if ( flags & BP$M_ACE_AGENT )						/* It's request from the Agent ? */
		{
		if (g_trace )
			{
			char	sia[32] = {0};
			inet_ntop(insk->sin_family, &insk->sin_addr, sia, sizeof (sia));

			$IFTRACE(g_trace, "[#%d] %s:%d-got agent's request", insd, sia, htons(insk->sin_port));
			}

		if ( !(1 & (status = process_agent(ctx, bufp, buflen, insd, insk, proto))) )
			{
			close(insd);
			return	STS$K_ERROR;
			}


		return	STS$K_SUCCESS;
		}



	__remove_proxy_auth (bufp, buflen, &buflen);				/* Remove Proxy-Authorization: from the HTTP header */

	__patch_http_uri(bufp, buflen, &buflen);				/* POST http://149.154.167.51:80/api HTTP/1.1
										--->> POST /api HTTP/1.1
										*/


	/* If running mode is Master-Agent dispatch request processing to */
	if ( g_connback )
		{
		if ( (1 & (status = req_to_agent(bufp, buflen, insd, insk))) )
			{
			/* Starting agent version X.00-99 a report by HTTP 200 is performed by
			 * BroAgent itself
			 */
			if ( g_agent_ver < $VERSION('X', 00, 99, 00) )
				{
				if ( proto == BP$K_PROTO_HTTPS )	/* Sent 200 as answer to the HTTPS CONNECT */
					xmit_n (insd, http_200, sizeof(http_200) - 1);
				}
			}
		else	{
			/*
			 * Return bad HTTP status code 504 to inform browser about temporal troubles with the
			 * proxying
			 */
			xmit_n (insd, http_504, sizeof(http_504) - 1);
			close(insd);
			}

		return	status;
		}


	/* At the time we are ready to connect to a target host or proxy peer */

	if ( 1 & peer_get(&peer) )
		{/* We got a proxy peer, performs connection to peer */
		if ( !(1 & (status = __connect_peer (&peer, bufp, buflen, &outsd, &outsk))) )
			xmit_n (insd, http_404, sizeof(http_404) - 1);
		else	{ /* Successfully connected to peer proxy */

			}
		}

	/* Establishing TCP-connection with the target end-point */
	else if ( !(1 & (status =  __connect (ctx->nsctx, &host, port, &outsd, &outsk))) )
		xmit_n (insd, http_404, sizeof(http_404) - 1);
	else	{
		/* In case of HTTP - resend original request to target host */
		if ( proto == BP$K_PROTO_HTTP )
			status = xmit_n (outsd, bufp, buflen);
		/* Sent 200 as answer to the HTTPS CONNECT */
		else if ( proto == BP$K_PROTO_HTTPS )
			status = xmit_n (insd, http_200, sizeof(http_200) - 1);
		}

	if ( 1 & status )
		/* Finally we can transfer main I/O processing to I/O Engine */
		return	status = io_enq(insd, insk, outsd, &outsk, NULL, &host, BP$K_PROTO_HTTP);

	close(insd);
	close(outsd);

	return	STS$K_ERROR;
}

/*
 *   DESCRIPTION: Process PRIVATE request. Read from socket a whole PRIVATE request,
 *	decode it, check against authorization and ban list, transfer processing to the Agent' specific
 *	routines.
 *
 *   INPUTS:
 *	ctx:	context
 *	insd:	socket descriptor
 *	insk:	socket structure of the remote
 *	bufp:	An I/O buffer, contains initial PRIVATE request sequence
 *	bufsz:	A size of the I/O buffer
 *
 *   OUTPUTS:
 *   RETURNS:
 */

/*
 * Tiny wrapper routine to implement obfuscate & send functionality.
 * Send marker sequence, length of the data buffer in NBO, data buffer.
 * Just for fun to get a power of the lambda-function in the old-schoole C programming
 * For true C badasses only!!!
 */
static inline int __obfuscate_n_send	(int agsd, char * buf, int buflen)
{
const  char marker[3] = {3, 5, 1};
int	len = htonl(buflen);

	xmit_n (agsd, marker, 3);					/* Send marqer sequence */
	xmit_n (agsd, &len, 4);						/* Send length of the data buffer */

	__swap_nibbles__(buf, buflen);					/* Obfuscate buffer */

	return xmit_n (agsd, buf, buflen);				/* Send 'n' return */
}



int	req_process_private (
			RQ_CTX	*ctx,
			int	agsd,
	struct sockaddr_in	*agsk,
			char	*bufp,
			int	bufsz
			)
{
int	status, buflen, port = 0, flags, rc;
ASC	host = {0}, auth = {0};

	/* The PRIVATE request is only from the BroAgent (tm)  in the connection-back mode !!! */
	if ( !g_connback )
		return	close(agsd), $LOG(STS$K_ERROR, "Illegal PRIVATE request in the non-connection back mode!");



	/* '1', '3', '5', <32bits_whole_request_length><request_body> */

	/*
	 '1', '3', '5', <32bits_whole_request_length> <request_body>
	+----+-----+-----+----------+-----------------+
	|    Marker      | length   | Request body
	+----+-----+-----+----------+-----------------+
	| 1  |  1  | 1   |  32 bits | Variable
	+----+-----+-----+----------+-----------------+
	*/

	/* Read 32bits of the request length */
	if ( !(1 & recv_n (agsd, &buflen, sizeof(buflen), &g_timers_set.t_req)) )
		return	close(agsd), $LOG(STS$K_ERROR, "Error receiving PRIVATE request length");

	buflen = ntohl(buflen);

	if ( buflen  > bufsz )
		return	close(agsd), $LOG(STS$K_ERROR, "PRIVATE request is too long (%d > %d octets)", buflen, bufsz);

	/* Read a rest of the request body */
	if ( !(1 & recv_n (agsd, bufp, buflen, &g_timers_set.t_req)) )
		return	close(agsd), $LOG(STS$K_ERROR, "Error receiving PRIVATE request body");

	__swap_nibbles__(bufp, buflen);						/* Decode request body */

	$IFTRACE(g_trace , "Request %d octets\n%.*s", buflen, buflen, bufp);

	/* Now we have in the buffer HTTP header */

	/* Parse HTTP's request header, extract authorization vector */
	if ( !(1 & (status = __parse_http_header(bufp, buflen, BP$K_PROTO_HTTPS, &host, &port,  &auth))) )
		return	close(agsd), STS$K_ERROR;
	/* Performs authorization by username or IP	*/
	else if ( !(1 & (status = auth_check (&auth, &agsk->sin_addr, &flags))) )
		return	close(agsd), STS$K_ERROR;
	/* Check against BAN  List */
	else if ( !(1 & (status = ban_check (&auth, &agsk->sin_addr))) )
		return	close(agsd), STS$K_ERROR;
	else	__remove_proxy_auth (bufp, buflen, &buflen);


	/* The PRIVATE request is only from agent */
	if ( !(flags & BP$M_ACE_AGENT) )
		return	close(agsd), $LOG(STS$K_ERROR, "[#%d] Illegal PRIVATE request from user", agsd);

	if ( g_trace )
		{
		char	sia[32] = {0};
		inet_ntop(agsk->sin_family, &agsk->sin_addr, sia, sizeof (sia));

		$IFTRACE(g_trace, "[#%d] %s:%d-got agent's PRIVATE request", agsd, sia, htons(agsk->sin_port));
		}

	if ( !(1 & (status = process_agent(ctx, bufp, buflen, agsd, agsk, BP$K_PROTO_PRIVATE))) )
		return	close(agsd), STS$K_ERROR;

	return	STS$K_SUCCESS;
}









void	*rq_thread	(
		RQ_CTX	*ctx
			)
{
int	status, insd = -1, proto;
struct sockaddr_in insk = {0};
socklen_t slen;
char	buf[8192];
struct	timespec now = {0, 0}, etime = {0, 0}, tmo = {10, 0};
struct pollfd pfd = {ctx->sd, POLLIN , 0};

	atomic_fetch_add(&cnt_th_current, 1);

	slen = sizeof(insk);
	getsockname(ctx->sd, (struct sockaddr*)&insk, &slen);

	$LOG(STS$K_INFO, "[#%d]Listen on %s:%d, nsctx=%#x", ctx->sd, inet_ntoa(insk.sin_addr), ntohs(insk.sin_port), ctx->nsctx );

	if ( listen(ctx->sd, g_backlog) < 0)
		$LOG(STS$K_ERROR, "listen(%d, %d), errno=%d", ctx->sd, g_backlog, errno);

	while ( !g_exit_flag )
		{
		/* Quick check that we need to performs auto-adjustment = exit */
		if ( atomic_load(&cnt_th_current) != atomic_load(&cnt_th_target) )
			{
			if ( status = clock_gettime(CLOCK_REALTIME, &now) )
				{
				$LOG(STS$K_ERROR, "clock_gettime->%d", status);
				continue;
				}

			__util$add_time (&now, &tmo, &etime);

			if ( status = pthread_mutex_timedlock(&th_exit_lock, &etime) )
				{
				$LOG(STS$K_ERROR, "pthread_mutex_timedlock()->%d, errno=%d", status, errno);
				continue;
				}

			if ( atomic_load(&cnt_th_current) > atomic_load(&cnt_th_target) )
				break;

			if ( status = pthread_mutex_unlock(&th_exit_lock) )
				$LOG(STS$K_ERROR, "pthread_mutex_timedlock()->%d, errno=%d", status, errno);
			}


		if( 0 >  (status = poll(&pfd, 1, timespec2msec (&g_timers_set.t_seq))) )
			{
			$LOG(STS$K_ERROR, "poll(%d, POLLIN)->%d, errno=%d", pfd.fd, status, errno);
			break;
			}
		else if ( (status == 1) && (pfd.revents & pfd.events) )
			status = 0;	/* Connected !!! */
		else	continue;


		slen = sizeof(insk);

		if ( 0 > (insd = accept(ctx->sd, (struct sockaddr*)&insk, &slen)) )
			{
			$LOG(STS$K_ERROR, "accept(%d), errno=%d", ctx->sd, errno);
			continue;
			}

		$LOG(STS$K_SUCCESS, "[#%d] Accept connection request from %s:%d on #%d", ctx->sd, inet_ntoa(insk.sin_addr), ntohs(insk.sin_port), insd);

		atomic_fetch_add(&cnt_rq, 1);

		if ( status = setsockopt(ctx->sd, IPPROTO_TCP, TCP_NODELAY, &one, sizeof(one)) )
			$LOG(STS$K_WARN, "setsockopt(#%d, IPPROTO_TCP, TCP_NODELAY)->%d, errno=%d", ctx->sd, status, errno);


		/* Read first portion of request from client */
		if ( !(1 & (status = recv_n (insd, buf, BP$SZ_MINLEN, &g_timers_set.t_seq))) )
			{
			$LOG(STS$K_ERROR, "[#%d]Error receiving initial sequence", insd);
			close(insd);
			continue;
			}

		if ( !(1 & (status = proto_match (buf, BP$SZ_MINLEN, &proto))) )
			{
			$LOG(STS$K_ERROR, "[#%d]Unmatched protocol", insd);
			close(insd);
			continue;
			}

		if ( proto == BP$K_PROTO_PRIVATE )
			req_process_private (ctx, insd, &insk, buf, sizeof(buf) );

		if ( (proto == BP$K_PROTO_SOCKS5) || (proto == BP$K_PROTO_SOCKS4) )
			{
			if ( proto == BP$K_PROTO_SOCKS4 )
				status = req_process_socks4 (ctx, insd, &insk, buf, sizeof(buf) );
			else if ( proto == BP$K_PROTO_SOCKS5 )
				status = req_process_socks5 (ctx, insd, &insk, buf, sizeof(buf) );
			else	close(insd);
			}

		else if ( (proto == BP$K_PROTO_HTTP) || (proto == BP$K_PROTO_HTTPS) )
			req_process_http (ctx, insd, &insk, buf, sizeof(buf), proto);


		}

	$IFTRACE(g_trace, "Stoping thread  ...");

	/* Cleanup */
	close(ctx->sd);
	ns_cleanup(ctx->nsctx);
	free (ctx) ;

	atomic_fetch_sub(&cnt_th_current, 1);

	if ( status = pthread_mutex_unlock(&th_exit_lock) )
		$LOG(STS$K_WARN, "pthread_mutex_timedlock()->%d, errno=%d", status, errno);

	$IFTRACE(g_trace, "Stop thread.");

	pthread_exit(NULL);
}

ASC	q_bind;

int	rq_crew_init	(
		ASC	*params,
		ASC	*s_bind
		)
{
int	status, i;
BP_DPOOL *p = &rq_params;
pthread_t	tid;
RQ_CTX	*ctx;

	sscanf ($ASCPTR(params), "%d,%d,%d,%d,%d,%d,%d,%d",
		&p->def, &p->min, &p->max, &p->inc, &p->dec, &p->delta, &p->hrate, &p->lrate);

	$LOG(STS$K_INFO, "Create RQ worker crew (def/min/max/inc/dec/delta/hrate/lrate=%d/%d/%d/%d/%d/%d/%d/%d)",
		p->def, p->min, p->max, p->inc, p->dec, p->delta, p->hrate, p->lrate);

	pthread_attr_init(&th_tattr);
	pthread_attr_setstacksize(&th_tattr, g_thstack /*PTHREAD_STACK_MIN * 2*/ );
	pthread_attr_setdetachstate(&th_tattr, PTHREAD_CREATE_DETACHED);

	q_bind = *s_bind;


	/* Set counters to initial states before starting of treads */
	atomic_init(&cnt_th_target, p->def);

	/* Create context pool with 'default' quantity of threads */
	for ( i = 0; i < p->def; i++ )
		{
		/* Allocate memory for new I/O thread context */
		if ( !(ctx = calloc (1, sizeof(RQ_CTX))) )
			{
			$LOG(STS$K_ERROR, "Cannot allocate %d octets for RQ thread, errno=%d", sizeof(RQ_CTX), errno);
			break;
			}

		if ( !(1 & (status = listener_init (s_bind, &ctx->sd))) )
			return	status;

		ctx->inreqs = 0;

		/* Initialize thread's own NS context */
		if ( !(1 & (status = ns_init(&ctx->nsctx))) )
			return	$LOG(status, "Error initialize thread's NS context");

		if ( status = pthread_create(&tid, &th_tattr, rq_thread, ctx) )
			{
			$LOG(STS$K_ERROR, "Cannot start RQ thread, pthread_create()->%d, errno=%d", status, errno);
			break;
			}
		else	pthread_setname_np(tid, "bproxy-rqp");
		}

	if ( !i )
		return	$LOG(STS$K_ERROR, "No RQ thread has been created!");


	clock_gettime(CLOCK_REALTIME, &ts_last_zero);

	ts_delta.tv_sec = p->delta;

	return	$LOG(STS$K_SUCCESS, "Requested %d RQ worker threads - %d has been created", p->def, i);
}



/*
 *   DESCRIPTION: I/O processor threads manager - analyze counters, start new threads, adjust target thread's quantity
 *	according DPOOL's configuration vector. This routine is supposed to be called at regular basis with interval has been
 *	defined in the configuration vector.
 *
 *
 *   IMPLICIT INPUT:
 *
 *   IMPLICIT OUTPUT:
 *
 *   RETURN:
 *	condition code
 */
int	rq_crew_manager	(void)
{
struct timespec now = {0}, etime = {0}, tmo = {3, 0};
int	status, rate, cur, tg, count;
RQ_CTX	*ctx = NULL;
pthread_t	tid;

	if ( (!rq_params.delta) || ((!rq_params.hrate) && (!rq_params.lrate)) )
		return	STS$K_SUCCESS;

	/* Is we reach an end of interval of checking ?*/
	clock_gettime(CLOCK_REALTIME, &now);

	__util$add_time(&ts_last_zero, &ts_delta, &etime);

	if ( 0 > __util$cmp_time (&now, &etime) )
		return	STS$K_SUCCESS;

	/* Performs rate computaion and checks */
	rate = atomic_load(&cnt_rq);
	rate /= ts_delta.tv_sec;

	/* Check that current rate is between <lowrate> and <highrate> */
	if ( __util$isinrange  (rate, rq_params.lrate, rq_params.hrate) )
		{
		atomic_store(&cnt_rq, 0);
		clock_gettime(CLOCK_REALTIME, &ts_last_zero);

		return	STS$K_SUCCESS;	/* Nothing to do !*/
		}


	/* So at this point we are ready to performs adjustment,
	 * so set "disable threads cancelation" flag
	 */
	__util$add_time (&now, &tmo, &etime);

	if ( status = pthread_mutex_timedlock(&th_exit_lock, &etime) )
		return	$LOG(STS$K_ERROR, "pthread_mutex_timedlock()->%d, errno=%d", status, errno);

	cur = atomic_load(&cnt_th_current);
	tg = atomic_load(&cnt_th_target);

	$IFTRACE(g_trace, "RQ threads : current=%d, target=%d, rate=%d", cur, tg, rate);

	if ( rate > rq_params.hrate )
		tg = cur + rq_params.inc;

	if ( rate < rq_params.lrate )
		tg = cur - rq_params.dec;

	tg = __util$range (tg, rq_params.min, rq_params.max);

	atomic_store(&cnt_th_target, tg);

	if ( status = pthread_mutex_unlock(&th_exit_lock) )
		return	$LOG(STS$K_ERROR, "pthread_mutex_timedlock()->%d, errno=%d", status, errno);

	atomic_store(&cnt_rq, 0);
	clock_gettime(CLOCK_REALTIME, &ts_last_zero);

	/* Do we need start additional threads ? */
	if ( cur < tg )
		{
		for ( count = tg - cur; count; count--)
			{
			/* Allocate memory for new I/O thread context */
			if ( !(ctx = calloc (1, sizeof(RQ_CTX))) )
				{
				$LOG(STS$K_ERROR, "Cannot allocate %d octets for RQ thread, errno=%d", sizeof(RQ_CTX), errno);
				break;
				}

			if ( !(1 & (status = listener_init (&q_bind, &ctx->sd))) )
				return	free(ctx), status;

			ctx->inreqs = 0;

			/* Initialize thread's own NS context */
			if ( !(1 & (status = ns_init(&ctx->nsctx))) )
				{
				/* Cleanup */
				free (ctx);
				$LOG(status, "Error initialize thread's NS context");
				break;
				}

			if ( status = pthread_create(&tid, &th_tattr, rq_thread, ctx) )
				{
				/* Cleanup */
				ns_cleanup(ctx->nsctx);
				free (ctx) ;
				$LOG(STS$K_ERROR, "Cannot start RQ thread, pthread_create()->%d, errno=%d", status, errno);
				break;
				}
			else	pthread_setname_np(tid, "bproxy-rqp");
			}
		}


	return	STS$K_SUCCESS;
}
