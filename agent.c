#define	__MODULE__	"AGENT"
#define	__IDENT__	"X.00-45"

#ifdef	__GNUC__
	#pragma GCC diagnostic ignored  "-Wparentheses"
	#pragma	GCC diagnostic ignored	"-Wunused-variable"
	#pragma	GCC diagnostic ignored	"-Wunused-parameter"
	#pragma	GCC diagnostic ignored	"-Wdiscarded-qualifiers"
#endif

/*
**++
**
**  FACILITY:  BroProxy - A yet another SOCKS5(4), HTTP(s) high performance proxy server
**
**  DESCRIPTION: A set of routines implement support of "connected back" proxies.
**
**  DESIGN ISSUE:
**
**
**
**  AUTHORS: Ruslan R. (The BadAss SysMan) Laishev
**
**  CREATION DATE:  31-MAY-2019
**
**  MODIFICATION HISTORY:
**
**	17-JUN-2019	RRL	Added locking of signaling channel.
**
**	19-JUN-2019	RRL	Ignore agent's request for control channel - keep firstly created.
**
**	11-SEP-2019	RRL	Removed agent_purge() routine has been added at previous edition of the module.
**
**	 2-DEC-2019	SYS	X.00-12 : Improved communication error handling in the req_to_agent();
**				improved messaging;
**
**	 3-DEC-2019	RRL	X.00-13 : Changes to implement "Keep Alive checking" of Agent's control channel
**				X.00-14 : Removed setsockopt(SO_KEEPALIVE)
**
**	10-DEC-2019	RRL	X.00-15 : Improved diagnostic messages.
**
**	25-FEB-2020	RRL	X.00-16 : Improved diagnostic messages.
**
**	26-FEB-2020	RRL	X.00-17 : Added logic of checking of non-completed in 't_conn' outstanding requests.
**
**	29-MAR-2020	RRL	X.00-20 : Changed HTTP's statuses according to RFC
**
**	11-OCT-2020	RRL	X.00-30 : Migrated  RB Tree API;
**
**	20-OCT-2020	RRL	X.00-31 : Fixed deadlocking
**
**	 1-NOV-2020	RRL	X.00-40 : Migrated to AVL API;
**
**	 8-APR-2021	RRL	X.00-41 : Added obfuscation of the client's HTTP request to the Agent.
**
**	22-NOV-2021	RRL	X.00-42 : Fixed bug with error handling in  the __wreq_alloc().
**
**	25-NOV-2021	RRL	X.00-43 : Fixed incorrect WREQ context allocation/releasing.
**
**	27-NOV-2021	RRL	X.00-44 : Removed unneeded locking of signaling channel.
**
**	 3-DEC-2021	RRL	X.00-45 : Added some diagnostic in the agent_ctl_check()
**
**
**--
*/

#include	<stdlib.h>
#include	<stdio.h>
#include	<pthread.h>
#include	<errno.h>
#include	<memory.h>
#include	<arpa/inet.h>
#include	<strings.h>
#include	<unistd.h>
#include	<time.h>
#include	<sys/types.h>
#include	<sys/socket.h>
#include	<stdatomic.h>


#define		__FAC__	"BPROXY"
#define		__TFAC__ __FAC__ ": "						/* Special prefix for $TRACE			*/
#include	"utility_routines.h"

#include	"avl.h"								/* AVL Tree API */
#include	"bproxy.h"

extern int	g_exit_flag, g_trace,
		g_wreqlim;							/* A number of outstanding request to the agent */
extern	BP_TIMERS	g_timers_set;

static struct sockaddr_in g_agent_sk = {0};
static int		g_agent_sd = -1;
pthread_mutex_t		g_agent_sd_lock = PTHREAD_MUTEX_INITIALIZER;


#define	HTTP_AGENT	"StarLet/" __IDENT__ "/" __ARCH__NAME__  " (built  at "__DATE__ " " __TIME__ ")"
static const char http_200 []  = {"HTTP/1.0 200 OK" CRLF "Proxy-Agent: " HTTP_AGENT CRLFCRLF},
		http_504 []  = {  "HTTP/1.0 504 Gateway Timeout"    CRLF "Proxy-Agent: " HTTP_AGENT CRLFCRLF};

const char	starlet_sig[ sizeof(unsigned long long) ] = {"$tar1et"};
const unsigned long long *starlet_sigull = (unsigned long long *) &starlet_sig;

typedef	union	__bp_wreq__	{
			ENTRY	ent;
	struct {
	unsigned long long	id;						/* Request Id = Request's primary key */
			int	insd;
	struct sockaddr_in	insk;
	struct timespec		expdt;						/* Expire date-time for the outstanding context		*/
	};
} BP_WREQ;

typedef	union	__bp_wreq_node__ {
			ENTRY	ent;
	avl_node_t	node;
} BP_WREQ_NODE;





static pthread_mutex_t	wreq_lock;						/* Mutex to coordinate access to BP_WREQ and RB_NODE contexts */
static atomic_ullong	wreq_id;
static BP_WREQ_NODE	*wreq_node_area = NULL;					/* Area to keep Tree Node(-s) */
static BP_WREQ		*wreq_data_area = NULL;						/* Area to keep BP_WREQ */
static __QUEUE	wreq_free_list = QUEUE_INITIALIZER,				/* A list to keep free BP_WREQ entries */
	node_free_list = QUEUE_INITIALIZER;					/* A list to keep free RB Node entries */

static avl_tree_t	wreq_tree = {0};					/* A tree to keep outstanding BP_WREQ entries */
static pthread_mutex_t	wreq_tree_lock;						/* Mutex to coordinate access to BP_WREQ and RB_NODE contexts */

enum	{
	BP$M_OBFUSCATION = (1 << 0),						/* A flag to enable obfuscation of data exchange betwwen
										  master-agent */
};

static int		g_agent_flags = 0;					/* See BM$M_* */

static	avl_compare_t	__wreq_id_cmp	(
			const void	*id1,
			const void	*id2
			)
{
	return 	(*((unsigned long long *)id1)) - (*((unsigned long long *)id2));
}


static	avl_freeitem_t	__wreq_freeitem	(
			void	*item
			)
{
		return NULL;
}



static inline int	__wreq_alloc	(
			BP_WREQ		**req,
			avl_node_t	**node
			)
{
int	count = 0, status = 0;
BP_WREQ	*pwreq;
BP_WREQ_NODE	*pn;

	pthread_mutex_lock(&wreq_lock);

	/* Allocate from pools data and node */
	status = $REMQHEAD(&wreq_free_list, &pwreq, &count);
	$IFTRACE(g_trace, "status=%#x, count=%d", status, count);

	if ( !(1 & status) || (!count) )
		status = $LOG(STS$K_ERROR, "No free BP_WREQ contexts, status=%#x, count=%d", status, count);
	else	{
		status = $REMQHEAD(&node_free_list, &pn, &count);

		if ( !(1 & status) || (!count) )
			status = $LOG(STS$K_ERROR, "No free Tree node contexts");
		else	{
			/* Zeroing node and request area from old content */
			__util$bzero(pn, sizeof(BP_WREQ_NODE));
			__util$bzero(pwreq, sizeof(BP_WREQ));

			avl_init_node(pn, pwreq);

			/* Set request Id, this field is used to be a record's key */
			if ( !(pwreq->id = atomic_fetch_add(&wreq_id, 1)) )
				pwreq->id = atomic_fetch_add(&wreq_id, 1);

			if ( pwreq->id == *starlet_sigull )
				if ( !(pwreq->id = atomic_fetch_add(&wreq_id, 1)) )
					pwreq->id = atomic_fetch_add(&wreq_id, 1);

			*req = pwreq;
			*node = (avl_node_t *) pn;
			}
		}

	pthread_mutex_unlock(&wreq_lock);


	return	status;
}


static  inline int	__wreq_release	(
			BP_WREQ		*req,
			avl_node_t	*node
			)
{
int	count, status;
const static ENTRY	zero_entry = {0};


	pthread_mutex_lock(&wreq_lock);

	req->ent = zero_entry;
	((BP_WREQ_NODE *) node)->ent = zero_entry;

	status = $INSQHEAD(&node_free_list, node, &count);
	$IFTRACE(g_trace, "status=%#x, count=%d", status, count);
	status = $INSQHEAD(&wreq_free_list, req, &count);
	$IFTRACE(g_trace, "status=%#x, count=%d", status, count);

	pthread_mutex_unlock(&wreq_lock);

	return	STS$K_SUCCESS;
}


static  inline int	wreq_init	(void)
{
int	count, i, status;
BP_WREQ	*pwreq;
BP_WREQ_NODE	*pn;

	pthread_mutex_lock(&wreq_lock);

	/* Allocate VM areas for the BP_WREQ and Tree nodes ... */
	if ( wreq_data_area )
		return	pthread_mutex_unlock(&wreq_lock), STS$K_SUCCESS;

	if ( !(pwreq = wreq_data_area = calloc(g_wreqlim, sizeof(BP_WREQ))) )			/* Create VM Area to keep deffered requests information */
		return	pthread_mutex_unlock(&wreq_lock), $LOG(STS$K_ERROR, "Error allocation VM (%d octets) for %d deffered requests, errno=%d",
			     g_wreqlim * sizeof(BP_WREQ), g_wreqlim, errno);

	if ( !(pn = wreq_node_area = calloc(g_wreqlim, sizeof(BP_WREQ_NODE))) )			/* Create VM Area to keep RB Nodes */
		return	pthread_mutex_unlock(&wreq_lock), $LOG(STS$K_ERROR, "Error allocation VM (%d octets) for %d Tree nodes, errno=%d",
			     g_wreqlim * sizeof(avl_node_t), g_wreqlim, errno);

	/* Initializw pools ... */
	for ( i = g_wreqlim; i--; pwreq++, pn++)
		{
		$INSQTAIL(&wreq_free_list, pwreq, &count);
		$INSQTAIL(&node_free_list, pn, &count);
		}

	/* Set initial value for Request Id */
	atomic_store(&wreq_id, (atomic_ullong)  wreq_data_area);


	/* Initialize AVL Tree context */
	avl_init_tree(&wreq_tree, __wreq_id_cmp, __wreq_freeitem);

	pthread_mutex_unlock(&wreq_lock);


	return	$LOG(STS$K_SUCCESS, "VM (%d octets) for %d deffered requests has been allocated", g_wreqlim * sizeof(BP_WREQ), g_wreqlim);
}



/*
 *  DESCRIPTION: Process request from the Agent to establishing control channel.
 *
 *  INPUT:
 *
 *	bufp:	A buffer with HTTP header of request  from Agent
 *	buflen:	HTTP header length
 *	sd:	Agent socket descriptor
 *	sk:	Agent socket
 *	proto:	A protocol of connection of agent
 *
 *  IMPLICIT OUTPUT:
 *	g_agent_sd, g_agent_sk
 *
 *  IMPLICITE_OUTPUT;
 *	g_agent_flags
 *
 *  RETURN:
 *	condition code
 */
int	agent_ctl_open	(
			char	*bufp,
			int	buflen,
			int	sd,
	struct sockaddr_in	*sk,
			int	proto
			)
{
char	buf[128];
int	status, rc;

	if ( !(1 & wreq_init()) )
		return	$LOG(STS$K_SUCCESS, "Error during initialization of control channel");

	/* Is there has been established control channel ? */
	if ( g_agent_sd > 0 )
		{
		$LOG(STS$K_WARN, "[#%d] Close previously opened control channel", g_agent_sd);
		close(g_agent_sd);
		}

	g_agent_sd = sd;
	g_agent_sk = *sk;

	g_agent_flags = 0;
	g_agent_flags |= ( (proto == BP$K_PROTO_PRIVATE) ? BP$M_OBFUSCATION : 0 );

	/* Send an answer to the HTTP' CONNECT request */
	if (  g_agent_flags & BP$M_OBFUSCATION )
		rc = htonl(HTTP$K_200), status = xmit_n (sd, &rc, sizeof(rc) );		/* 200 */
	else	status = xmit_n (sd, http_200, sizeof(http_200) - 1);			/* HTTP's 200 */

	$IFTRACE(g_trace, "[#%d] Sent (%d octets) '%s', status=%#x", sd, sizeof(http_200) - 1, http_200, status);

	return	$LOG(STS$K_SUCCESS, "[#%d] Agent's control channel is established with %s:%d", g_agent_sd,
		     inet_ntop(AF_INET, &g_agent_sk.sin_addr, buf, sizeof(buf)), htons(g_agent_sk.sin_port));
}


/*
 *  DESCRIPTION: Close session with the agent, free local resourses.
 *
 *  INPUTS:
 *	NONE
 *
 *  IMPLICIT OUTPUT:
 *	g_agent_sd, g_agent_sk
 *
 *  RETURN:
 *	condition code
 */
int	agent_ctl_shut	( void )
{
int	status, i;
BP_WREQ	*pwreq;
avl_node_t *pn, *pn2;
char	buf[128];


	if ( g_agent_sd < 0 )
		return	$LOG(STS$K_WARN, "Control channel has not been established");

	$LOG(STS$K_INFO, "Shutdown connection back Agent ...");

	/* Run over contexts pool, close browser side socket */
	status = pthread_mutex_lock(&wreq_tree_lock);

	for (pn = wreq_tree.head; pn; )
		{
		pn2 = pn;					/* Save current NODE into the PN2 */
		pn = pn->next;					/* Get node to be processed at next iteration */


		pwreq = pn2->item;				/* Get address of the request area */
		close(pwreq->insd);				/* Close network socket of the browser connection */


		avl_unlink_node(&wreq_tree, pn2);		/* Delete from the RB tree the node */
		__wreq_release(pn2->item, pn2);			/* Return to the pool node and request contexts */
		}

	status = pthread_mutex_unlock(&wreq_tree_lock);

	if ( status = pthread_mutex_lock(&g_agent_sd_lock) )
		$LOG(STS$K_WARN, "pthread_mutex_lock()->%d, errno=%d", status, errno);

	$LOG(STS$K_SUCCESS, "[#%d] Close agent's control channel with %s:%d", g_agent_sd,
		     inet_ntop(AF_INET, &g_agent_sk.sin_addr, buf, sizeof(buf)), htons(g_agent_sk.sin_port));


	close(g_agent_sd);
	g_agent_sd = -1;

	/* Unlock signaling channel */
	if ( pthread_mutex_unlock(&g_agent_sd_lock) )
		$LOG(STS$K_WARN, "pthread_mutex_unlock(), errno=%d", errno);

	return	STS$K_SUCCESS;

}


/*
 *  DESCRIPTION: Performs forwarding of  HTTP request has been received from browser to Agent over has been established
 *		control channel ( see ctl_from_agent() ). Allocate local context structure to keep information
 *		about of outstanding request - assign Request Id.
 *
 *  INPUT:
 *
 *	bufp:	A buffer with HTTP header of request  from Agent
 *	buflen:	HTTP header length
 *	sd:	Client (browser side) socket descriptor
 *	sk:	Client (browser side) socket
 *
 *  RETURN:
 *	condition code
 */

int	req_to_agent   (void *bufp, int buflen, int sd, struct sockaddr_in *sk)
{
int	status, rc;
BP_WREQ *wreq;
avl_node_t	*pn;
struct	timespec now = {0, 0}, etime = {0, 0}, tmo = {10, 0};

	if ( 0 > g_agent_sd )
		return	$LOG(STS$K_ERROR, "No proxy Agent has been connected");

	$IFTRACE(g_trace, "[#%d-]Forward %d octets\n%.*s", sd, buflen, buflen, bufp);


	/* Allocate new context */
	if ( !(1 & __wreq_alloc(&wreq, &pn)) )
		return	$LOG(STS$K_ERROR, "Context allocation error");

	/* Store information to be used on connection back request */
	wreq->insd = sd;
	wreq->insk = *sk;

	/*
	 * Store expiration date-time into context - it will help to purge non-completed
	 * deffered requests...
	 */
	if ( status = clock_gettime(CLOCK_REALTIME, &now) )	/* Get time to use on follows ... */
		$LOG(STS$K_ERROR, "clock_gettime->%d", status);

	__util$add_time (&now, &g_timers_set.t_conn, &wreq->expdt);


	$IFTRACE(g_trace, "Insert into the Tree ReqId=%llx, client sd=#%d", wreq->id, sd);

	rc = pthread_mutex_lock(&wreq_tree_lock);				/* Get lock on RB Tree operation */
	status = avl_insert_node(&wreq_tree, pn) ? STS$K_SUCCESS : STS$K_ERROR;	/* Insert new context */
	rc  = pthread_mutex_unlock(&wreq_tree_lock);				/* Unlock RB Tree */

	if ( !(1 & (status)) )							/* Check result of insert into the RB Tree */
		{
		__wreq_release(pn->item, pn);
		return	$LOG(STS$K_ERROR, "Cannon insert new context into the Tree");
		}


	/* Lock signaling channel for write */
	__util$add_time (&now, &tmo, &etime);

	$IFTRACE(g_trace, "[%llx, sd=#%d]Locking control channel ... ", wreq->id, sd);

	if ( status = pthread_mutex_timedlock(&g_agent_sd_lock, &etime) )
		return	$LOG(STS$K_ERROR, "pthread_mutex_timedlock()->%d, errno=%d", status, errno);

	$IFTRACE(g_trace, "[%llx, sd=#%d]Control channel is locked", wreq->id, sd);
	$IFTRACE(g_trace, "[%llx] Sending %d octets\n%.*s", wreq->id, buflen, buflen, bufp);


	if ( !(1 & (status = xmit_n (g_agent_sd, &wreq->id, sizeof(wreq->id)))) )			/* Send Request Id */
		$LOG(status, "[#%d-]Error send ReqId to Agent, errno=%d", sd, errno);
	else	{
		if (  g_agent_flags & BP$M_OBFUSCATION )
			{
			rc = htonl(buflen);								/* Send data length prefix */
			status = xmit_n (g_agent_sd, &rc, sizeof(rc) );

			__swap_nibbles__(bufp, buflen);
			}

		if ( !(1 & (status = xmit_n (g_agent_sd, bufp, buflen))) )				/* Send HTTP' request */
			$LOG(status, "[#%d-]Error send HTTP request to Agent, errno=%d", sd, errno);
		}

	/* Unlock signaling channel */
	if ( pthread_mutex_unlock(&g_agent_sd_lock) )
		$LOG(STS$K_ERROR, "pthread_mutex_unlock(), errno=%d", errno);

	$IFTRACE(g_trace, "[%llx, sd=#%d]Control channel is unlocked", wreq->id, sd);

	/* In case of communication error - close control channel */
	if ( !(1 & status ))
		{
		$LOG(STS$K_INFO, "Close Agent's control channel");
		close(g_agent_sd);

		g_agent_sd = -1;

		$LOG(status, "[#%d-]Error processing request", sd);
		}


	return	status;
}

/*
 *  DESCRIPTION: Process an answer from the Agent as a result of processing has been forwarded HTTP request ( see req_to_agent() )
 *
 *  INPUT:
 *
 *	bufp:	A buffer with HTTP header of request  from Agent
 *	buflen:	HTTP header length
 *	sd:	Agent socket descriptor
 *	sk:	Agent socket
 *	reqid:	Reuqest Id ( see req_to_agent() )
 *
 *  RETURN:
 *	condition code
 */

int	ans_from_agent (void *bufp, int buflen, int agsd, struct sockaddr_in *agsk, ASC *rqid, int proto)
{
unsigned long long id = 0;
BP_WREQ *wreq;
avl_node_t *pn;
char	*endptr = NULL;
int	status, rc;
struct	timespec now = {0, 0}, etime = {0, 0}, tmo = {10, 0};

	/* ReqId is a 64 bits hexadecimal, so convert to binary */
	id = strtoull($ASCPTR(rqid), &endptr, 16);

	$IFTRACE(g_trace, "ReqId=%llx", id);

	/* Find outstanding request context in the tree, if context is there - remove it from tree */
	pthread_mutex_lock(&wreq_tree_lock);

	if ( pn =  avl_search(&wreq_tree, &id) )
		avl_unlink_node(&wreq_tree, pn);

	pthread_mutex_unlock(&wreq_tree_lock);

	if ( !pn )
		return	$LOG(STS$K_ERROR, "Corrupted or expired ReqId=%llx", id);

	/* We retrieve WREQ context which is keep information about of
	 * outstanding request.
	 * Don't forget to release it!
	 */
	wreq = pn->item;

#if 0
	/* Lock signaling channel for write */
	if ( status = clock_gettime(CLOCK_REALTIME, &now) )
		$LOG(STS$K_ERROR, "clock_gettime->%d", status);

	__util$add_time (&now, &tmo, &etime);

	$IFTRACE(g_trace, "[%llx, sd=#%d]Locking control channel ... ", id, agsd);

	if ( status = pthread_mutex_timedlock(&g_agent_sd_lock, &etime) )
		return	$LOG(STS$K_ERROR, "pthread_mutex_timedlock()->%d, errno=%d", status, errno);

	$IFTRACE(g_trace, "[%llx, sd=#%d]Control channel is locked", id, agsd);
#endif


	/* Send an answer to the HTTP' CONNECT request */
	if (  g_agent_flags & BP$M_OBFUSCATION )
		rc = htonl(HTTP$K_200), status = xmit_n (agsd, &rc, sizeof(rc) );	/* 200 */
	else	status = xmit_n (agsd, http_200, sizeof(http_200) - 1);			/* HTTP's 200 */

	$IFTRACE(g_trace, "200 is sent, status=%#x", status);

	/* Finally we can transfer main I/O processing to I/O Engine */
	status = io_enq(wreq->insd, &wreq->insk, agsd, agsk, NULL, NULL, proto);


#if 0
	/* Unlock signaling channel */
	if ( pthread_mutex_unlock(&g_agent_sd_lock) )
		$LOG(STS$K_ERROR, "pthread_mutex_unlock(), errno=%d", errno);

	$IFTRACE(g_trace, "[%llx, sd=#%d]Control channel is unlocked", id, agsd);
#endif


	/* Free context */
	__wreq_release (wreq, pn);


	$IFTRACE(g_trace, "status=%#x", status);

	return	status;
}




/*
 *  DESCRIPTION: Performs maintenance task: send 'keep-alive' sequence, scan for expired contexts.
 *
 *  INPUTS:
 *	NONE
 *
 * IMPLICITE OUTPUTS:
 *
 *	wreq_free_list
 *	rb_free_list
 *
 *  OUTPUTS:
 *	NONE
 *
 *  RETURN:
 *	condition code
 */

int	agent_ctl_check   ( void )
{
int	rc, j, j2, status, status2;
struct timespec now;
static struct timespec nextrun = {0};	/* Keep next time to performs keep-alive checking and context purging */
BP_WREQ	*pwreq;
avl_node_t	*pn, *pn2;

	/* Do we need something do at all ? If not signaling channel has been established - just exit */
	if ( 0 > g_agent_sd )
		return	STS$K_SUCCESS;

	if ( status = clock_gettime(CLOCK_REALTIME, &now) )
		$LOG(STS$K_ERROR, "clock_gettime()->%d", status);

	/* Don't try to scan too frequently ... */
	if ( 0 < ( __util$cmp_time(&nextrun, &now)) )
		return	STS$K_SUCCESS;

	/* Try to allocate signaing channel for exclusive access */
	if ( !pthread_mutex_trylock(&g_agent_sd_lock) )
		{
		/* Send fake request with special Request Id to check that TCP-conection is alive */
		status = xmit_n (g_agent_sd, starlet_sig, sizeof(starlet_sig));
		$IFTRACE(g_trace, "Sent Keep-Alive, status=%#x", status);

		/* Unlock signaling channel */
		if ( rc = pthread_mutex_unlock(&g_agent_sd_lock) )
			$LOG(STS$K_WARN, "pthread_mutex_unlock()->%d, errno=%d", rc, errno);
		}

	/* Performs scanning of outstanding request older then t_conn */
	/* Get an exclisive access to the request table */
	$IFTRACE(g_trace, "Locking WREQ Tree ...");

	status2 = pthread_mutex_lock(&wreq_tree_lock);

	$IFTRACE(g_trace, "Start scanning fo expired context/request ...");

	for ( j = j2 = 0, pn = wreq_tree.head; pn; )
		{
		pn2 = pn;					/* Save current NODE into the PN2 */
		pn = pn->next;					/* Get node to be processed at next iteration */


		pwreq = pn2->item;				/* Get address of the request area */

		j2++;						/* Adjust total scanned records */

								/* Is the request is expired ? */
		if ( 0 < ( __util$cmp_time(&pwreq->expdt, &now)) )
			continue;

		$LOG(STS$K_ERROR, "[%llx, #%d]Request has not been completed in %d msec", pwreq->id, pwreq->insd, timespec2msec (&g_timers_set.t_conn));

		/* Report by HTTP 504 to originator of HTTP request */
		status2 = xmit_n (pwreq->insd, http_504, sizeof(http_504) - 1);
		$IFTRACE(g_trace, "[#%d]Sent '%.*s', status=%#x", pwreq->insd, sizeof(http_504) - 1, http_504, status2 );

		/* Close socket, context should be closed by I/O processor */
		close(pwreq->insd);

		avl_unlink_node(&wreq_tree, pn2);	/* Delete from the RB tree the node */
		__wreq_release(pn2->item, pn2);		/* Return to the pool node and request contexts */

		j++;	/* Adjust purged records count */
		}

	status2 = pthread_mutex_unlock(&wreq_tree_lock);

	/* Adjust 'next time to check run' */
	__util$add_time(&now, &g_timers_set.t_conn, &nextrun);

	$IFTRACE(g_trace, "%d context scanned, %d purged", j2, j);

	return	status;
}
