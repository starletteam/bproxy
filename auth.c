#define	__MODULE__	"AUTH"
#define	__IDENT__	"X.00-02"

#ifdef	__GNUC__
	#pragma GCC diagnostic ignored  "-Wparentheses"
	#pragma	GCC diagnostic ignored	"-Wunused-variable"
#endif

#ifdef _WIN32
	#pragma once
	#define WIN32_LEAN_AND_MEAN             // Exclude rarely-used stuff from Windows headers
#endif

/*
**++
**
**  FACILITY:  BroProxy - A yet another SOCKS5(4), HTTP(s) high performance proxy server
**
**  DESCRIPTION: This module cover authentication/authorization  functionality. Process configuration option,
**	load and parse configuration files, initialize internal contexts ...
**
**  DESIGN ISSUE: {tbs}
**
**  AUTHORS: Ruslan R. (The BadAss SysMan) Laishev
**
**  CREATION DATE:  11-MAR-2019
**
**  MODIFICATION HISTORY:
**
**	19-MAR-2019	RRL	Recoded to using new ACL syntax:
**
**	<action>=<ip_spec>
**	action:
**		ACCEPT
**		REJECT
**	ip_spec:
**		<ip>/<cidr_bits>
**		<ip>/<netmask>
**		<ip>
**
**	28-MAY-2019	RRL	X.00-02 : Recoded auth_load_user() to process file more clearly.
**
**	 6-JUL-2019	RRL	Fixed bug of diagnositc message.
**
**--
*/

#include	<stdlib.h>
#include	<stdio.h>
#include	<pthread.h>
#include	<errno.h>
#include	<poll.h>
#include	<sys/epoll.h>
#include	<fcntl.h>
#include	<memory.h>
#include	<arpa/inet.h>
#include	<strings.h>
#include	<unistd.h>
#include	<time.h>
#include	<sys/types.h>
#include	<sys/socket.h>
#include	<netdb.h>

#define		__FAC__	"BPROXY"
#define		__TFAC__ __FAC__ ": "		/* Special prefix for $TRACE			*/
#include	"utility_routines.h"


#include	"bproxy.h"
#include	"base64.h"

extern int	g_exit_flag,
		g_trace,
		g_authmode;


BP_AUTH_DATA	auth_data [BP$K_MAXACE];
int	auth_data_count = 0;


static const	socklen_t slen = sizeof(struct sockaddr);

/*
 *   DESCRIPTION: Read file line-by-line given text file, parse IP address notation in the several forms:
 *	ip/mask
 *	ip/bits
 *	ip
 *	skip empty or unrecognized/illformed lines.
 *
 *   INPUT:
 *	dbsource:	File specification to be processed
 *
 *   IMPLICIT OUTPUT:
 *	auth_data, auth_data_count
 *
 *   RETURN:
 *	condition code
 */
int	auth_load_ip	(
		ASC	*dbsource
			)
{
int	status = 0, bmask = 0, i ;
FILE	*finp;
char	buf[128], action[32], sip[32], smask[32], c;
BP_AUTH_DATA	*pauth;

	if ( !(finp = fopen ($ASCPTR(dbsource), "r")) )
		return	$LOG(STS$K_ERROR, "Error open '%.*', errno=%d", $ASC(dbsource), errno);

	for ( i = 1, status = 0, pauth = &auth_data[auth_data_count];  !feof(finp); i++)
		{
		/* Get string from file */
		fgets(buf, sizeof(buf), finp);

		if ( !(status = __util$uncomment (buf, strnlen(buf, sizeof(buf)), '!')) )
			continue;

		if ( !(status = __util$collapse (buf, strnlen(buf, sizeof(buf)))) )
			continue;

		buf[status] = '\0';

		/*
		 * ACCEPT=172.16.0.0/24
		 * ACCEPT=172.16.0.0/255.255.0.0
		 * REJECT=172.16.1.1
		 */
		action[0] = 0;

		if ( 3 == (status = sscanf(buf, "%8[^=]=%17[0-9.]/%17[0-9.]", action, sip, smask)) )
			{
			status = inet_pton(AF_INET, sip, &pauth->net);

			if ( !(status = inet_pton(AF_INET, smask, &pauth->mask)) )
				{
				status = atoi(smask);
				bmask = 0xFFFFFFFF << (32 - status);
				bmask = htonl(bmask);
				memcpy(&pauth->mask, &bmask, sizeof(bmask));
				}

			c = toupper(action[0]);

			if ( c == 'A' )
				pauth->flags = BP$M_ACE_ACCEPT;
			else if ( c == 'R' )
				pauth->flags = BP$M_ACE_REJECT;
			else	{
				$LOG(STS$K_ERROR, "Unrecognized action keyword '%s'", action);
				continue;
				}

			auth_data_count++;
			pauth++;
			}
		else if ( 2 == (status = sscanf(buf, "%8[^=]=%17[0-9.]", action, sip)) )
			{
			status = inet_pton(AF_INET, sip, &pauth->net);

			c = toupper(action[0]);

			if ( c == 'A' )
				pauth->flags = BP$M_ACE_ACCEPT;
			else if ( c == 'R' )
				pauth->flags = BP$M_ACE_REJECT;
			else	{
				$LOG(STS$K_ERROR, "Unrecognized action keyword '%s'", action);
				continue;
				}

			auth_data_count++;
			pauth++;
			}
		else	$LOG(STS$K_WARN, "Skip unrecognized line #%d='%s'", buf);
		}

	fclose(finp);

	for ( pauth = auth_data, i = auth_data_count; i--; pauth++ )
		{
		inet_ntop(AF_INET, &pauth->net, sip, slen);
		inet_ntop(AF_INET, &pauth->mask, smask, slen);

		$IFTRACE(g_trace, "action=%s, ip='%s'/mask='%s'", pauth->flags & BP$M_ACE_ACCEPT ? "Accept" : "Reject", sip, smask);
		}

	return	status;
}



/*
 *   DESCRIPTION: Read file line-by-line given text file, parse, extract user and password field;
 *	skip empty or unrecognized/illformed lines. We expect user's line in the format:
 *	<user>:<password>
 *
 *   INPUT:
 *	dbsource:	File specification to be processed
 *
 *   IMPLICIT OUTPUT:
 *	auth_data
 *
 *   RETURN:
 *	condition code
 */

int	auth_load_user	(
		ASC	*dbsource
			)
{
int	status = 0, i;
FILE	*finp;
char	buf[128], suser[BP$SZ_USER] = {0}, spass[BP$SZ_PASS] = {0}, action[32], c;
BP_AUTH_DATA	*pauth;

	if ( !(finp = fopen ($ASCPTR(dbsource), "r")) )
		return	$LOG(STS$K_ERROR, "Error open '%.*s, errno=%d", $ASC(dbsource), errno);

	for ( i = 1, status = 0, pauth = &auth_data[auth_data_count];  !feof(finp); i++)
		{
		/* Get string from file */
		fgets(buf, sizeof(buf), finp);

		if ( !(status = __util$uncomment (buf, strnlen(buf, sizeof(buf)), '!')) )
			continue;

		if ( !(status = __util$collapse (buf, strnlen(buf, sizeof(buf)))) )
			continue;

		buf[status] = '\0';
		/*
		 * USER=<user>:<password>
		 * AGENT=<user>:<password>
		 */
		action[0] = 0;

		if ( 3 == (status = sscanf(buf, "%8[^=]=%31[^:\n]:%31[^\n]", action, suser, spass)) )
			{
			c = toupper(action[0]);

			if ( c == 'A' )
				pauth->flags = BP$M_ACE_AGENT;
			else if ( c == 'U' )
				pauth->flags = BP$M_ACE_USER;
			else	{
				$LOG(STS$K_ERROR, "Unrecognized user's type '%s'", action);
				continue;
				}


			__util$str2asc (suser, &pauth->user);
			__util$str2asc (spass, &pauth->pass);

			auth_data_count++;
			pauth++;
			}
		else	status = $LOG(STS$K_WARN, "Skip unrecognized line #%d='%s'", buf);
		}

	fclose(finp);

	for ( pauth = auth_data, i = auth_data_count; i--; pauth++ )
		{
		$IFTRACE(g_trace, "%s=user[0:%d]='%.*s'/pass[0:%d]='%.*s'",
			 pauth->flags == BP$M_ACE_AGENT ? "AGENT" : "USER",
			$ASCLEN(&pauth->user), $ASC(&pauth->user), $ASCLEN(&pauth->pass), $ASC(&pauth->pass));
		}

	return	status;
}

/*
 *   DESCRIPTION: Performs access checking user/pass or IP against has been loaded authorization data.
 *	A type of checking
 *
 *   INPUT:
 *	user:	username string, ASCIC
 *	pass:	password string, ASCIC
 *	ia:	IP address
 *
 *   OUTPUT:
 *	flags:	a matched record flags, see BP$K_ACE_*
 *
 *
 *   RETURN:
 *	condition code
 */
int	auth_check	(
		ASC	*auth,
	struct in_addr	*ia,
		int	*flags
		)
{
int	i;
BP_AUTH_DATA	*pauth;
in_addr_t	addr;
ASC	user = {0}, pass = {0};
char	sip[32], smask[32];

	*flags = 0;

	if ( g_authmode == BP$K_AUTH_UNDEF )
		return	STS$K_SUCCESS;

	if ( g_authmode == BP$K_AUTH_LOGIN )
		{
		/*
		 * In case of Basic authorization we expect to see in the auth='username:password' pair
		 */
		if ( 2 != sscanf($ASCPTR(auth), "%31[^:\n]:%31[^\n]", $ASCPTR(&user), $ASCPTR(&pass)) )
			return	$LOG(STS$K_ERROR, "Cannot extract user/pass pair from '%.*s'", $ASC(auth));

		$ASCLEN(&user) = strnlen($ASCPTR(&user), ASC$K_SZ);
		$ASCLEN(&pass) = strnlen($ASCPTR(&pass), ASC$K_SZ);

		for ( i = auth_data_count, pauth = auth_data; i--; pauth++)
			{
			/* Skip unrelevant ACE records */
			if ( !(pauth->flags & (BP$M_ACE_USER | BP$M_ACE_AGENT)) )
				continue;

			/* Check user/name pair from request against local database */
			if ( __util$cmpasc(&user, &pauth->user))
				continue;

			if ( __util$cmpasc(&pass, &pauth->pass))
				continue;

			*flags = pauth->flags;

			return	$LOG(STS$K_SUCCESS, "%.*s@%s has been authorized", $ASC(&user), inet_ntoa(*ia) );
			}

		return	$LOG(STS$K_ERROR, "%.*s@%s authorization error", $ASC(&user), inet_ntoa(*ia) );
		}


	/* l_authmode == BP$K_AUTH_IP */
	for ( i = auth_data_count, pauth = auth_data; i--; pauth++)
		{
		/* Skip unrelevant ACE records */
		if ( (pauth->flags & (BP$M_ACE_USER | BP$M_ACE_AGENT)) )
			continue;


		inet_ntop(AF_INET, &pauth->net, sip, slen);
		inet_ntop(AF_INET, &pauth->mask, smask, slen);

		$IFTRACE(g_trace, "%s vs %s/%s", inet_ntoa(*ia), sip, smask);

		addr = *((in_addr_t *) ia);
		addr &= pauth->mask;

		if ( addr == pauth->net )
			{
			$IFTRACE(g_trace, "%s match ACE: %s=%s/%s", inet_ntoa(*ia), pauth->flags & BP$M_ACE_ACCEPT ? "Accept" : "Reject", sip, smask);

			if ( pauth->flags & BP$M_ACE_ACCEPT )
				return	STS$K_SUCCESS;

			break;
			}
		}

	return	$LOG(STS$K_ERROR, "%s has not been authorized", inet_ntoa(*ia) );
}




/*
 *   DESCRIPTION: Performs authorization against local database.
 *	A type of checking
 *
 *   INPUT:
 *	user:	username string, ASCIC
 *	pass:	password string, ASCIC
 *
 *   OUTPUT:
 *	flags:	a matched record flags, see BP$K_ACE_*
 *
 *   RETURN:
 *	condition code
 */
int	auth_check2	(
		ASC	*user,
		ASC	*pass,
		int	*flags
		)
{
int	i;
BP_AUTH_DATA	*pauth;

	*flags = 0;

	for ( i = auth_data_count, pauth = auth_data; i--; pauth++)
		{
		/* Skip unrelevant ACE records */
		if ( !(pauth->flags & (BP$M_ACE_USER | BP$M_ACE_AGENT)) )
			continue;

		/* Check user/name pair from request against local database */
		if ( __util$cmpasc(user, &pauth->user))
			continue;

		if ( __util$cmpasc(pass, &pauth->pass))
			continue;

		*flags = pauth->flags;

		return	$LOG(STS$K_SUCCESS, "%.*s has been authorized", $ASC(user));
		}

	return	$LOG(STS$K_ERROR, "%.*s authorization error", $ASC(user));
}



/*
 *   DESCRIPTION: Performs access checking user/pass or IP against has been loaded authorization data.
 *	A type of checking
 *
 *   INPUT:
 *	user:	username string, ASCIC
 *
 *   RETURN:
 *	condition code
 */
int	auth_uname_check(
		ASC	*user
		)
{
BP_AUTH_DATA	*pauth;

	if ( __util$cmpasc(user, &auth_data[0].user))
		return	$LOG(STS$K_ERROR, "%.*s unknown user", $ASC(user));

	return	$LOG(STS$K_SUCCESS, "%.*s has been authorized by login", $ASC(user));
}



/*
 *   DESCRIPTION: Performs checking of the authentication's configuraton options, calling routines
 *	to load ip or user authentication data.

 *   INPUT:
 *	authmode:	Authentication mode: IP or LOGIN
 *	dbsource:	File specification to be processed
 *
 *   RETURN:
 *	condition code
 */
int	auth_init	(
		ASC	*authmode,
		ASC	*dbsource
			)
{
int	status, c = 0;

	/* Sanity check */
	if ( (!authmode || !dbsource)
	     || (!$ASCLEN(authmode) && !$ASCLEN(dbsource)) )
		{
		$LOG(STS$K_WARN, "No authorization has been configured! Accept all request!");
		return	STS$K_SUCCESS;
		}

	if ( $ASCLEN(authmode) && !$ASCLEN(dbsource) )
		return	$LOG(STS$K_ERROR, "Check usage of the /DBSOURCE");

	if ( !$ASCLEN(authmode) && $ASCLEN(dbsource) )
		return	$LOG(STS$K_ERROR, "Check usage of the /AUTHMODE");

	/* Check that AUTHOMODE=IP|LOGIN */
	c = *($ASCPTR(authmode));
	c = toupper(c);

	if ( c == 'I' )
		{
		g_authmode = BP$K_AUTH_IP;
		status = auth_load_ip(dbsource);
		}
	else if ( c == 'L' )
		{
		g_authmode = BP$K_AUTH_LOGIN;
		status = auth_load_user(dbsource);
		}
	else	return	$LOG(STS$K_ERROR, "Unrecognized value of the /AUTHMODE option");


	return	$LOG(status, "Authorization subsystem initialization");
}
