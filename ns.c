#define	__MODULE__	"NS"
#define	__IDENT__	"X.00-03"

#ifdef	__GNUC__
	#pragma GCC diagnostic ignored  "-Wparentheses"
	#pragma	GCC diagnostic ignored	"-Wunused-variable"
#endif

#ifdef _WIN32
	#pragma once
	#define WIN32_LEAN_AND_MEAN             // Exclude rarely-used stuff from Windows headers
#endif

/*
**++
**
**  FACILITY:  BroProxy - A yet another SOCKS5(4), HTTP(s) high performance proxy server
**
**  DESCRIPTION: This module contains routines to cover functionality related Name Service:
**		resolving, caching and so on.
**		A behavour is controlled by configuration options:
**			/NSERVERS=<ns_server[,ns_server]>
**				ns_server = <ip[:port][/proto]>
**			/NSCACHE=<number>
**			/NSCACHE6=<number>
**
**  DESIGN ISSUE:
**
**
**  AUTHORS: Ruslan R. (The BadAss SysMan) Laishev
**
**  CREATION DATE:  23-APR-2019
**
**  MODIFICATION HISTORY:
**
**	30-APR-2019	RRL	Redesigned to use volatile context for NS stuff.
**
**	16-APR-2020	RRL	Resolved SIGSEGV in the ns_name2ip() reduced a local buffer size from NS_MAMSG=64K to 4096 octets;
**				the problem has been discoverd with combination with /THSTACK <= 64K
**
**--
*/

#include	<stdlib.h>
#include	<stdio.h>
#include	<pthread.h>
#include	<errno.h>
#include	<arpa/inet.h>
#include	<strings.h>
#include	<unistd.h>
#include	<time.h>
#include	<sys/types.h>
#include	<sys/socket.h>
#include	<netdb.h>
#include	<netinet/in.h>
#include	<arpa/nameser.h>
#include	<resolv.h>

#define		__FAC__	"BPROXY"
#define		__TFAC__ __FAC__ ": "		/* Special prefix for $TRACE			*/
#include	"utility_routines.h"

#include	"bproxy.h"

extern int	g_exit_flag, g_trace, g_nscache, g_ns_cache6;
extern	ASC	q_nserver;

int	ns_name2ip	(
		void	*nsctx,
		char	*name,
		char	*service,
	struct in_addr	*addr
			)
{
int	ret, len, i, status;
struct addrinfo *ai = NULL;
unsigned char ans[ 4096 /*NS_MAXMSG*/ ];
ns_msg	msg;
ns_rr	rr;
struct __res_state *res = (struct __res_state *) nsctx;

	if ( 0 > (len = ret = res_nquery(res, name, C_IN, T_A, ans, sizeof (ans))) )
		return	$LOG(STS$K_ERROR, "Error resolving '%s' to IP, res_nquery(%#x)->%d, errno=%d", name, nsctx, ret, errno);

	if ( ret = ns_initparse(ans, len, &msg) )
		return	$LOG(STS$K_ERROR, "Error resolving '%s' to IP, ns_initparse()->%d, errno=%d", name, ret, errno);

	if ( !(len = ns_msg_count(msg, ns_s_an)) )
		return	$LOG(STS$K_ERROR, "No host address for '%s' has been found");

	for ( status = STS$K_ERROR, i = 0; i < len; i++)
		{
		if ( (ret = ns_parserr(&msg, ns_s_an, i, &rr)) )
			$LOG(STS$K_ERROR, "ns_parserr()->%d, errno=%d", ret, errno);

		if ( ns_rr_rdlen(rr) != NS_INADDRSZ )
			continue;

		memcpy(addr, ns_rr_rdata(rr), sizeof (struct in_addr) );
		status = STS$K_SUCCESS;
		break;
		}

	return	status;
}


/*
 *  DESCRIPTION: Parse configuration options related to the NS, fill NS table by parsed data
 *
 *  IMPLICIT INPUT:
 *	g_nserver, g_nscache, g_nscache
 *
 *
 * IMPLICIT OUTPUT:
 *	ns_table
 *
 * RETURN:
 *	Condition code, see STS$K_* constants.
 */

int	ns_init	(
	void	**nsctx
		)
{
int	status, i;
char	*cp, *saveptr, ns[128], port[8], proto[8];
struct sockaddr_in *psock;
struct __res_state *resolver_data;

	if ( !(resolver_data = calloc ( 1, status = sizeof(struct __res_state))) )
		return	$LOG(STS$K_ERROR, "Insufficient memory, calloc(), errno=%d", errno);
	*nsctx = resolver_data;

	/* Initialize resolver by default data from resolv.conf */
	if ( status = res_ninit(resolver_data) )
		return	$LOG(STS$K_ERROR, "Inititialization of resolver failed, status=%d, errno=%d", status, errno);

	$IFTRACE(g_trace, "NS Resolver has been initialized, nsctx=%p", resolver_data);

	/* Process configuration option ... */
	$ASCLEN(&q_nserver) = __util$uncomment ($ASCPTR(&q_nserver), $ASCLEN(&q_nserver), '!');
	$ASCLEN(&q_nserver) = __util$collapse ($ASCPTR(&q_nserver), $ASCLEN(&q_nserver));

	if ( !$ASCLEN(&q_nserver) )
		return	STS$K_SUCCESS;

	for ( psock = resolver_data->nsaddr_list, resolver_data->nscount  = 0, saveptr = NULL;
		(i < MAXNS) && (cp = strtok_r(saveptr ? NULL : $ASCPTR(&q_nserver), ",", &saveptr));
			i++ )
		{
		$IFTRACE(g_trace, "#%02.2d : Parse NS '%s'", i, cp);

		if ( !(status = sscanf(cp, "%128[^:\n]:%8[^\n/]/%5[udpUDPtcpTCP]", ns, port, proto)) )
			{
			$LOG(STS$K_ERROR, "Illformed NS record '%s', expected format - ip[:port][/TCP|UDP]", cp);
			continue;
			}

		$IFTRACE(g_trace, "'%s':'%s'/'%s'", ns, port, proto);

		if ( !inet_pton(AF_INET, ns, &psock->sin_addr) )
			{
			$LOG(STS$K_ERROR, "Cannot convert '%s' to IP", ns);
			continue;
			}

		/* Fill sockaddr_in structure in the NS table for future using */
		if ( status = atoi(port) )
			psock->sin_port = htons(status);
		else	psock->sin_port = htons(53);

		psock->sin_family = AF_INET;

		$IFTRACE(g_trace, "NS server #%02.2d=%s", i, cp);

		/*
		 * Adjust to next NS table record,
		 * NS records counter
		 */
		psock++;
		resolver_data->nscount++;
		}

	return	STS$K_SUCCESS;
}


/*
 *  DESCRIPTION: Release context and resources has been created by ns_init()
 *
 *  IMPLICIT INPUT:
 *	g_nserver, g_nscache, g_nscache
 *
 *
 * IMPLICIT OUTPUT:
 *	ns_table
 *
 * RETURN:
 *	Condition code, see STS$K_* constants.
 */
int	ns_cleanup	(
		void	*nsctx
			)
{
int	status;
struct __res_state *resolver_data = nsctx;

	res_nclose(resolver_data);

	free(nsctx);

	return	STS$K_SUCCESS;

}
