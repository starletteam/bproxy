#define	__MODULE__	"BPROXY"
#define	__IDENT__	"V.01-07"
#define	__REV__		"1.07.0"

#ifdef	__GNUC__
	#pragma GCC diagnostic ignored  "-Wparentheses"
	#pragma	GCC diagnostic ignored	"-Wdate-time"
	#pragma	GCC diagnostic ignored	"-Wunused-variable"
	#pragma	GCC diagnostic ignored	"-Wmissing-braces"
#endif

#ifdef _WIN32
	#pragma once
	#define WIN32_LEAN_AND_MEAN             // Exclude rarely-used stuff from Windows headers
#endif


/*
**++
**
**  FACILITY:  BroProxy - A yet another SOCKS5(4), HTTP(s) high performance proxy server
**
**  DESCRIPTION: This is a main module, contains configuration processing, initialization,
**	pools (threads , buffers , and so on ...) creation. Start main listening thread.
**
**
**  AUTHORS: Ruslan R. (The BadAss SysMan) Laishev
**
**  CREATION DATE:  26-FEB-2019
**
**  MODIFICATION HISTORY:
**
**	19-MAR-2019	SYS	Added handling of the /BAN=<filespec> option;
**				adde calling intialization of the ban sites contexts;
**
**	22-APR-2019	RRL	Added /CSVCDR /T4CDR
**
**	23-APR-2019	RRL	Added /NSERVERS, /NSCACHE, /NSCACHE6
**
**	24-APR-2019	SYS	Added /TIMERS
**
**	26-APR-2019	SYS	Added /STATFILE, STATDELTA
**
**	29-APR-2019	RRL	Moved ns_init() to req.c
**
**	30-APR-2019	RRL	/EXTBIND improving
**
**	21-MAY-2019	RRL	Removed /CHAINED, added /PEERS
**
**	29-MAY-2019	RRL	Replaced CLOCK_MONOTONIC by CLOCK_REALTIME due non-discovered troubles with delta time computation.
**
**	31-MAY-2019	RRL	X.00-50 : Support "connected back" proxies.
**
**	 3-JUN-2019	RRL	Added experimental option /CONNBACK to switch BroProxy to MASTER-AGENT mode.
**
**	18-JUN-2019	RRL	Added /LOGSIZE=<number> option to limit log file size;
**
**	 8-SEP-2019	RRL	Added purging of expired outstanding request ( in connection back mode).
**
**	11-SEP-2019	RRL	X.00-52ECO1 : Removed calling of agent_purge() routine has been added at 8-SEP to resolve situation with unsuccessful
**				connection with target host at agent side.
**
**	 2-DEC-2019	RRL	X.00-53 : Improved handling of communication error in connection back mode.
**
**	 3-DEC-2019	RRL	X.00-54ECO1 : Added "Keep Alive checking" of the Agent's control channel
**
**	25-DEC-2019	RRL	X.00-54ECO2 : Fixed bug with  target port extraction during processing
**				request like:
**				CONNECT proxyleak.com:80 HTTP/1.1
**				Host: proxyleak.com:80
**				User-Agent: A proxy
**				Proxy-Authorization: Basic QnJvQnVkZDpzaGFybWFua2ExMzU=
**
**	30-DEC-2019	RRL	X.00-54ECO3 : Improved error handing of misconfigured start.
**
**	 1-FEB-2020	RRL	X.00-54ECO4 : Disable start w/o /CONFIG=<fspec> option
**
**	 2-FEB-2020	RRL	X.00-54ECO5 : Added option /THSTACK=<number> to control thread's stack size;
**				resolved problem with consuming VM by calling calloc() from thread;
**
**	 3-FEB-2020	RRL	X.00-54ECO5 : Resolved "problem" with allocation of 36 MB of memory - it's was implied by
**				preallocation of big number of streams.
**
**	21-FEB-2020	RRL	X.00-54ECO5 : Fixed call of setsockopt() with incorrect passing of the optval argument;
**				resolved  %BRPOXY-W: setsockopt(#6, IPPROTO_TCP, TCP_NODELAY)->14, errno=0
**
**	24-FEB-2020	RRL	X.00-54ECO8 : Improved diagnostic messages.
**
**	26-FEB-2020	RRL	X.00-55 : Added logic of checking of non-completed in 't_conn' outstanding requests;
**				fixed process crash with SIGILL.
**
**	19-MAR-2020	RRL	X.00-56 : Refactoring to move a part of reporting to BroAgent side.
**
**	24-MAR-2020	RRL	X.00-56ECO1 : Refactoring of logic of the stm_get().
**
**	29-MAR-2020	RRL	X.00-56ECO2 : Small correction of HTTP's responses according to RFC.
**
**	16-MAR-2020	RRL	X.00-56ECO3 : Resolved the SIGSEGV  in the ns_name2ip() has been introduced by /THSTACK size <= 64K
**
**	24-MAR-2020	RRL	X.00-56ECO4 : Fixed SISGEV in the __parse_http_header()
**
**	28-MAY-2020	RRL	X.00-57 : Refactoring of handling network I/O with EPOLL's stuff.
**
**	 6-JUN-2020	RRL	X.00-57ECO1 : Increased TCP backlog parameter from 32 to 128
**
**	21-JUL-2020	RRL	X.00-58 : Improved auto-adjusting of Request and I/O threads.
**
**	25-OCT-2020	RRL	V.01-00 : migration to RB Tree API.
**
**	 1-NOV-2020	RRL	V.01-01 : migration to AVL Tree API.
**
**	16-NOV-2020	RRL	V.01-01ECO1 : Recoded shutdown section of the main process to exclude SIGSEGV at process exiting stage.
**
**	13-FEB-2021	RRL	V.01-02 : Obfuscation of connection request from BroAgent.
**
**	11-MAR-2021	RRL	V.01-02ECO1 : Fixed typos in diagnostic messages.
**
**	 2-APR-2021	RRL	V.01-03 : Added data trafic obfuscation on the master-agent leg.
**
**	 8-APR-2021	RRL	V.01-04 : Added obfusction of client's HTTP request to the Agent;
**				replace XOR-ing with the nibble swaping.
**
**	16-MAY-2021	RRL	V.01-05 : Added /BACKLOG=<number> configuration parameter
**
**	29-JUN-2021	RRL	V.01-05ECO1 : Small non-principal corrections.
**
**	26-OCT-2021	RRL	V.01-05ECO3 : Improved error diagnostic in the REQ.C/recv_n() routine.
**
**	19-NOV-2021	RRL	V.01-05ECO4 : Fixed a bug of premature cancelation of the req_thread().
**
**	22-NOV-2021	RRL	V.01-05ECO5 : Fixed a bug with error handling of exhausting of outstanding requests to agent.
**				added /WREQLIM=<number> option to define a maximum number of outstanding request to agent.
**
**	25-NOV-2021	RRL	V.01-05ECO6 : Fixed leaking of WREQ contexts (outstanding requests to agent) AGENT.C module.
**				V.01-05ECO7 : Fixed potential context leaking in the IO.C
**
**	27-NOV-2021	RRL	V.01-05ECO8 : Fixes.
**
**	 3-DEC-2021	RRL	V.01-05ECO9 : Added some more diagnostic in the agent_ctl_check()
**
**	20-DEC-2021	RRL	V.01-06 : Added patching URI in the HTTP (!CONNECT) proxying.
**
**	21-DEC-2021	RRL	V.01-07 : Added support for SOCKS5 in master-agent mode.
**--
*/

#include	<stdio.h>
#include	<stdlib.h>
#include	<string.h>
#include	<unistd.h>
#include	<net/if.h>
#include	<sys/types.h>
#include	<sys/ioctl.h>
#include	<sys/stat.h>
#include	<arpa/inet.h>
#include	<errno.h>
#include	<stdarg.h>
#include	<time.h>
#include	<inttypes.h>
#include	<signal.h>

#define		__FAC__	"BPROXY"
#define		__TFAC__ __FAC__ ": "		/* Special prefix for $TRACE			*/
#include	"utility_routines.h"

#include	"bproxy.h"


#ifndef	__ARCH__NAME__
#define	__ARCH__NAME__	"VAX"
#endif

/* Global configuration parameters */
struct	sockaddr_in	g_lsock = {0};	/* We listen on ...	*/
const	socklen_t slen = sizeof(struct sockaddr);
const	int one = 1;


const char	g_http_agent [] = "StarLet/" __IDENT__ "/" __ARCH__NAME__  " (built  at "__DATE__ " " __TIME__ ")";

ASC	q_logfspec = {0},
	q_confspec = {0},
						/* STREAM pools vector of parameters	*/
	q_stmpool = {$ASCINI("128, 0, 0, 0, 0, 0, 0, 0")},

						/* I/O Threads  vector of parameters	*/
	q_iothreads = {$ASCINI("13, 5, 135, 5, 2, 10, 128, 32")},

						/* I/O Threads  vector of parameters	*/
	q_reqthreads = {$ASCINI("15, 3, 15, 3, 2")},

	q_bind = {0},
	q_dbsrc = {0},
	q_extbind = {0},
	q_peers = {0},
	q_auth = {0},
	q_uaf = {0},				/* IP or User authorization file	*/
	q_ban = {0},				/* File with list of banned sites	*/
	q_limit = {0},				/* Bandwitdth and traffic limits file	*/
	q_csvcdr = {0},				/* CDR files in .CSV and T4 formats	*/
	q_t4stat = {0},
	q_nserver = {0},
	q_timers = {$ASCINI("300, 1000, 3000, 3000")},
	q_statfspec = {0};

struct sockaddr_in g_extbind_sock = {0};

int	g_exit_flag = 0,			/* Global flag 'all must to be stop'	*/
	g_trace = 1,				/* A flag to produce extensible logging	*/

	g_authmode = BP$K_AUTH_UNDEF,
	g_nscache = 0,
	g_nscache6 = 0,
	g_statdelta = 0,
	g_iobufsz = BP$K_IOBUFSZ,
	g_connback = 0,				/* 1 - run in Master-Agent mode		*/
	g_logsize = 0,
	g_thstack = PTHREAD_STACK_MIN * 2,	/* 128 Kb by default			*/
	g_backlog = 128,			/* A back log length			*/
	g_wreqlim = 8192;			/* Limit for outstanding requests to agent */


BP_TIMERS	g_timers_set = {0};

OPTS optstbl [] =
{
	{$ASCINI("config"),	&q_confspec, ASC$K_SZ,	OPTS$K_CONF},

	{$ASCINI("bind"),	&q_bind, ASC$K_SZ,	OPTS$K_STR},

	{$ASCINI("extbind"),	&q_extbind, ASC$K_SZ,	OPTS$K_STR},
	{$ASCINI("peers"),	&q_peers, ASC$K_SZ,	OPTS$K_STR},

	{$ASCINI("trace"),	&g_trace, 0,		OPTS$K_OPT},
	{$ASCINI("logfile"),	&q_logfspec, ASC$K_SZ,	OPTS$K_STR},
	{$ASCINI("logsize"),	&g_logsize, 0,		OPTS$K_INT},

	{$ASCINI("stmpool"),	&q_stmpool, ASC$K_SZ,	OPTS$K_STR},
	{$ASCINI("iothreads"),	&q_iothreads, ASC$K_SZ,	OPTS$K_STR},
	{$ASCINI("reqthreads"),	&q_reqthreads, ASC$K_SZ,OPTS$K_STR},

	{$ASCINI("authmode"),	&q_auth, ASC$K_SZ,	OPTS$K_STR},
	{$ASCINI("uaf"),	&q_uaf, ASC$K_SZ,	OPTS$K_STR},
	{$ASCINI("ban"),	&q_ban, ASC$K_SZ,	OPTS$K_STR},

	{$ASCINI("limit"),	&q_limit, ASC$K_SZ,	OPTS$K_STR},

	{$ASCINI("csvcdr"),	&q_csvcdr, ASC$K_SZ,	OPTS$K_STR},
	{$ASCINI("t4cdr"),	&q_t4stat, ASC$K_SZ,	OPTS$K_STR},

	{$ASCINI("nserver"),	&q_nserver, ASC$K_SZ,	OPTS$K_STR},
	{$ASCINI("nscache"),	&g_nscache, 0,		OPTS$K_INT},
	{$ASCINI("nscache6"),	&g_nscache6, 0,		OPTS$K_INT},

	{$ASCINI("timers"),	&q_timers, ASC$K_SZ,	OPTS$K_STR},

	{$ASCINI("statfile"),	&q_statfspec, ASC$K_SZ,	OPTS$K_STR},
	{$ASCINI("statdelta"),	&g_statdelta, 0,	OPTS$K_INT},

	{$ASCINI("iobufsz"),	&g_iobufsz, 0,		OPTS$K_INT},

	{$ASCINI("connback"),	&g_connback, 0,		OPTS$K_OPT},
	{$ASCINI("thstack"),	&g_thstack, 0,		OPTS$K_INT},
	{$ASCINI("backlog"),	&g_backlog, 0,		OPTS$K_INT},

	{$ASCINI("wreqlim"),	&g_wreqlim, 0,		OPTS$K_INT},
	OPTS_NULL
};








void	sig_handler (int signo)
{
	if ( g_exit_flag )
		{
		fprintf(stdout, "Exit flag has been set, exiting ...\n");
		fflush(stdout);

		_exit(signo);
		}


	if ( signo == SIGUSR1 )
		{
		fprintf(stdout, "Set /TRACE=%s\n", (g_trace != g_trace) ? "ON" : "OFF");
		fflush(stdout);
		signal(signo, SIG_DFL);
		}
	else if ( (signo == SIGTERM) || (signo == SIGINT) || (signo == SIGQUIT))
		{
		fprintf(stdout, "Get the %d/%#x, set exit_flag!\n", signo, signo);
		fflush(stdout);

		g_exit_flag = 1;
		return;
		}
	else	{
		fprintf(stdout, "Get the %d/%#x signal\n", signo, signo);
		fflush(stdout);
		}

	_exit(signo);
}

void	init_sig_handler(void)
{
const int siglist [] = {SIGTERM, SIGINT, SIGUSR1, SIGQUIT, 0 };
int	i;

	/*
	 * Establishing a signals handler
	 */
	signal(SIGPIPE, SIG_IGN);	/* We don't want to crash the server due fucking unix shit */

	for ( i = 0; siglist[i]; i++)
		{
		if ( (signal(siglist[i], sig_handler)) == SIG_ERR )
			$LOG(STS$K_ERROR, "Error establishing handler for signal %d/%#x, error=%d", siglist[i], siglist[i], errno);

		//$IFTRACE(g_trace, "Set handler for signal %d/%#x (%s)", siglist[i], siglist[i], strsignal(siglist[i]));
		}
}






int	config_validate	(void)
{
int	iw, status = STS$K_SUCCESS;
char	*cp, *saveptr = NULL, *endptr = NULL;


	/* Mandatory parameter */
	if ( !$ASCLEN(&q_bind) )
		return	$LOG(STS$K_ERROR, "No /BIND=<ia:port>, recheck configuration");


	/* /TIMERS*/
	$ASCLEN(&q_timers) = __util$uncomment ($ASCPTR(&q_timers), $ASCLEN(&q_timers), '!');
	$ASCLEN(&q_timers) = __util$collapse ($ASCPTR(&q_timers), $ASCLEN(&q_timers));

	if ( $ASCLEN(&q_timers) )
		{
		cp = strtok_r( $ASCPTR(&q_timers), ",", &saveptr);
		iw = strtoul(cp, &endptr, 0);
		g_timers_set.t_seq.tv_sec = iw/1000;
		g_timers_set.t_seq.tv_nsec = 1000 * (iw % 1000);

		cp = strtok_r( NULL, ",", &saveptr);
		iw = strtoul(cp, &endptr, 0);
		g_timers_set.t_req.tv_sec = iw/1000;
		g_timers_set.t_req.tv_nsec = 1000 * (iw % 1000);

		cp = strtok_r( NULL, ",", &saveptr);
		iw = strtoul(cp, &endptr, 0);
		g_timers_set.t_conn.tv_sec = iw/1000;
		g_timers_set.t_conn.tv_nsec = 1000 * (iw % 1000);

		cp = strtok_r( NULL, ",", &saveptr);
		iw = strtoul(cp, &endptr, 0);
		g_timers_set.t_xmit.tv_sec = iw/1000;
		g_timers_set.t_xmit.tv_nsec = 1000 * (iw % 1000);
		}

	if ( $ASCLEN(&q_extbind) )
		{
		if ( !inet_pton(g_extbind_sock.sin_family = AF_INET, $ASCPTR(&q_extbind), &g_extbind_sock.sin_addr) )
			status = $LOG(STS$K_ERROR, "Cannot convert '%.s' to internal representative, errno=%d", $ASC(&q_extbind), errno );
		}

	g_thstack = $RANGE(g_thstack, 8192 * 1024, PTHREAD_STACK_MIN);
	$IFTRACE(g_trace, "Thread's stack size=%d octets", g_thstack);


	g_wreqlim = $RANGE(g_wreqlim, 8192, 64*1024);
	$IFTRACE(g_trace, "Maximum of outstanding requests to Agent (The)", g_wreqlim);

	return	status;
}


int	main	(int argc, char **argv)
{
int	status;

	if ((argc == 2) && (!strcmp (argv[1], "-v")))
		{
		fprintf (stdout, "%s\n", __REV__);
		return	1;
		}

	$LOG(STS$K_INFO, "Rev: " __IDENT__ "/"  __ARCH__NAME__   ", (built  at "__DATE__ " " __TIME__ " with CC " __VERSION__ ")");

	/*
	 * Process command line arguments
	 */
	if ( !(1 & __util$getparams(argc, argv, optstbl)) )
		return	$LOG(STS$K_ERROR, "Error processing configuration");

	if ( !$ASCLEN(&q_confspec) )
		return	$LOG(STS$K_ERROR, "Missing /CONFIG=<fspec> option");

	if ( $ASCLEN(&q_logfspec) )
		{
		__util$deflog($ASCPTR(&q_logfspec), NULL);

		$LOG(STS$K_INFO, "Rev: " __IDENT__ "/"  __ARCH__NAME__   ", (built  at "__DATE__ " " __TIME__ " with CC " __VERSION__ ")");
		}

	if ( g_trace )
		__util$showparams(optstbl);


	init_sig_handler ();

	if ( !(1 & config_validate ()) )
		return	$LOG(STS$K_ERROR, "Error config validation");


	status = stm_pool_init (&q_stmpool, g_iobufsz);
	status = io_crew_init (&q_iothreads);

	status = auth_init(&q_auth, &q_uaf);

	status = peer_load(&q_peers);
	status = ban_load(&q_ban);
	status = quota_load(&q_limit);

	if ( !(1 & (status = rq_crew_init(&q_reqthreads, &q_bind))) )
		return	$LOG(STS$K_ERROR, "Abort start");


	status = cdr_init();

	while ( !g_exit_flag )
		{
		for ( status = 2; status = sleep(status); );

		/* If logfile size has been set - rewind it ... */
		if ( g_logsize )
			__util$rewindlogfile(g_logsize);

		/* Performs auto adjusting of the I/O worker threads */
		io_crew_manager	();
		stm_pool_manager();
		rq_crew_manager();

		/* Check & load updates from file ... */
		status = peer_load(&q_peers);
		status = ban_load(&q_ban);
		status = quota_load(&q_limit);

		if ( !(1 & agent_ctl_check()) )
			{
			$LOG(STS$K_ERROR, "Agent's control channel error");

			agent_ctl_shut();
			}
		}

	$LOG(STS$K_INFO, "Shuting down with exit_flag=%d ... ", g_exit_flag);

	/* Small delay, let's finising threads ... */
	for (status = 5; status = sleep(status); );


	agent_ctl_shut();

	$LOG(STS$K_INFO, "Exiting with exit_flag=%d!", g_exit_flag);
}
