#define	__MODULE__	"BAN"
#define	__IDENT__	"X.00-01"

#ifdef	__GNUC__
	#pragma GCC diagnostic ignored  "-Wparentheses"
	#pragma	GCC diagnostic ignored	"-Wunused-variable"
#endif

#ifdef _WIN32
	#pragma once
	#define WIN32_LEAN_AND_MEAN             // Exclude rarely-used stuff from Windows headers
#endif

/*
**++
**
**  FACILITY:  BroProxy - A yet another SOCKS5(4), HTTP(s) high performance proxy server
**
**  DESCRIPTION: This module contains routines to cover ban functionality - disable access according
**	ACLs in the bproxy.ban configuration file.
**
**  DESIGN ISSUE: {tbs}
**
**  AUTHORS: Ruslan R. (The BadAss SysMan) Laishev
**
**  CREATION DATE:  19-MAR-2019
**
**  MODIFICATION HISTORY:
**
**--
*/

#include	<stdlib.h>
#include	<stdio.h>
#include	<errno.h>
#include	<strings.h>
#include	<unistd.h>
#include	<sys/types.h>
#include	<sys/stat.h>
#include	<pthread.h>

#define		__FAC__	"BPROXY"
#define		__TFAC__ __FAC__ ": "		/* Special prefix for $TRACE			*/
#include	"utility_routines.h"


#include	"bproxy.h"


extern int	g_exit_flag,
		g_trace;


static BP_BAN_ACE	*ban_data = NULL;
static int	ban_data_count = 0;
static	time_t	ban_mtime = {0};
pthread_rwlock_t ban_lock = PTHREAD_RWLOCK_INITIALIZER;


/*
 *   DESCRIPTION: Read line-by-line from given text file, load site string mask;
 *	*beeline*
 *	*yandex.ru
 *	*yand%%.ru
 *	skip empty or unrecognized/illformed lines.
 *
 *   INPUT:
 *	dbsource:	File specification to be processed
 *
 *   IMPLICIT OUTPUT:
 *	ban_data, ban_data_count
 *
 *   RETURN:
 *	condition code
 */
int	ban_load	(
		ASC	*dbsource
			)
{
int	status = 0, count, i, lineno;
FILE	*finp;
char	buf[128], smask[ASC$K_SZ], action[32], c;
BP_BAN_ACE	*pban, *pban_data = NULL;
struct	stat	sb = {0};
struct	timespec stime = {0, 0}, etime = {0, 0}, delta = {10, 0};

	if ( !$ASCLEN(dbsource) )
		return	STS$K_SUCCESS;

	/* Check firstly that the ban file is newer then local context */
	if ( 0 > (stat($ASCPTR(dbsource), &sb)) )
		$LOG(STS$K_ERROR, "BAN - Error access file '%.*s', errno=%d", $ASC(dbsource), errno);

	if ( !(sb.st_mtim.tv_sec >  ban_mtime) )
		return	STS$K_SUCCESS;

	if ( ban_mtime )
		$LOG(STS$K_INFO, "Update Ban ACEs from '%.*s'", $ASC(dbsource) );

	if ( !(finp = fopen ($ASCPTR(dbsource), "r")) )
		return	$LOG(STS$K_ERROR, "Error open '%.*', errno=%d", $ASC(dbsource), errno);

	/* Allocate memory for new BAN ACEs */
	 if ( !(pban_data = calloc(BP$K_MAXACE, sizeof(BP_BAN_ACE))) )
		 {
		 fclose(finp);
		 return	$LOG(STS$K_ERROR, "BAN - calloc(), errno=%d",  errno);
		 }

	 for ( lineno = 1, count = 0, pban = pban_data; count <= BP$K_MAXBAN && !feof(finp); lineno++)
		{
		/* Get string from file */
		fgets(buf, sizeof(buf), finp);

		if ( !(status = __util$uncomment (buf, strnlen(buf, sizeof(buf)), '!')) )
			continue;

		if ( !(status = __util$collapse (buf, strnlen(buf, sizeof(buf)))) )
			continue;

		buf[status] = '\0';

		/*
		 * ACCEPT=dp.beeline.ru
		 * REJECT=*.beeline.ru
		 */
		action[0] = 0;

		if ( 2 == (status = sscanf(buf, "%8[^=]=%17[\-\_A-Za-z0-9\.\*\%]", action, smask)) )
			{
			c = toupper(action[0]);

			if ( c == 'A' )
				pban->flags = BP$M_ACE_ACCEPT;
			else if ( c == 'R' )
				pban->flags = BP$M_ACE_REJECT;
			else	{
				$LOG(STS$K_ERROR, "BAN - Unrecognized action keyword '%s', lineno #%d", action, lineno);
				continue;
				}

			__util$str2asc (buf, &pban->site);

			count++;
			pban++;
			}
		}

	fclose(finp);

	for ( pban = pban_data, i = count; i--; pban++ )
		$IFTRACE(g_trace, "Ban ACE %s=[0:%d]='%.*s'", pban->flags & BP$M_ACE_ACCEPT ? "Accept" : "Reject", $ASCLEN(&pban->site), $ASC(&pban->site) );

	/*
	 * Acuire exclusive access to the BAN ACEs stuff
	 */
	if ( status = clock_gettime(CLOCK_REALTIME, &stime) )
		{
		free(pban_data);
		return	$LOG(STS$K_ERROR, "BAN - clock_gettime->%d", status);
		}

	__util$add_time (&stime, &delta, &etime);

	if ( status = pthread_rwlock_timedwrlock(&ban_lock, &etime) )
		{
		free(pban_data);
		return	$LOG(STS$K_ERROR, "BAN - pthread_rwlock_timedwrlock()->%d, errno=%d", status, errno);
		}

	if ( ban_data )
		free(ban_data);

	/* Save modification time for future checking */
	ban_mtime = sb.st_mtim.tv_sec;

	ban_data = pban_data;
	ban_data_count = count;

	if ( status = pthread_rwlock_unlock(&ban_lock) )
		$LOG(STS$K_WARN, "BAN - pthread_rwlock_unlock()->%d, errno=%d", status, errno);

	return	$LOG(STS$K_SUCCESS, "BAN - %d ACEs has been loaded", ban_data_count);
}


/*
 *   DESCRIPTION: Check a site name (IP or FQDN) against of a bproxy.ban list
 *
 *   INPUT:
 *	site:	site string, ASCIC
 *	ia:	IP address of the site
 *
 *   RETURN:
 *	condition code
 */
int	ban_check	(
		ASC	*site,
	struct in_addr	*ia
		)
{
int	i, status;
BP_BAN_ACE	*pban;
struct	timespec stime = {0, 0}, etime = {0, 0}, delta = {10, 0};

	/* No ACE ? */
	if ( !ban_data_count )
		return	STS$K_SUCCESS;

	/*
	 * Acuire exclusive access to the BAN ACEs stuff
	 */
	if ( status = clock_gettime(CLOCK_REALTIME, &stime) )
		return	$LOG(STS$K_ERROR, "BAN - clock_gettime->%d", status);

	__util$add_time (&stime, &delta, &etime);

	if ( status = pthread_rwlock_timedrdlock(&ban_lock, &etime) )
		return	$LOG(STS$K_ERROR, "BAN - pthread_rwlock_timedrdlock()->%d, errno=%d", status, errno);

	for (status = 0,  i = ban_data_count, pban = ban_data; i--; pban++)
		{
		if ( status = __util$pattern_match ($ASCPTR(site), $ASCPTR(&pban->site)) )
			break;
		}

	pthread_rwlock_unlock(&ban_lock);

	if ( !status )
		return	STS$K_SUCCESS;

	if ( pban->flags & BP$M_ACE_REJECT )
		return	$LOG(STS$K_ERROR, "%.*s has been banned", $ASC(site));

	return	STS$K_SUCCESS;
}
