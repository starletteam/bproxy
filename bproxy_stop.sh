#!/bin/bash

#
#  Performs a startup action before run a main procedure.
#

	killall -9 bproxy_run.sh >/dev/null
	killall -9 bproxy  >/dev/null

	exit 0
