#define	__MODULE__	"QUOTA"
#define	__IDENT__	"X.00-01"

#ifdef	__GNUC__
	#pragma GCC diagnostic ignored  "-Wparentheses"
	#pragma	GCC diagnostic ignored	"-Wunused-variable"
#endif

#ifdef _WIN32
	#pragma once
	#define WIN32_LEAN_AND_MEAN             // Exclude rarely-used stuff from Windows headers
#endif

/*
**++
**
**  FACILITY:  BroProxy - A yet another SOCKS5(4), HTTP(s) high performance proxy server
**
**  DESCRIPTION: This module contains routines to load quota and limits information
**
**
**  DESIGN ISSUE: {tbs}
**
**  AUTHORS: Ruslan R. (The BadAss SysMan) Laishev
**
**  CREATION DATE:  10-APR-2019
**
**  MODIFICATION HISTORY:
**
**--
*/

#include	<stdlib.h>
#include	<stdio.h>
#include	<errno.h>
#include	<strings.h>
#include	<unistd.h>
#include	<sys/types.h>
#include	<sys/stat.h>
#include	<stdatomic.h>

#define		__FAC__	"BPROXY"
#define		__TFAC__ __FAC__ ": "		/* Special prefix for $TRACE			*/
#include	"utility_routines.h"

#include	"bproxy.h"

extern int	g_exit_flag,
		g_trace;


static	BP_QUOTAS	quota = {0};
	BP_QUOTAS	g_quota = {0};
static	time_t	quota_mtime = {0};


static OPTS opts [] =
{
	{$ASCINI("inbandlim"),	&quota.inband, 0,	OPTS$K_INT},
	{$ASCINI("outbandlim"),	&quota.outband, 0,	OPTS$K_INT},
	{$ASCINI("connlim"),	&quota.conn, 0,		OPTS$K_INT},
	{$ASCINI("datalim"),	&quota.traffic, sizeof(quota.traffic),	OPTS$K_INT},

	OPTS_NULL
};



/*
 *   DESCRIPTION: Process configuration file
 *
 *   INPUT:
 *	dbsource:	File specification to be processed
 *
 *   IMPLICIT OUTPUT:
 *	quotas, quotas_mtime
 *
 *   RETURN:
 *	condition code
 */
int	quota_load	(
		ASC	*dbsource
			)
{
int	status = 0;
struct	stat	sb = {0};

	if ( !$ASCLEN(dbsource) )
		return	STS$K_SUCCESS;

	/* Check firstly that the quota file is newer then local context */
	if ( 0 > (stat($ASCPTR(dbsource), &sb)) )
		$LOG(STS$K_ERROR, "QUOTA - Error access file '%.*s', errno=%d", $ASC(dbsource), errno);

	if ( !(sb.st_mtim.tv_sec >  quota_mtime) )
		return	STS$K_SUCCESS;

	if ( quota_mtime )
		$LOG(STS$K_INFO, "QUOTA - Update Quotas & Limits from '%.*s'", $ASC(dbsource) );

	__util$readconfig ($ASCPTR(dbsource), opts);

	atomic_store(&g_quota.inband, quota.inband);
	atomic_store(&g_quota.outband, quota.outband);
	atomic_store(&g_quota.conn, quota.conn);
	atomic_store(&g_quota.traffic, quota.traffic);

	/* Save modification time for future checking */
	quota_mtime = sb.st_mtim.tv_sec;

	return	$LOG(STS$K_SUCCESS, "QUOTA - '%.*s' has bee loaded", $ASC(dbsource));
}

